# LCE-LS

Analysis code and pipelines from the Liver Cancer Evolution (LCE) consortium Lesion Segregation (LS) project.

## Summary

The primary manuscript relating to this work can be found as a pre-print at [bioRxiv](https://www.biorxiv.org/content/10.1101/868679v1).

We have deliberately left a full audit trail of edits and commit comments in the repository for transparency.

The code here deals with data processing from VCF for variant calls and BED/Kalisto output files for annotation and gene expression. Raw FASTQ files and BAM alignments are (will be) available from:

- WGS | European Nucleotide Archive (ENA), accession: PRJEB15138
- RNA-seq | Array Express, accession: E-MTAB-8518
- Digitised histology images | Biostudies: S-BSST129

In addition, we are in the process of releasing intermediate analysis files for more convenient exploration of the data and re-generation of manuscript figures.

Please direct queries to martin.taylor@igmm.ed.ac.uk

## Project organisation

All code (primarily R, some perl and shell) along with configuration files are located in the **src** directory. Analysis for this project has been split into multiple discrete projects with their own analysis directories directly under the project root:

### mu
- Initial processing of mutation calls from VCF.
- Annotation of mutations.
- Projection of orthologous positions between genomes.
- Derive sequence context for mutations
- Integration annotation and disparate identifiers for tumours and the animals they are from.

### nodules
- Calling of mutation asymmetry segments per tumour and merging data.

### ma
- Processing of multi-allelic variation.

### tcr
- Transcription coupled repair analysis.

### integration
- Analysis of sister chromatid exchange sites and their properties.

### kucab
- Processing of mutation data from [Kucab et al](https://doi.org/10.1016/j.cell.2019.03.001).

### icgc
- Processing of somatic mutation data from [ICGC release 28](https://dcc.icgc.org/releases/release_28)

Individual analysis directories contain a Snakemake file (or symbolic link to one in the src directory). It is intended that each project is run separately with Snakemake and should be run in the order presented above.

Manuscript figures are generated with **src/fig*.R** files utilising processed datafiles produced but Snakemake in the analysis directories.

## Getting started

### The conda environment

The conda environment is defined in the file **src/lce.yaml**.

### Customising for your batch submission environment

Adjustments to the peculiarities of your local cluster environment should be implemented in the file **src/cluster.lsf.json**, the provided example is tuned to the LSF batch management system. Indications of memory and runtime requirements for each job type can also be obtained from this file.

### Source data

Several input data-files are required for analysis including reference genomes, Ensembl genomic annotation, repeat masker output in addition to the mutation calls and gene expression data generated for this project. URLs, relative download locations and initial processing (decompression and format conversion) are documented and coded in **src/sourceData.sh**.

### Intermediate analysis files

Intermediate analysis files from which the manuscript figures can be directly generated are currently being prepared for release through the Edinburgh DataShare. This document will be updated with a link to their location when they are ready.
