####
# Run parameters
# Expects passed arguments of: $1=path_to_parameters_file $2=strain
#cd /hps/nobackup/flicek/user/mst/lce/ANALYSIS
#source ../src/initEnvironment.sh ../src/parameters_lce.sh c3h
paramFile=$1
export strain=$2
source $paramFile
source ${src}/parameters_lce_${SYSNAME}.sh
$PARAMsamtools
$PARAMr
$PARAMbedtools
$PARAMperl


cd ${analysis}
####
# Run familial clustering and multihit processing
runmem='-M 20096 -R "rusage[mem=20096]"'
runtime=3:00
logo=${runlogs}/${strain}.rhr.${dstamp}.o
loge=${runlogs}/${strain}.rhr.${dstamp}.e
bsub ${runmem} -W ${runtime}  -o ${logo} -e ${loge} Rscript --vanilla ${src}/recurrantHitsReload.R ${strain} ${rel} ${dstamp} ${reload}


####
# co and anti occurance analysis
runmem='-M 20096 -R "rusage[mem=20096]"'
runtime=1:00
logo=${runlogs}/${strain}.rhr.${dstamp}.o
loge=${runlogs}/${strain}.rhr.${dstamp}.e
bsub ${runmem} -W ${runtime}  -o ${logo} -e ${loge} Rscript --vanilla ${src}/coAntiOcc.R ${strain} ${rel} ${dstamp} ${reload}


####
# Run stacked sister chromatid exhange analysis
runmem='-M 24096 -R "rusage[mem=24096]"'
# runmem='-M 64096 -R "rusage[mem=64096]"'
runtime=94:00
logo=${logs}/${strain}.stackedSisterExchange.${dstamp}.o
loge=${logs}/${strain}.stackedSisterExchange.${dstamp}.e
bsub ${runmem} -W ${runtime}  -o ${logo} -e ${loge} Rscript --vanilla ${src}/stackedExchange.R ${strain} 20180406 20180813

####
# Run per-nodule based Transcription coupled repair analysis
runmem='-M 28096 -R "rusage[mem=28096]"'
runtime=24:00
logo=${logs}/${strain}.tcr.${dstamp}.o
loge=${logs}/${strain}.tcr.${dstamp}.e
bsub ${runmem} -W ${runtime}  -o ${logo} -e ${loge} Rscript --vanilla ${src}/transcriptionCoupledRepair.R ${strain} 20180406 20180813

####
# Run TCR results integration
runmem='-M 3096 -R "rusage[mem=3096]"'
runtime=4:00
logo=${logs}/${strain}.tcrIntegrator.${dstamp}.o
loge=${logs}/${strain}.tcrIntegrator.${dstamp}.e
bsub ${runmem} -W ${runtime}  -o ${logo} -e ${loge} Rscript --vanilla ${src}/transcriptionCoupledRepairIntegrator.R ${strain} 20180406 20180813
