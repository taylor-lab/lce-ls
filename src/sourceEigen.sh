#######
# bl6 liver tissue eigenvectors
# File eigenvectors_500kb.tar.gz obtained from Stuart Aitken
####
# This section does not replicate automatically - need info from Stuart
cd ${genomeDir}
zcat ${basedir}/eigenvectors_500kb.tar.gz | tar -xvf -
cd eigenvectors_500kb
# Stuart reports that these chroms need the sign of the eigenvector flipping
for c in 1 7 9 12 16 18 19
do
  export c
  cat eigen.KR.500000.chr.${c}.txt | perl -walne 'if(!$r){$r=0}$l=$r+1;$r=$l+499999;$F[0]=$F[0]*-1;print join"\t",($ENV{c},$l,$r,$F[0])' > bl6.eigen.${c}.tsv
done
# the sign of the vector is OK for these chromosomes
for c in 2 3 4 5 6 8 10 11 13 14 15 17 X Y
do
  export c
  cat eigen.KR.500000.chr.${c}.txt | perl -walne 'if(!$r){$r=0}$l=$r+1;$r=$l+499999;print join"\t",($ENV{c},$l,$r,$F[0])' > bl6.eigen.${c}.tsv
done
cat bl6.eigen.*.tsv | sort -Vk1,1 -k2,2 > ../bl6.liverHiC.eigen.tsv
rm *.tsv *.txt eigen_dump.sh
cd ..
rmdir eigenvectors_500kb
########
