#!/usr/bin/perl
use strict;

my ($vcfAnno,$threshClustCount,$dist) = @ARGV;
#open(CL, "<$clustList") or die "Failed to read $clustList\n";
my %clustKey;


open(VCF,"<$vcfAnno") or die "Failed to read $vcfAnno\n";
my $isOpen = 0;
my $lastContig = "NA";
my $lastPos = "0";
my $clustNum = 0;
my $key="NA";
my ($chr,$start,$end,$muSum);
my @muSite = ();
my @muSiteCount = ();
my @muPatient = ();
#my $stop = 1000000;
while (<VCF>){
  my $ln = $_;
  next if ($ln =~ /^#/);
  chomp;
  my @sp = split /\t/;
  my $numMu = 0;
  my @whichMu =();
  for my $p (9 .. $#sp-1){
    if (length($sp[$p])>1){
      $numMu++;
      push @whichMu, ($p - 9);
    }
  }
  if($sp[0] ne $lastContig or ($sp[1] - $lastPos) > $dist){
    # Close existing cluster
    if ($muSum >= $threshClustCount){
      my $siteList = join ",", @muSite;
      my $siteCounts = join ",", @muSiteCount;
      my $patients = join ",", @muPatient;
      my $siteSpan = $end - $start;
      print join "\t", ($chr,$start,$end,$key,$muSum,$siteSpan,$siteList,$siteCounts,$patients);
      print "\n";
    }
    $clustNum++;
    $key = "c$clustNum";
    # Initialise new cluster
    $isOpen = $key;
    $chr = $sp[0];
    $start = $sp[1]-1;
    $end = $sp[1];
    $muSum = $numMu;
    @muSite = (1);
    @muSiteCount = ($numMu);
    @muPatient = @whichMu;
  }
  else {
    # Extend cluster
    $end = $sp[1];
    $muSum += $numMu;
    my $pos = $sp[1] - $start;
    push @muSite,$pos;
    push @muSiteCount,$numMu;
    push @muPatient, @whichMu;
  }
  $lastContig = $sp[0];
  $lastPos = $sp[1];
  #last if ($stop-- <1);
}

# Close last cluster
if ($muSum >= $threshClustCount){
  my $siteList = join ",", @muSite;
  my $siteCounts = join ",", @muSiteCount;
  my $patients = join ",", @muPatient;
  my $siteSpan = $end - $start;
  print join "\t", ($chr,$start,$end,$key,$muSum,$siteSpan,$siteList,$siteCounts,$patients);
  print "\n";
}
print "#NORMALEXIT\n";
