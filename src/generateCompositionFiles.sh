
export thing=gene

# Ignore strandedness of feature - returns composition of forward strand over feature
bedtools getfasta -tab -bed ${genomeDir}/${strain}.${thing}.bed -fi ${genomeDir}/${strain}.fa | perl -w ${src}/streamTriNucCounter.pl > ${genomeDir}/${strain}.${thing}.triCounts

export fac=CTCF
export thing=${fac}.motifsInPeaks
for i in bl6 c3h cast caroli f344
  do bedtools intersect -wao -a ${genomeDir}/${i}.${fac}.motifs.bed -b ${genomeDir}/${i}.${fac}.chipPeaks.bed | perl -walne 'next if(!/^[\dX]\d*\t/);$F[3]="$F[0]:$F[1]$F[4]$F[2]";print join"\t",@F' > ${analysis}/${i}.${thing}.bed
  bedtools slop -l 1000 -r 1000 -g ${genomeDir}/${i}.chromSize.main -i ${analysis}/${i}.${thing}.bed > ${analysis}/${i}.${thing}.1kflanks.bed
  bedtools getfasta -name -tab -bed ${analysis}/${i}.${thing}.1kflanks.bed -fi ${genomeDir}/${i}.fa > ${analysis}/${i}.${thing}.1kflanks.slices
done
