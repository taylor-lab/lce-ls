##
# Scan on a per nodule basis for significant sister chromatid exhange events

####
changeOne<-"A"
changeTwo<-"T"
nodList<-nodule[treatment!="" & diagnosis %in% c("HCC","dysplasticnodule"),nodName,]

denMat<-mat[V13 %in% nodList,]
setkey(denMat,V13)
###
# code adapted from lesionSegregationX.R
w10m<-fread(file=paste("../GENOMES/",strain,"_10mb_windows.nucCounts",sep=""))
names(w10m)<-c("chr","start","end","A","C","G","T")
fOne<-w10m[chr=="X",sum(A)]
fTwo<-w10m[chr=="X",sum(T)]
winScale<-1e7
wholeXratio<-denMat[V1=="X",.(V7.frac=(sum(V7==changeOne)/fOne)/((sum(V7==changeOne)/fOne)+(sum(V7==changeTwo)/fTwo))),by=V13]
chromOffsets$mid<-chromOffsets$off+(chromOffsets$length/2)
###

#source("../src/lce_taylor_functions.R")
#####
# Chromosome by chromsome segmentation of a genome
##
# Initialise data structure
if(!exists("chromMerge")){
  chromMerge<-generateMerge()
}
for(nodNum in 1:length(nodList)){
  # Work with focal nodule
  focalSample<-nodList[nodNum]
  # This is the conditional selection of just the mutation types we want
  # to test for strand asymmetry of
  fmat<-mat[V13==focalSample & (V7==changeOne | V7==changeTwo)]
  fmat$Change<-NA
  fmat$Change[which(fmat$V7==changeOne)]=0
  fmat$Change[which(fmat$V7==changeTwo)]=1
  chromMerge<-rbind(chromMerge,segmentCaller(fmat,focalSample))
  print(nodNum)
}

####
# Sister exchange hotspots
nchrom<-length(chromOffsets$chromId)
gidMax<-chromOffsets$off[nchrom]+chromOffsets$length[nchrom]
cmOut<-chromMerge[!is.na(gNext),.(gEnd,gNext)]
breakStarts<-cmOut$gEnd
breakEnds<-cmOut$gNext
bsv<-rep(1,length(breakStarts))
bev<-rep(-1,length(breakStarts))
changePoints<-c(breakStarts,breakEnds)
genomeOrder<-order(changePoints)
orderedVec<-c(bsv,bev)[genomeOrder]
cumulativeSumCoverage<-cumsum(orderedVec)
#plot(changePoints[genomeOrder],cumulativeSumCoverage,type="l")

#####                     #####
##### Figure generation   #####
#####                     #####

png(file=paste(strain,dstamp,"sisterExchangeScanner.png",sep="."),width=1000,height=700)

figmat<-matrix(0,5,10)
figmat[c(2:4),c(1:9)]=1
figmat[c(5),c(1:9)]=5
figmat[1,c(1:9)]=2
figmat[1,10]=4
figmat[c(2:4),10]=3
layout(figmat)

####
# plot individual genome segments
#>fig1 - per genome segments
nodListOrdered<-wholeXratio[order(rank(V7.frac),V13)]$V13
par(mar=c(3,5,1,1))
plot(1,xlim=c(1,gidMax),ylim=c(1,length(nodList)),axes=F,type="n",xlab="",ylab="Nodule (ranked by chrX ratio)",cex.lab=1.6)
nodListFractionPolar<-data.table(nodule=nodListOrdered,countGrey=0,countPolar=0)
for(nodNum in 1:length(nodListOrdered)){
  ypos<-nodNum
  yhigh<-ypos+.4
  ylow<-ypos-.4
  yprofile<-c(ylow,yhigh,yhigh,ylow)
  fnod<-chromMerge[nodule==nodListOrdered[nodNum],]
  for(g in (1:dim(fnod)[1])){
    gSpan<-fnod$gEnd[g]-fnod$gStart[g]
    if(fnod$chr[g]=="X"){
      # don't count bias of X in nodListFractionPolar as we just want the
      # autosomal fraction
      gSpan=0
    }
    col="grey"
    if(fnod$fraction[g]>.7){
      col="blue"
      nodListFractionPolar$countPolar[nodNum]=nodListFractionPolar$countPolar[nodNum]+gSpan
      gSpan=0
    }
    if(fnod$fraction[g]<.3){
      col="yellow"
      nodListFractionPolar$countPolar[nodNum]=nodListFractionPolar$countPolar[nodNum]+gSpan
      gSpan=0
    }
    nodListFractionPolar$countGrey[nodNum]=nodListFractionPolar$countGrey[nodNum]+gSpan
    polygon(c(fnod$gStart[g],fnod$gStart[g],fnod$gEnd[g],fnod$gEnd[g]),yprofile,col=col,border=NA)
  }
}
axis(1,at=c(chromOffsets$off,gidMax),tick=T,labels=rep("",dim(chromWinBounds)[1]+1))
even<-which(c(1:length(chromOffsets$off))%%2==0)
odd<-which(c(1:length(chromOffsets$off))%%2==1)
axis(1,at=chromOffsets$mid[odd],labels=chromOffsets$chromId[odd],tick=F,cex.axis=1.6,col.axis="grey")
axis(1,at=chromOffsets$mid[even],labels=chromOffsets$chromId[even],tick=F,cex.axis=1.6,col.axis="black")


#>fig2 - retention asymmetry
sampleResolution<-1e6
samplePoints<-c(1:floor(gidMax/sampleResolution))*sampleResolution
fracHigh<-rep(NA,length(samplePoints))
for(sp in 1:length(samplePoints)){
  numHigh<-as.vector(unlist(chromMerge[gStart<=samplePoints[sp] & gEnd>samplePoints[sp] & fraction >= 0.7,.N,chr]))
  numLow<-as.vector(unlist(chromMerge[gStart<=samplePoints[sp] & gEnd>samplePoints[sp] & fraction <= 0.3,.N,chr]))
  nInformative<-(as.numeric(numHigh[2])+as.numeric(numLow[2]))
  fracHigh[sp]=as.numeric(numHigh[2])/nInformative
  if(is.na(nInformative)){
    fracHigh[sp]=NA
  }
  else{
    if(nInformative<10){
      fracHigh[sp]=NA
    }
  }
}
par(mar=c(0,5,1,1))
plot(samplePoints,fracHigh,type="l",axes=F,ylim=c(0,1),xlab="",ylab="Bias",cex.lab=1.6)
abline(h=0.5,col="lightblue")
axis(2,at=c(0,0.5,1),cex.axis=1.6)
axis(4,at=c(0,0.5,1),labels=F)

# Load relevant genomeLabel file for known driver annotation
glab<-read.table(file=paste(strain,"genomeLabel",sep="."))
for(k in 1:dim(glab)[1]){
  abl<-unlist(strsplit(as.vector(glab[k,1]),":"))
  ablPos<-as.numeric(abl[2])+chromOffsets$off[which(chromId==abl[1])]
  abcol<-"green"
  abcol<-as.vector(unlist(knownDriverList[which(knownDriverList$V1==glab[k,2]),2]))
  abline(v=ablPos,col=abcol)
  #text(ablPos,1,glab[k,2],cex=1.6,col=abcol,pos=4)
}


#>fig3
par(mar=c(3,0,1,2))
plot(1,axes=F,type="n",ylab="",xlab="Asymmetric (%)",ylim=c(1,length(nodList)),xlim=c(0,100))
abline(v=50,col="lightblue")
points((nodListFractionPolar$countPolar/(nodListFractionPolar$countPolar+nodListFractionPolar$countGrey))*100,1:length(nodList),pch=20)
axis(1,cex.axis=1.6)

#>fig4
par(mar=c(0,0,1,1))
plot(1,xlim=c(0,1),ylim=c(0,1),type="n",axes=F)
wth<-.1
polygon(c(0,0,wth,wth),c(0,1,1,0),col="blue",border=NA)
polygon(c(0,0,0,wth),c(0,1,1,0),col="yellow",border=NA)
# Loop through known drivers again to produce a key for their annotation
# in the summary strand bias plot
for(k in 1:dim(glab)[1]){
  abl<-unlist(strsplit(as.vector(glab[k,1]),":"))
  ablPos<-w10m[chr==abl[1] & start < abl[2] & end > abl[2],which=T]
  abcol<-"green"
  abcol<-as.vector(unlist(knownDriverList[which(knownDriverList$V1==glab[k,2]),2]))
  text(0.3,1-(k/6),glab[k,2],cex=1.6,col=abcol,pos=4)
}

#>fig5
par(mar=c(1,5,0,1))
plot(changePoints[genomeOrder],cumulativeSumCoverage,type="n",axes=F,xlim=c(0,gidMax),ylab="Exchange count",cex.lab=1.6)
points(changePoints[genomeOrder],cumulativeSumCoverage,type="l",col="grey")
roll30<-rollapply(cumulativeSumCoverage,30,mean,by=1,fill=T,align="center")
points(changePoints[genomeOrder],roll30,type="l",col="black")
axis(2,cex.axis=1.6)

dev.off()

###############
###############
###############
png(file="democoins4b70cptPlet.png",width=600,height=300)
plot(x)
points(meanx,type="l",col="red",xlab="position",ylab="Fraction forward strand")
#abline(v=ps$changePoints,col="blue")
#abline(v=ps$changePoints,col=rgb(0,0,1,.5),lwd=3)
O
abline(v=cptPelt,col=rgb(0,0,1,.5),lwd=3)
abline(v=bpos,col="black",lty=2,lwd=2)
dev.off()


####
# given changepoints - calculate averages for segment
