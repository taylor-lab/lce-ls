use strict;
my %nuclook = ( 'A' => 0,
  'C' => 1,
  'G' => 2,
  'T' => 3);
my $desiredThreash = 0.3;
my $minMu = 60;
my $isVCF = 0;
my $biasThreash = (1-$desiredThreash)-.5;
my (@fileList) = @ARGV;
foreach my $fi (@fileList){
  $isVCF=1 if($fi =~/vcf.gz$/);
  if ($isVCF){
    open FI, "zcat $fi |";
  }
  else {
    open FI, "<$fi";
  }
  my $numBiasC = 0;
  my $numBiasT = 0;
  my $numMu = 0;
  my $maxBias = 0;
  my %chridx;
  while(<FI>){
    chomp;
    next if (/^s*#/);
    my @sp = split /\t/;
    my @mu;
    if ($isVCF){
      @mu = ($sp[3],$sp[4]);
    }
    else{
      @mu = split />/,$sp[3];
    }
    $numMu++;
    if(!defined $chridx{$sp[0]}){
      @{$chridx{$sp[0]}} = (0,0,0,0);
    }
    $chridx{$sp[0]}[$nuclook{$mu[0]}]++;
  }
  close FI;
  my $out;
  for my $i (keys %chridx){
    my $sumC = $chridx{$i}[1]+$chridx{$i}[2];
    my $sumT = $chridx{$i}[0]+$chridx{$i}[3];
    my $biasC = "NA";
    my $biasT = "NA";
    my ($disC,$disT) = (0,0);
    if($sumC>=$minMu){
      $biasC = $chridx{$i}[1]/$sumC;
      $disC=abs(0.5-$biasC);
      $numBiasC++ if ($disC >= $biasThreash);
    }
    if($sumT>=$minMu){
      $biasT = $chridx{$i}[3]/$sumT;
      $disT=abs(0.5-$biasT);
      $numBiasT++ if ($disT >= $biasThreash);
    }
    $maxBias = $disT if ($disT > $maxBias);
    $maxBias = $disC if ($disC > $maxBias);
    $out .= join "\t",($i,$sumC,$sumT,$biasC,$biasT);
    $out .= "\n";
  }
  my @fs = split '/',$fi;
  my $fiShort = pop @fs;
  if (($numBiasC + $numBiasT) > 0){
    open CSS, ">$fiShort.byChromStrandSeg";
    print CSS $out;
    close CSS;
  }
  my $mbConvert = ($maxBias+0.5);
  print "$fiShort\t$numMu\t$numBiasC\t$numBiasT\t$mbConvert\n";
}
