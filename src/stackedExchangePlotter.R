cmdArgs = commandArgs(trailingOnly=TRUE)
cliargs<-cmdArgs

#cliargs<-c("c3h","20180406","20180813","0")
source("productionInit.R")


if(file.exists(paste(strain,"stackedExchange","Rdata",sep="."))){
    library("zoo")
    load(paste(strain,"stackedExchange","Rdata",sep="."))
}

doCosine=1
png(paste(strain,"stackedExchange","png",sep="."),width=600,height=1800)
figmat<-matrix(0,16,5)
figmat[1,]=1
figmat[2:6,]=2
figmat[7:8,]=3
figmat[9,]=4
figmat[10,]=5
figmat[11,]=6
figmat[12,]=7
figmat[13,]=8
figmat[14,]=9
figmat[15,]=10
figmat[16,]=11
layout(figmat)

# plotting params
axisScale<-1.4
axisLabScale<-1.8

# panel 1 - wobble area
par(mar=c(0,5,3,1))
numBreaks<-dim(breaks)[1]
plot(-windowFlank:windowFlank,wobbleVec,type="n",axes=F,ylab="Pos. uncertainty",xlab="",ylim=c(0,numBreaks),cex.lab=axisLabScale)
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,wobbleVec,0),border=NA,col="grey")
axis(2,at=c(0,numBreaks/2,numBreaks),labels=c(0,50,100),cex.axis=axisScale)
# panel 2 - muRate curve
par(mar=c(4,5,1,1))
plot(sw,muroll,type="n",xlab="Distance from switch (nt)",ylab="Mutation rate",axes=F,ylim=c(0,max(muroll,na.rm=T)),cex.lab=axisLabScale)
for(j in 1:numPermutes){
  murollPermute<-rollapply((muvecPermute[j,]/numBreaks)*1e6,FUN=mean,width=windowWidth,by=windowWidth,partial=T,align="center")
  points(sw,murollPermute,type="l",col=rgb(.6,.6,.6,.3))
}
abline(v=0,lwd=2,col="lightblue")
points(sw,muroll,type="l",col="black",lwd=2)
axis(1,cex.axis=axisScale)
axis(2,cex.axis=axisScale)
# panel 3 - spectrum shift
if(doCosine==1){
  mutypeMatRoll<-matrix(0,192,length(sw))
  mutypeMatPermuteRoll<-mutypeMatRoll
  for(j in 1:192){
    print(paste("roller",j,sep=" "))
    mutypeMatRoll[j,]=rollapply(mutypeMatrix[j,],FUN=sum,width=windowWidth,by=windowWidth,partial=T,align="center")
    mutypeMatPermuteRoll[j,]=rollapply(mutypeMatrixPermute[j,],FUN=sum,width=windowWidth,by=windowWidth,partial=T,align="center")
  }
  permuteAgregateFull<-rowSums(mutypeMatPermuteRoll)
  permuteAgregateFolded<-permuteAgregateFull[muTemplates$foldFor]+permuteAgregateFull[muTemplates$foldRev]
  cosineDistByWindow<-rep(0,length(sw))
  for(i in 1:length(sw)){
    m<-matrix(c(permuteAgregateFolded,mutypeMatRoll[muTemplates$foldFor,i]+mutypeMatRoll[muTemplates$foldRev,i]),2,96,byrow=T)
    cosineDistByWindow[i]=cosineDist(m)
  }
  ymax<-max(cosineDistByWindow)
  if(is.na(ymax)){
    ymax=1
  }
  plot(sw,cosineDistByWindow,axes=F,cex.lab=axisLabScale,ylab="Mutation spectrum distance",ylim=c(0,ymax))
  axis(1,cex.axis=axisScale)
  axis(2,cex.axis=axisScale)
}
topc<-function(x,count=100){
  (x/count)*100
}
par(mar=c(1,5,1,1))
# indels

indelWindows<-rollapply((indelVec/numBreaks)*1e6,FUN=mean,width=windowWidth,by=5000,align="center",partial=T)
plot(sw,indelWindows,ylim=c(min(indelWindows,na.rm=T),max(indelWindows,na.rm=T)*2),ylab="Indel rates (x1e6)",axes=F,cex.lab=axisLabScale,xlab="")
points(sw,indelWindows,type="l",col="black",lwd=2)
axis(2,cex.axis=axisScale)

# genic forward
ymax<-max(c(topc(genicForVec,count=numBreaks),topc(genicRevVec,count=numBreaks)))
plot(-windowFlank:windowFlank,topc(genicForVec),type="n",axes=F,ylab="Genic forward",xlab="",ylim=c(0,ymax),cex.lab=axisLabScale)
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(genicForVec,count=numBreaks),0),border=NA,col="grey")
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(genicForOnVec,count=numBreaks),0),border=NA,col="red")
axis(2,cex.axis=axisScale)
# genic reverse
plot(-windowFlank:windowFlank,topc(genicRevVec,count=numBreaks),type="n",axes=F,ylab="Genic reverse",xlab="",ylim=c(0,ymax),cex.lab=axisLabScale)
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(genicRevVec,count=numBreaks),0),border=NA,col="grey")
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(genicRevOnVec,count=numBreaks),0),border=NA,col="red")
axis(2,cex.axis=axisScale)
# ctcf
ymax<-max(topc(ctcfVec,count=numBreaks))
plot(-windowFlank:windowFlank,topc(ctcfVec,count=numBreaks),type="n",axes=F,ylab="CTCF",xlab="",ylim=c(0,ymax),cex.lab=axisLabScale)
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(ctcfVec,count=numBreaks),0),border=NA,col="darkgrey")
axis(2,cex.axis=axisScale)
# h3k4me3
ymax<-max(topc(h3k4me3Vec,count=numBreaks))
plot(-windowFlank:windowFlank,topc(h3k4me3Vec,count=numBreaks),type="n",axes=F,ylab="H3K4me3",xlab="",ylim=c(0,ymax),cex.lab=axisLabScale)
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(h3k4me3Vec,count=numBreaks),0),border=NA,col="darkgrey")
axis(2,cex.axis=axisScale)
# h3k27ac
ymax<-max(topc(h3k27acVec,count=numBreaks))
plot(-windowFlank:windowFlank,topc(h3k4me3Vec,count=numBreaks),type="n",axes=F,ylab="H3K27ac",xlab="",ylim=c(0,ymax),cex.lab=axisLabScale)
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(h3k27acVec,count=numBreaks),0),border=NA,col="darkgrey")
axis(2,cex.axis=axisScale)
# repeats
ymax<-max(topc(repeatsVec,count=numBreaks))
plot(-windowFlank:windowFlank,topc(repeatsVec,count=numBreaks),type="n",axes=F,ylab="Repeats",xlab="",ylim=c(0,ymax),cex.lab=axisLabScale)
polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,topc(repeatsVec,count=numBreaks),0),border=NA,col="darkgrey")
axis(2,cex.axis=axisScale)
# eigen compartment
if(strain=="bl6"){
  eigen<-eigenA/(eigenA+eigenB)
  ymax<-max(eigen*100)
  plot(-windowFlank:windowFlank,topc(repeatsVec,count=numBreaks),type="n",axes=F,ylab="Compartment A",xlab="",ylim=c(0,ymax),cex.lab=axisLabScale)
  polygon(c(-windowFlank,-windowFlank:windowFlank,windowFlank),c(0,eigen*100,0),border=NA,col="darkgrey")
  axis(2,cex.axis=axisScale)
}



dev.off()
