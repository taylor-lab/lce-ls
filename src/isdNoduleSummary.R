library("data.table")
library("zoo")
library("RColorBrewer")
library("bit64")
src="../src/"
genomesDir="../genomes/"
segmentsDir="./segments/"
expressionDir="./expression/"
imagesDir="./images/"
source(paste0(src,"lce_taylor_functions.R"))

# cat ../inputdata/bl6.whitelistmerged.bed | perl -walne '$m=$F[2]-$F[1];if($F[0]=~/^\d+$/){$n+=$m*2}else{$n+=$m};if(eof){print$n}'
strainList<-c("bl6","c3h","cast","caroli")
# need to modify to arc.blacklist
genomeVisible<-data.table(strain=c("bl6","c3h","cast","caroli"),genomeNt=as.numeric(0),maleDipNt=as.numeric(0))
for(strnNum in 1:length(strainList)){
  strn<-strainList[strnNum]
  blacklist<-fread(file=paste0(strn,".arc.blacklist.bed"))
  blacklist[,ln:=V3-V2]
  chrms<-fread(file=paste0("../genomes/",strn,".chromSize.main"))
  genomeVisible[strain==strn]$maleDipNt<-2*(chrms[V1!="X",sum(V2)]-blacklist[V1!="X",sum(ln)])+(chrms[V1=="X",sum(V2)]-blacklist[V1=="X",sum(ln)])
  genomeVisible[strain==strn]$genomeNt<-chrms[,sum(V2)]-blacklist[,sum(ln)]
}
fwrite(genomeVisible,file="lceGenomeVisible.tab")

#####
# Samples and summaries
sas<-fread("allStrains.LCE_isdWGSs.tab",header=T,sep=",")
for(strnNum in 1:length(strainList)){
  strn<-strainList[strnNum]
  sas[strain==strn,mpm:=(numSnvs/genomeVisible[strain==strn]$maleDipNt)*1e6]
}

#####

#####
# Mutation tables
#c3h.tabAll<-fread(file=paste0("c3h",".projectedFilteredMu.tab"),header=T,sep=",")
#cast.tabAll<-fread(file=paste0("cast",".projectedFilteredMu.tab"),header=T,sep=",")
#caroli.tabAll<-fread(file=paste0("caroli",".projectedFilteredMu.tab"),header=T,sep=",")
#bl6.tabAll<-fread(file=paste0("bl6",".projectedFilteredMu.tab"),header=T,sep=",")
#####


###
# Plot mutation counts
svg(file="mutationsPerDENnodule.svg",width=mmScale(40),height=mmScale(20))
ym<-(floor(max(sas$mpm,na.rm=T)/10)+1)*10
ybounds<-c(0,ym)
layout(matrix(c(1,2,3,4,5),1,5))
par(mar=c(4,7,1,0))
plot(sas$mpm,type="n",axes=F,ylim=ybounds,xlab="",ylab="")
axis(2,cex.axis=1.6,las=1)
mtext("Point mutations per Mb",side=2,line=3,cex=1)
par(mar=c(4,1,1,0))
#bl6
strn<-"bl6"
tp<-sas[cleanDiagnosis!="normal" & cause=="DEN" & strain==strn,.(nodId,numSnvs,cause,den1sig,den2sig,spont1sig,spont2sig,cosineSigFit,knownDriver,mpm)]
plot(tp[order(rank(mpm)),]$mpm,type="n",axes=F,ylim=ybounds,xlab="")
mtext(strn,side=1,line=0,cex=1)
mtext(paste0("n=",dim(tp)[1]),side=1,line=1,cex=0.8)
m<-median(tp[order(rank(mpm)),]$mpm)
mx<-c(0.25,0.75)*length(tp[order(rank(mpm)),]$mpm)
points(mx,c(m,m),type="l",lwd=3,col="black")
points(tp[order(rank(mpm)),]$mpm,pch=20,col="grey")
#c3h
strn<-"c3h"
tp<-sas[cleanDiagnosis!="normal" & cause=="DEN" & strain==strn,.(nodId,numSnvs,cause,den1sig,den2sig,spont1sig,spont2sig,cosineSigFit,knownDriver,mpm)]
plot(tp[order(rank(mpm)),]$mpm,type="n",axes=F,ylim=ybounds,xlab="")
mtext(strn,side=1,line=0,cex=1)
mtext(paste0("n=",dim(tp)[1]),side=1,line=1,cex=0.8)
m<-median(tp[order(rank(mpm)),]$mpm)
mx<-c(0.25,0.75)*length(tp[order(rank(mpm)),]$mpm)
points(mx,c(m,m),type="l",lwd=3,col="black")
points(tp[order(rank(mpm)),]$mpm,pch=20,col="grey")
#cast
strn<-"cast"
tp<-sas[cleanDiagnosis!="normal" & cause=="DEN" & strain==strn,.(nodId,numSnvs,cause,den1sig,den2sig,spont1sig,spont2sig,cosineSigFit,knownDriver,mpm)]
plot(tp[order(rank(mpm)),]$mpm,type="n",axes=F,ylim=ybounds,xlab="")
mtext(strn,side=1,line=0,cex=1)
mtext(paste0("n=",dim(tp)[1]),side=1,line=1,cex=0.8)
m<-median(tp[order(rank(mpm)),]$mpm)
mx<-c(0.25,0.75)*length(tp[order(rank(mpm)),]$mpm)
points(mx,c(m,m),type="l",lwd=3,col="black")
points(tp[order(rank(mpm)),]$mpm,pch=20,col="grey")
#caroli
strn<-"caroli"
tp<-sas[cleanDiagnosis!="normal" & cause=="DEN" & strain==strn,.(nodId,numSnvs,cause,den1sig,den2sig,spont1sig,spont2sig,cosineSigFit,knownDriver,mpm)]
plot(tp[order(rank(mpm)),]$mpm,type="n",axes=F,ylim=ybounds,xlab="")
mtext(strn,side=1,line=0,cex=1)
mtext(paste0("n=",dim(tp)[1]),side=1,line=1,cex=0.8)
m<-median(tp[order(rank(mpm)),]$mpm)
mx<-c(0.25,0.75)*length(tp[order(rank(mpm)),]$mpm)
points(mx,c(m,m),type="l",lwd=3,col="black")
points(tp[order(rank(mpm)),]$mpm,pch=20,col="grey")
dev.off()

###
# Mutation spectra and count histogram

svg(file="mutationSpectraAndCountHistWithDrivers.svg",width=mmScale(120),height=mmScale(35))
lgrid<-matrix(0,16,10)
lgrid[1:7,]=1
lgrid[8:9,]=2
lgrid[10:16,1:4]=3
lgrid[10:16,5:7]=3
layout(lgrid)
tpa<-sas[cleanDiagnosis!="normal" & cause=="DEN",.(nodId,numSnvs,cause,den1sig,den2sig,spont1sig,spont2sig,cosineSigFit,knownDriver,mpm,animalId,cleanDiagnosis,tumorGrade,steatosis,mitoses,diameter)]
xmod<-0
ym<-(floor(max(sas$mpm,na.rm=T)/11)+1)*10
driverList<-c("Braf","Hras","Egfr","Ctnnb1","Kras")
####
# mutation histogram
driMat<-matrix("white",dim(tpa)[1]+30,6)
driCol<-c("#b35806","#e08214","#7f3b08","#4393c3","#fdb863","black")
par(mar=c(0,5,1,0))
plot(1,xlim=c(0,dim(tpa)[1]+30),ylim=c(0,ym),type="n",axes=F,xlab="",ylab="")
axis(2,cex.axis=1.6,las=1)
mtext("Point mutations per Mb",side=2,line=3,cex=1)
for(sn in 1:length(strainList)){
  strn<-strainList[sn]
  tp<-sas[cleanDiagnosis!="normal" & cause=="DEN" & strain==strn,.(nodId,numSnvs,cause,den1sig,den2sig,spont1sig,spont2sig,cosineSigFit,knownDriver,mpm,animalId,cleanDiagnosis,tumorGrade,steatosis,mitoses,diameter)]
  plotOrder<-order(tp$mpm,decreasing=T)
  for(n in 1:length(plotOrder)){
    # inelegant and inefficient but not run many times
    xpos<-xmod+n
    for(g in 1:length(driverList)){
      if(grepl(driverList[g],tp$knownDriver[n])){
        nxtSpace<-min(which(driMat[xpos,]=="white"))
        driMat[xpos,nxtSpace]=driCol[g]
      }
    }
  }
  s2s<-tp$mpm[plotOrder]
  s1s<-tp$mpm[plotOrder]-(tp$mpm[plotOrder]*tp$spont2sig)
  d2s<-s2s-(tp$mpm[plotOrder]*tp$spont1sig)
  d1s<-s1s-(tp$mpm[plotOrder]*tp$den2sig)
  marg<-0.50
  m<-median(tp[order(rank(mpm)),]$mpm)
  points(c(xmod,xmod+length(plotOrder)),c(m,m),type="l",lwd=3,col="grey")
  topLineY<-as.vector(t(matrix(c(s2s,s2s),length(plotOrder),2)))
  xl<-(xmod+c(1:length(plotOrder)))-marg
  xr<-(xmod+c(1:length(plotOrder)))+marg
  topLineX<-as.vector(t(matrix(c(xl,xr),length(plotOrder),2)))
  polygon(c(topLineX[1],topLineX,topLineX[length(topLineX)]),c(0,topLineY,0),col="lightblue",border=NA)
  topLineY<-as.vector(t(matrix(c(s1s,s1s),length(plotOrder),2)))
  polygon(c(topLineX[1],topLineX,topLineX[length(topLineX)]),c(0,topLineY,0),col="darkblue",border=NA)
  topLineY<-as.vector(t(matrix(c(d2s,d2s),length(plotOrder),2)))
  polygon(c(topLineX[1],topLineX,topLineX[length(topLineX)]),c(0,topLineY,0),col="green",border=NA)
  topLineY<-as.vector(t(matrix(c(d1s,d1s),length(plotOrder),2)))
  polygon(c(topLineX[1],topLineX,topLineX[length(topLineX)]),c(0,topLineY,0),col=" ",border=NA)

  xmod=xmod+length(plotOrder)+5
}

####
# Drivers histogram
par(mar=c(0,5,0,0))
plot(1,xlim=c(0,dim(tpa)[1]+30),ylim=c(0,length(driverList)),type="n",axes=F,xlab="",ylab="")
for(col in 1:dim(driMat)[1]){
    for(row in 1:length(driverList)){
      if(driMat[col,row]!="white"){
        xl<-col-marg
        xr<-col+marg
        yt<-length(driverList)-(row-1)
        yb<-length(driverList)-row
        yt=yt-0.1
        yb=yb+0.1
        polygon(c(xl,xl,xr,xr),c(yb,yt,yt,yb),border=NA,col=driMat[col,row])
      }
    }
}

####
# Driver gene matrix
tp<-sas[cleanDiagnosis!="normal" & cause=="DEN" & strain==strn,.(nodId,numSnvs,cause,den1sig,den2sig,spont1sig,spont2sig,cosineSigFit,knownDriver,mpm,animalId,cleanDiagnosis,tumorGrade,steatosis,mitoses,diameter)]
hitMatrix<-matrix(0,length(driverList),dim(tp)[1])
hitMatrixType<-hitMatrix

#plotHitMatrix()

dev.off()

###
# Correlation of point mutations and sister exchanges
