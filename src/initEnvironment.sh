#!/bin/bash
####
# Run parameters
# Expects passed arguments of: $1=path_to_parameters_file $2=strain
export paramFile=$1
export strain=$2
export strainList="bl6 c3h cast caroli f344 hg38"
export lceCollab=/nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration
source $paramFile
source ${src}/parameters_lce_${SYSNAME}.sh
$PARAMsamtools
$PARAMr
$PARAMbedtools
$PARAMperl
