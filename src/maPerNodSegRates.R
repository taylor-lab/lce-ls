cliargs<-commandArgs(trailingOnly=TRUE)
source("../src/lceRbasics.R")

#cliargs<-c("c3h","93129_N7")
nodId<-cliargs[1]
nodName<-nodId
sas<-fread(file=paste0(analysisDir,"allStrains.LCE_isdWGSs.tab"),header=T,sep=",")
strn<-sas[nodId==nodName,strain][1]



lmatFile<-paste0(maDir,nodId,".nodMatMa")
drcrFile<-paste0(nodulesDir,nodId,".T.A.fnodDrcr")

lmat<-fread(file=lmatFile,header=T)
drcr<-fread(file=drcrFile,header=F)
names(drcr)<-c("strn","nodId","chr","startIndex","endIndex","cStart","cEnd","cNext","gStart","gEnd","gNext","fraction","changeOne","changeTwo","Drcr")
drcr[,maFraction:=as.numeric(NA)]

for(i in 1:dim(drcr)[1]){
  muCount<-lmat[chr==drcr$chr[i] & pos>=drcr$cStart[i] & pos<=drcr$cEnd[i],.N]
  maCount<-lmat[chr==drcr$chr[i] & pos>=drcr$cStart[i] & pos<=drcr$cEnd[i] & (pAlt1Ce>=2 | pAlt2Ce>=2),.N]
  if(muCount>0){
    drcr$maFraction[i]=maCount/muCount
  }
}

fwrite(drcr,file=paste0(maDir,nodId,".fnodDrcrMa"))
