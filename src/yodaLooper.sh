module add bedtools/2.27.0
module add samtools/1.5
stamp=`date +%Y%m`
strain=bl6
for runNum in `seq 9`; do
  bsub -W 3:00 -M 20096 -R "rusage[mem=20096]" -o ${strain}.${runNum}.${stamp}.o -e /dev/null Rscript --vanilla ../src/yodaLooper.R ${strain} ${runNum} ;
done

strain=c3h
for runNum in `seq 40`; do
  bsub -W 3:00 -M 20096 -R "rusage[mem=20096]" -o ${strain}.${runNum}.${stamp}.o -e /dev/null Rscript --vanilla ../src/yodaLooper.R ${strain} ${runNum} ;
done

strain=cast
for runNum in `seq 9`; do
  bsub -W 3:00 -M 20096 -R "rusage[mem=20096]" -o ${strain}.${runNum}.${stamp}.o -e /dev/null Rscript --vanilla ../src/yodaLooper.R ${strain} ${runNum} ;
done

strain=caroli
for runNum in `seq 9`; do
  bsub -W 3:00 -M 20096 -R "rusage[mem=20096]" -o ${strain}.${runNum}.${stamp}.o -e /dev/null Rscript --vanilla ../src/yodaLooper.R ${strain} ${runNum} ;
done

strain=f344
for runNum in `seq 4`; do
  bsub -W 3:00 -M 20096 -R "rusage[mem=20096]" -o ${strain}.${runNum}.${stamp}.o -e /dev/null Rscript --vanilla ../src/yodaLooper.R ${strain} ${runNum} ;
done
