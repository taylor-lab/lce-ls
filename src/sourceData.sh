#!/bin/bash
####
# Run parameters
# Expects passed arguments of: $1=path_to_parameters_file $2=strain
export strainList="bl6 c3h cast caroli"
export inputDir=${basedir}/inputdata
export lceCollab=/nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration
$PARAMsamtools
$PARAMr
$PARAMbedtools
$PARAMperl


###
# Get genomes

cd ${genomeDir}
wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/fasta/mus_musculus_c3hhej/dna/Mus_musculus_c3hhej.C3H_HeJ_v1.dna.toplevel.fa.gz
gunzip Mus_musculus_c3hhej.C3H_HeJ_v1.dna.toplevel.fa.gz
lgen=c3h
ln -s Mus_musculus_c3hhej.C3H_HeJ_v1.dna.toplevel.fa c3h.fa
echo "export gnm=C3H_HeJ_v1" > c3h.${rel}.version

wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/fasta/mus_musculus_casteij/dna/Mus_musculus_casteij.CAST_EiJ_v1.dna.toplevel.fa.gz
gunzip Mus_musculus_casteij.CAST_EiJ_v1.dna.toplevel.fa.gz
lgen=cast
ln -s Mus_musculus_casteij.CAST_EiJ_v1.dna.toplevel.fa cast.fa
echo "export gnm=CAST_EiJ_v1" > cast.${rel}.version

wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/fasta/mus_caroli/dna/Mus_caroli.CAROLI_EIJ_v1.1.dna.toplevel.fa.gz
gunzip Mus_caroli.CAROLI_EIJ_v1.1.dna.toplevel.fa.gz
lgen=caroli
ln -s Mus_caroli.CAROLI_EIJ_v1.1.dna.toplevel.fa ${lgen}.fa
# Note - Ensembl have EIJ but lce has changed that to EiJ
echo "export gnm=CAROLI_EiJ_v1.1" > caroli.${rel}.version


#### bl6
wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.primary_assembly.fa.gz
gunzip Mus_musculus.GRCm38.dna.primary_assembly.fa.gz
lgen=bl6
ln -s Mus_musculus.GRCm38.dna.primary_assembly.fa ${lgen}.fa
echo "export gnm=GRCm38" > ${lgen}.${rel}.version

###
# Rat??
echo "export gnm=F344" > f344.${rel}.version


wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.toplevel.fa.gz
gunzip Homo_sapiens.GRCh38.dna.toplevel.fa.gz
lgen=GRCh38
ln -s Homo_sapiens.GRCh38.dna.toplevel.fa ${lgen}.fa
echo "export gnm=GRCh38" > ${lgen}.${rel}.version

###
# hg19
/nfs/leia/research/flicek/user/rayner/src/hal/bin/hal2fasta  /hps/nobackup/flicek/user/cander21/ref/liftover/1509_ca.hal hg19 > ../genomes/hg19.fa
samtools faidx ../genomes/hg19.fa
strn=hg19
cat ../genomes/hg19.fa | perl -walne 'if($F[0]=~/^\d+$/){print"$F[0]\t$F[1]"}if($F[0]=~/^[XY]$/){print"$F[0]\t$F[1]"}' | sort -k1V > ${strn}.chromSize.main


# produce chromSize.main files
for strn in `echo ${strainList}`
do
  echo "${strn} fa indexing and chromSize"
  source ${genomeDir}/${strn}.${rel}.version
  samtools faidx ${strn}.fa
  cat ${strn}.fa.fai | perl -walne '$F[0]=~s/^chr//;if($F[0]=~/^\d+$/){print"$F[0]\t$F[1]"}if($F[0]=~/^[XY]$/){print"$F[0]\t$F[1]"}' | sort -k1V > ${strn}.chromSize.main
done

for strn in `echo ${strainList}`
do
  echo "${strn} making genomic windows"
  bedtools makewindows -g ${genomeDir}/${strn}.chromSize.main -w 1000 > ${genomeDir}/${strn}.1kbWindows.bed
done

# Produce knownDriversList
for strn in `echo ${strainList}`
do
  export strn
  echo "${strn} knonwDrivers"
  grep -v GENE_NAME ${lceCollab}/library_metadata/20171218/${strn}_putative_drivers_20171218.csv | perl -walne 's/"//g;@s=split/,/;$s[0]=~s/chr//;print "$s[0]:$s[1]_$s[3]/$s[4]\t$s[6]\t$ENV{strn}"' > ${strn}.knownDrivers
done

####
#### HiCi eigenvectors are calculatd in sourceEigen.sh and run manually
####

#######
# now move into ANALYSIS directory for next steps

cd ${analysis}
###
# Data and annotation generated in the LCE project
cp ${lceCollab}/library_metadata/${rel}/LCE_WGS_annotation_${rel}.tsv ${analysis}/
# simplify subsequent R processing of strings
cat ${analysis}/LCE_WGS_annotation_${rel}.tsv | perl -wlne 's/ //g;s/\//_/g;s/\//_/g;print' > LCE_WGS_annotation_${rel}.tsv.clean

# produce extractedImpact files for indels
for strn in `echo ${strainList}`
do
  echo "${strn} indels eImpact"
  source ${genomeDir}/${strn}.${rel}.version
  rname="indel.eImpact"
  depName="indels.matrix"
  deps="${src}/done.sh ${depName}.hold"
  bsub -W 1:00 -M 4096 -R "rusage[mem=4096]" -E ${deps} -J ${strn}.${rname} -o ${runlogs}/${strn}.${rname}.o -e /dev/null "zcat ${lceCollab}/variant_calls/${rel}/${strn}_strelka_indels_annotated_${gnm}.vcf.gz | perl -w ${src}/extractImpactPerTumorVef.pl ${strn}_strelka_indels_${rel}.ids > ${analysis}/${strn}.indels.extractedImpact ; echo ${dstamp} > ${strn}.${rname}.hold"
done

### Extract variant impact per gene (overlapping genes make this problematic from matrix file)
for strn in `echo ${strainList}`
do
  echo "${strn} snvs eImpact"
  source ${genomeDir}/${strn}.${rel}.version
  rname="all.extractedImpact"
  depName="indel.eImpact"
  depName2="matrix"
  deps="${src}/done.sh ${depName}.hold ${depName2}.hold"
  bsub -W 2:00 -M 4096 -R "rusage[mem=4096]" -E ${deps} -J ${strn}.${rname} -o ${runlogs}/${strn}.eImpact.o -e /dev/null "zcat ${lceCollab}/variant_calls/${rel}/${strn}_strelka_snvs_annotated_${gnm}.vcf.gz | perl -w ${src}/extractImpactPerTumorVef.pl ${strn}_strelka_snvs_${rel}.ids > ${analysis}/${strn}.snvs.extractedImpact ; cat ${analysis}/${strn}.snvs.extractedImpact ${analysis}/${strn}.indels.extractedImpact > ${analysis}/${strn}.all.extractedImpact ; echo ${dstamp} > ${strn}.${rname}.hold"
done

### Filter based on abnormal average coverage across samples.
# based on Craig Anderson's coverage processing. - Need code for original
# coverageWhitelist.bed generation
for strn in `echo ${strainList}`
do
  echo "${strn} suspect coveragge"
  source ${genomeDir}/${strn}.${rel}.version
  cat ${inputDir}/${strn}.whitelistmerged.bed | sort -V > ${genomeDir}/${strn}.coverageWhitelist.bed
  bedtools complement -i ${genomeDir}/${strn}.coverageWhitelist.bed -g ${genomeDir}/${strn}.chromSize.main > ${genomeDir}/${strn}.suspectCoverage.bed
done


### Add in repeatMasker annotation
cp ${lceCollab}/genome_metadata/genomes/mus_caroli/CAROLI_EiJ_v1.1/RepeatMasker-20170127/CAROLI_EiJ_v1.1.fa.out ${genomeDir}/caroli.repeatMasker.out
cp ${lceCollab}/genome_metadata/genomes/mus_musculus/C3H_HeJ_v1/RepeatMasker-20170127/C3H_HeJ_v1.fa.out ${genomeDir}/c3h.repeatMasker.out
cp ${lceCollab}/genome_metadata/genomes/mus_musculus_castaneus/CAST_EiJ_v1/RepeatMasker-20170127/CAST_EiJ_v1.fa.out ${genomeDir}/cast.repeatMasker.out
cp ${lceCollab}/genome_metadata/genomes/mus_musculus/GRCm38/RepeatMasker-20170127/GRCm38.fa.out ${genomeDir}/bl6.repeatMasker.out
#cp ${lceCollab}/genome_metadata/genomes/rattus_norvegicus/F344/RepeatMasker-20170127/F344.fa.out ${genomeDir}/f344.repeatMasker.out
# Convert repeatMasker .out files to bed and filter on main chromosome
for i in `echo ${strainList}`
  echo "${strn} repeatMasker to BED"
  do cat ${genomeDir}/${i}.repeatMasker.out | perl -wlne 's/^\s+//;print' | perl -F'\s+' -walne 'if($F[0]=/^\d+/){$F[9]=~s/[\(\)]//g;$F[4]=~s/^chr//;$s=$F[5]-1;$F[8]=~s/C/-/;print"$F[4]\t$s\t$F[6]\t$F[9]\t$F[0]\t$F[8]"}' | perl -wlne 'if(/^(chr)*[\d+XY]/){print}' > ${genomeDir}/${i}.repeatMasker.bed
done

####
# 15day liver gene expression
cd ${genomeDir}/expression_15day
zcat ${lceCollab}/expression/20180209/c3h_15day_rnaseq_kallisto_tpm_C3H_HeJ_v1.tar.gz | tar -xvf -
zcat ${lceCollab}/expression/20180209/bl6_15day_rnaseq_kallisto_tpm_GRCm38.tar.gz | tar -xvf -
zcat ${lceCollab}/expression/20180209/caroli_15day_rnaseq_kallisto_tpm_CAROLI_EiJ_v1.1.tar.gz | tar -xvf -
zcat ${lceCollab}/expression/20180209/cast_15day_rnaseq_kallisto_tpm_CAST_EiJ_v1.tar.gz | tar -xvf -
zcat ${lceCollab}/expression/20180209/f344_15day_rnaseq_kallisto_tpm_F344.tar.gz | tar -xvf -
cd ${basedir}

####
# nodule gene expression data
export strain=c3h
cd ${genomeDir}/expression_nodule
zcat ${lceCollab}/expression/20180713/c3h_tumour_rnaseq_kallisto_tpm_C3H_HeJ_v1.tar.gz | tar -xvf -
if [ -e c3h.nodule.Kalisto.tpm ] ;then rm c3h.nodule.Kalisto.tpm ;fi
for i in `ls *${strain}*kallisto_tpm.tsv | perl -walne 'if(/c3h_(\S+)_C3H/){print$1}'` ; do cat do*${i}_*_kallisto_tpm.tsv | perl -swalne 'next if(/^gene_id/);$t{$F[0]}+=$F[5];if(eof){foreach $k(keys%t){print join"\t",($name,$k,$t{$k})}}' -- -name=${i} >> c3h.nodule.Kalisto.tpm ; done

export strain=bl6
cd ${genomeDir}/expression_nodule
zcat ${lceCollab}/expression/20180713/bl6_tumour_rnaseq_kallisto_tpm_GRCm38.tar.gz | tar -xvf -
if [ -e bl6.nodule.Kalisto.tpm ] ;then rm bl6.nodule.Kalisto.tpm ;fi
for i in `ls *${strain}*kallisto_tpm.tsv | perl -walne 'if(/bl6_(\S+)_GRCm38/){print$1}'` ; do cat do*${i}_*_kallisto_tpm.tsv | perl -swalne 'next if(/^gene_id/);$t{$F[0]}+=$F[5];if(eof){foreach $k(keys%t){print join"\t",($name,$k,$t{$k})}}' -- -name=${i} >> bl6.nodule.Kalisto.tpm ; done

export strain=cast
cd ${genomeDir}/expression_nodule
zcat ${lceCollab}/expression/20180406/cast_tumour_rnaseq_kallisto_tpm_CAST_EiJ_v1.tar.gz | tar -xvf -
if [ -e ${strain}.nodule.Kalisto.tpm ] ;then rm ${strain}.nodule.Kalisto.tpm ;fi
for i in `ls *${strain}*kallisto_tpm.tsv | perl -walne 'if(/cast_(\S+)_CAST_EiJ/){print$1}'` ; do cat do*${i}_*_kallisto_tpm.tsv | perl -swalne 'next if(/^gene_id/);$t{$F[0]}+=$F[5];if(eof){foreach $k(keys%t){print join"\t",($name,$k,$t{$k})}}' -- -name=${i} >> ${strain}.nodule.Kalisto.tpm ; done

export strain=caroli
cd ${genomeDir}/expression_nodule
zcat ${lceCollab}/expression/20180406/caroli_tumour_rnaseq_kallisto_tpm_CAROLI_EiJ_v1.1.tar.gz | tar -xvf -
if [ -e ${strain}.nodule.Kalisto.tpm ] ;then rm ${strain}.nodule.Kalisto.tpm ;fi
for i in `ls *${strain}*kallisto_tpm.tsv | perl -walne 'if(/caroli_(\S+)_CAROLI_EiJ/){print$1}'` ; do cat do*${i}_*_kallisto_tpm.tsv | perl -swalne 'next if(/^gene_id/);$t{$F[0]}+=$F[5];if(eof){foreach $k(keys%t){print join"\t",($name,$k,$t{$k})}}' -- -name=${i} >> ${strain}.nodule.Kalisto.tpm ; done


#####
# get GTFs
cd ${genomeDir}
wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/gtf/mus_musculus/Mus_musculus.GRCm38.${ensVersion}.gtf.gz
ln -s Mus_musculus.GRCm38.${ensVersion}.gtf.gz bl6.gtf.gz

wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/gtf/mus_musculus_c3hhej/Mus_musculus_c3hhej.C3H_HeJ_v1.86.gtf.gz
ln -s Mus_musculus_c3hhej.C3H_HeJ_v1.86.gtf.gz c3h.gtf.gz

wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/gtf/mus_musculus_casteij/Mus_musculus_casteij.CAST_EiJ_v1.86.gtf.gz
ln -s Mus_musculus_casteij.CAST_EiJ_v1.86.gtf.gz cast.gtf.gz

wget -nd ftp://ftp.ensembl.org/pub/release-${ensVersion}/gtf/mus_caroli/Mus_caroli.CAROLI_EIJ_v1.1.90.gtf.gz
ln -s Mus_caroli.CAROLI_EIJ_v1.1.90.gtf.gz caroli.gtf.gz

for i in `echo ${strainList}` ; do zcat ${genomeDir}/${i}.gtf.gz | grep -v '^#' | perl -w ${src}/gtfToTsv.pl > ${genomeDir}/${i}.gtf.tsv ; done


# Produce genic-span subset
# Remember GTF is 1-based and BED 0-based half inlusive so need to transform left cooordinate
export thing=genicspan
for i in `echo ${strainList}` ; do cat ${genomeDir}/${i}.gtf.tsv | perl -walne 'if($F[9]eq"gene"){$F[1]-=1;print join "\t",@F}' > ${genomeDir}/${i}.${thing}.bed; done
for i in `echo ${strainList}` ; do cat ${genomeDir}/${i}.gtf.tsv | perl -walne 'if($F[9]eq"gene"){print join "\t",@F}' > ${genomeDir}/${i}.${thing}.tsv; done

for i in `echo ${strainList}`
  do runmem='-M 26096 -R "rusage[mem=26096]"'
  runtime=4:00
  logo=${runlogs}/${i}.tri.o
  loge=${runlogs}/${i}.tri.e
  echo "${strain} genic composition"
  bsub ${runmem} -W ${runtime}  -o ${logo} -e ${loge} "bedtools getfasta -name+ -tab -bed ${genomeDir}/${i}.${thing}.bed -fi ${genomeDir}/${i}.fa | perl -w ${src}/streamTriNucCounterNoMerge.pl > ${genomeDir}/${i}.${thing}.triCounts" ;
done

#grep transcript bl6.gtf.tsv > bl6.transcript.bed
#grep exon bl6.gtf.tsv > bl6.exon.bed

###
# Find motifs for TFs of interest


for i in `echo ${strainList}`
  do runmem='-M 2096 -R "rusage[mem=2096]"'
  runtime=5:00
  logo=${runlogs}/${i}.fimo.o
  loge=${runlogs}/${i}.fimo.e
  bsub ${runmem} -W ${runtime}  -o ${logo} -e ${loge} "fimo --o ${genomeDir}/${i}.fimo --bfile --uniform-- ${genomeDir}/testset.meme ${genomeDir}/${i}.fa " ;
done

fac=CTCF
for i in `echo ${strainList}`
  do grep ${fac} ${i}.fimo/fimo.tsv | perl -walne 'print join"\t",($F[2],$F[3]-1,$F[4],$F[1],$F[5],$F[6],$F[9])' | bedsort > ${i}.${fac}.motifs.bed
done

###
# peaks
ln -s /nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration/peak_calls/20180713/replicated/c3h_CTCF_C3H_HeJ_v1.bed ${genomeDir}/c3h.CTCF.chipPeaks.bed
ln -s /nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration/peak_calls/20180713/replicated/bl6_CTCF_GRCm38.bed ${genomeDir}/bl6.CTCF.chipPeaks.bed
ln -s /nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration/peak_calls/20180713/replicated/caroli_CTCF_CAROLI_EiJ_v1.1.bed ${genomeDir}/caroli.CTCF.chipPeaks.bed
ln -s /nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration/peak_calls/20180713/replicated/cast_CTCF_CAST_EiJ_v1.bed ${genomeDir}/cast.CTCF.chipPeaks.bed
#ln -s /nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration/peak_calls/20180713/replicated/f344_CTCF_F344.bed ${genomeDir}/f344.CTCF.chipPeaks.bed
cat /nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration/peak_calls/20180713/replicated/f344_CTCF_F344.bed | grep '^chr' | perl -wlane 's/^chr//;print' > f344.CTCF.chipPeaks.bed

fac=CTCF
for i in `echo ${strainList}`
  do bedtools intersect -wao -a ${genomeDir}/${i}.${fac}.motifs.bed -b ${genomeDir}/${i}.${fac}.chipPeaks.bed > ${analysis}/${i}.${fac}.motifsInPeaks.bed
done

###
# Liftovers from mm9 to mm10
cd ${genomeDir}
wget -nd http://hgdownload.soe.ucsc.edu/goldenPath/mm9/liftOver/mm9ToMm10.over.chain.gz
gunzip mm9ToMm10.over.chain.gz


###
# Nucleosomes
# GEO:GSE58005
# get *.peaks.txt.gz manually through web interface
cd ${genomeDir}
grep -v smt_pos GSM1399400_3M_rep1.peaks.txt | perl -walne '$l=$F[3]-1;print"$F[0]\t$l\t$F[3]\t$F[4]:$F[6]"' > GSM1399400_3M_rep1.peaks.bed
grep -v smt_pos GSM1399401_3M_rep2.peaks.txt | perl -walne '$l=$F[3]-1;print"$F[0]\t$l\t$F[3]\t$F[4]:$F[6]"' > GSM1399401_3M_rep2.peaks.bed
../src/liftOver GSM1399400_3M_rep1.peaks.bed mm9ToMm10.over.chain GSM1399400_3M_rep1.peaks.mm10.bed GSM1399400_3M_rep1.peaks.unmapped
../src/liftOver GSM1399401_3M_rep2.peaks.bed mm9ToMm10.over.chain GSM1399401_3M_rep2.peaks.mm10.bed GSM1399401_3M_rep2.peaks.unmapped
cat GSM1399400_3M_rep1.peaks.mm10.bed | perl -walne 'if($F[0]=~/chr[\dXY]+$/){s/chr//g;print}' > GSM1399400_3M_rep1.peaks.mm10.clean.bed
cat GSM1399401_3M_rep2.peaks.mm10.bed | perl -walne 'if($F[0]=~/chr[\dXY]+$/){s/chr//g;print}' > GSM1399401_3M_rep2.peaks.mm10.clean.bed
bedtools slop -i GSM1399400_3M_rep1.peaks.mm10.clean.bed -g bl6.chromSize.main -l 200 -r 200 > GSM1399400_3M_rep1.peaks.mm10.clean.200flanks.bed
bedtools slop -i GSM1399401_3M_rep2.peaks.mm10.clean.bed -g bl6.chromSize.main -l 200 -r 200 > GSM1399401_3M_rep2.peaks.mm10.clean.200flanks.bed
bsub -M 6400 -R "rusage[mem=6400]" -W 1:00 -o ${runlogs}/rep1nucs.o -e ${runlogs}/rep1nucs.e "bedtools getfasta -name -tab -bed ${genomeDir}/GSM1399400_3M_rep1.peaks.mm10.clean.200flanks.bed -fi ${genomeDir}/bl6.fa > ${analysis}/GSM1399400_3M_rep1.peaks.mm10.clean.200flanks.slices"
bsub -M 6400 -R "rusage[mem=6400]" -W 1:00 -o ${runlogs}/rep2nucs.o -e ${runlogs}/rep2nucs.e  "bedtools getfasta -name -tab -bed ${genomeDir}/GSM1399401_3M_rep2.peaks.mm10.clean.200flanks.bed -fi ${genomeDir}/bl6.fa > ${analysis}/GSM1399401_3M_rep2.peaks.mm10.clean.200flanks.slices"
