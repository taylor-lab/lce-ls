
# Produces Watson and Crick strand distributions for focal mutation type
# Does genome segmentation and produces summary ribon-plot

if(!exists("rainfallYlim")){
  rainfallYlim<-c(2,7)
}
if(!exists("rainfallYlabs")){
  rainfallYlabs<-c(min(rainfallYlim):max(rainfallYlim))
}
if(!exists("pointGamma")){
  pointGamma<-0.1
}

layout(figmat)
cboundCol<-"black"
cboundType<-3
par(mar=c(0,5,1,4),lend=1)
ylabParse<-expression(paste("Inter-",mu," distance (nt)"))
plot(1,type="n",xlim=c(1,genomeSpan),ylim=rainfallYlim,axes=F,ylab="",cex.lab=1.4)
labs=parse(text=paste0("10^",rainfallYlabs))
axis(2,cex.axis=1.4,at=rainfallYlabs,labels=labs,las=1,pos=-.01*genomeSpan)
abline(v=chromOffsets$off[c(2:nchrom)],col=cboundCol,lty=cboundType)
for(c in 1:dim(chromOffsets)[1]){
  rs<-which(nmatChange1[,1]==chromOffsets$chromId[c])
  xpos<-as.vector(unlist(nmatChange1[rs,3]))+chromOffsets$off[c]
  points(xpos,log10(nmatChange1$closest[rs]),pch=20,cex=0.8,col=rgb(colWatsonVec[1],colWatsonVec[2],colWatsonVec[3],pointGamma))
  repos<-which(nmatChange1$closest[rs]>ymax)
}
mtext(ylabParse,side=2,line=3)
mtext("Watson strand",side=2,line=2)

par(mar=c(0,5,0,4))
plot(1,type="n",xlim=c(1,genomeSpan),ylim=rev(rainfallYlim),axes=F,ylab="")
labs=parse(text=paste0("10^",rainfallYlabs))
axis(2,cex.axis=1.4,at=rainfallYlabs,labels=labs,las=1,pos=-.01*genomeSpan)
for(c in 1:dim(chromOffsets)[1]){
  rs<-which(nmatChange2[,1]==chromOffsets$chromId[c])
  xpos<-as.vector(unlist(nmatChange2[rs,3]))+chromOffsets$off[c]
  points(xpos,log10(nmatChange2$closest[rs]),pch=20,cex=0.8,col=rgb(colCrickVec[1],colCrickVec[2],colCrickVec[3],pointGamma))
}
abline(v=chromOffsets$off[c(2:nchrom)],col=cboundCol,lty=cboundType)
mtext(ylabParse,side=2,line=3)
mtext("Crick strand",side=2,line=2)



ypos<-0.5
yhigh<-ypos+.4
ylow<-ypos-.4
yprofile<-c(ylow,yhigh,yhigh,ylow)
plot(1,type="n",xlim=c(1,genomeSpan),ylim=c(0,1),axes=F,ylab="",xlab="")
fractionPolar<-0
sumGrey<-0
for(g in (1:dim(fnod)[1])){
  fblue<-fnod$fraction[g]
  fyellow<-1-fnod$fraction[g]
  col<-rgb(fyellow*colCrickVec[1],fyellow*colCrickVec[2],fblue)
  if(!is.na(fnod$gNext[g])){
    points(c(fnod$gEnd[g],fnod$gNext[g]),c(fnod$fraction[g],fnod$fraction[g+1]),type="l",col="red")
  }
  points(c(fnod$gStart[g],fnod$gEnd[g]),c(fblue,fblue),type="l",lwd=3,col=col)
}
axis(4,at=c(0,0.5,1),pos=(genomeSpan*1.01),cex.axis=1.4)

par(mar=c(4,5,0,4))
ypos<-0.5
yhigh<-ypos+.2
ylow<-ypos-.2
yprofile<-c(ylow,yhigh,yhigh,ylow)
plot(1,type="n",xlim=c(1,genomeSpan),ylim=c(0,1),axes=F,ylab="",xlab="",cex.lab=1.4)
for(g in (1:dim(fnod)[1])){
  gSpan<-fnod$gEnd[g]-fnod$gStart[g]
  if(fnod$chr[g]=="X"){
    # don't count bias of X in nodListFractionPolar as we just want the
    # autosomal fraction
    gSpan=0
  }
  col="grey"
  if(fnod$fraction[g]>.7){
    col=colWatson
    fractionPolar=fractionPolar+gSpan
    gSpan=0
  }
  if(fnod$fraction[g]<.3){
    col=colCrick
    fractionPolar=fractionPolar+gSpan
    gSpan=0
  }
  sumGrey=sumGrey+gSpan
  polygon(c(fnod$gStart[g],fnod$gStart[g],fnod$gEnd[g],fnod$gEnd[g]),yprofile,col=col,border=NA)
}
abline(v=chromOffsets$off[c(2:nchrom)],col=cboundCol,lty=cboundType)
axis(1,at=c(1,chromOffsets$off[c(2:nchrom)],genomeSpan),labels=F)
axis(1,at=chromOffsets$off+(chromOffsets$length/2),tick=F,labels=chromOffsets$chromId,cex.axis=1.4)
