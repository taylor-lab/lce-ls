# Count trinucleotide occurances on the forward strand
# Soft-masked and N bases count result in XXX counts.
# Martin Taylor, based on TriNucClassifierCount.pl but modified June 2016 to
# accept a streamed output of tabluated sequence from bedtools getfasta

use strict;


my ($tripos) = classes();

my @outOrder = sort {$a cmp $b} keys %{$tripos};

my $process=0;

my @header = ("chr\tchrLeft\tchrRight\tname",@outOrder);
print join "\t",@header;
print "\n";

#my ($triposSeqLocalTemplate) = classes();

while (<STDIN>){
  chomp;
  my @sp = split /\t/;
  my $ln = length($sp[1]);
  next if ($ln < 3);
  my @qq = split /:+/,$sp[0];
  if($#qq<0){
    @qq=("NA");
  }
  my($l,$r) = split /-/, $qq[2];
  # deals with some heterogeneity in the bedtools name+ generated output and how we process it.
  $r =~ s/\D+//g;
  $l +=1; # converts back to one-based coordinate
  my $triposSeqLocal = classes();
  my ($classed) = triNuc($triposSeqLocal,uc($sp[1]));
  my @outvec = ($qq[1],$l,$r,$qq[0]);
  foreach my $n (@outOrder){
    push @outvec, $triposSeqLocal->{$n};
  }
  print join "\t", @outvec;
  print "\n";
}

sub classes {
    my %cls;
    my @nucs = qw{A C G T};
    my $id=1;
    foreach my $n (@nucs){
	foreach my $m (@nucs){
	    foreach my $o (@nucs){
		my $tri = $n.$m.$o;
		if (!defined $cls{$tri}){
		    $cls{$tri} = 0;
		}
	    }
	}
    }
    $cls{'XXX'} = 0;
    return(\%cls);
}

sub triNuc {
    my ($tref,$sqstream) = @_;
    my $ln = length($sqstream);
    for my $i (0 .. ($ln-3)){
	my $trip=substr($sqstream,$i,3);
	if (!defined $tref->{$trip}){
	    $tref->{'XXX'}++;
	}
	else {
	    $tref->{$trip}++;
	}
    }
    return(1);
}
