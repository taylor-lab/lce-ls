
cd ${genomeDir}
# bl6

for i in do12360 do12361 do12362 do12363 do12367 do12368 do12369 do12370 do12371 do12373 do12374 do12375 do12376 do12377 do12378 do12379 do12380 do12381 do12382 do12383 do12384 do12385 do12386 do12387 do12388 do12389 do12390 do12391 do12392 do12393 do12394 do12395 do12396 do12397 do12398 do12399 do12400 do12401 do12402 do12403 do12404 do12406 do12407 do12408 do12409 do12410 do12411 do12412 do12413 do12414 do12415 do12416 do12418 do12419 do12420 do12421 do12422 do12424 do12425 do12426 do12427 do12429 do12430 do12431 do12433 do12434 do12436 do12437 do12438 do12439 do12440 do12442 do12443 do12444 do12445 do12446 do12447 do12449 do12450 do12452 do12453 do12454 do14396 do14399 do14400 do14401 do14403 do14404 do14405 do14412 do14415 do14419 do14422 do14423 do14426

bedtools multicov -bams ${alnDir}/do13022_*.bam ${alnDir}/do13032_*.bam ${alnDir}/do13043_*.bam ${alnDir}/do13043_*.bam ${alnDir}/do13047_*.bam ${alnDir}/do13055_*.bam ${alnDir}/do13058_*.bam ${alnDir}/do9189_*.bam ${alnDir}/do9190_*.bam ${alnDir}/do9195_*.bam ${alnDir}/do9388_*.bam ${alnDir}/do9782_*.bam ${alnDir}/do9867_*.bam ${alnDir}/do9883_*.bam -bed c3h.1kbWindows.bed > c3h.1kb.normals.multiCov

# cast normals
bedtools multicov -bams do12362_*.bam ${alnDir}/do12363_*.bam ${alnDir}/do12370_*.bam ${alnDir}/do12371_*.bam ${alnDir}/do12373_*.bam ${alnDir}/do12382_*.bam ${alnDir}/do12398_*.bam ${alnDir}/do12412_*.bam ${alnDir}/do12421_*.bam ${alnDir}/do12438_*.bam ${alnDir}/do12440_*.bam ${alnDir}/do12445_*.bam ${alnDir}/do12450_*.bam ${alnDir}/do14396_*.bam ${alnDir}/do14399_*.bam ${alnDir}/do14400_*.bam ${alnDir}/do14403_*.bam ${alnDir}/do14405_*.bam ${alnDir}/do14412_*.bam ${alnDir}/do14415_*.bam ${alnDir}/do14419_*.bam ${alnDir}/do14422_*.bam ${alnDir}/do14423 -bed cast.1kbWindows.bed > cast.1kb.normals.multiCov

# caroli normals
bedtools multicov -bams  -bed caroli.1kbWindows.bed > caroli.1kb.normals.multiCov

# bl6 normals
bedtools multicov -bams  -bed bl6.1kbWindows.bed > bl6.1kb.normals.multiCov
