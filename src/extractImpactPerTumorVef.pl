# Given VCF, extract table of:
# chr left right name 1 + from to context triName refCount altCount tumor_id sharedCount
use strict;
my ($idFile) = @ARGV;
my @idOrder = readIdFile($idFile);
# Genotype record structure in 20171103 release
# GT:VAF:DP:FDP:SDP:SUBDP:AU:CU:GU:TU
my $lastFeildOrder = "GT:VAF:DP:FDP:SDP:SUBDP:AU:CU:GU:TU";
my $nucCountPos = decodeFeildOrder($lastFeildOrder);
my (%filterGV);
while(<STDIN>){
  next if (/^#/);
  chomp;
  my @sp = split /\t/;
  if($sp[6] =~ /Germline/){
    my ($tv) = split /\|/, $sp[2];
    $filterGV{$tv}=1;
    next;
  }
  next if(defined $filterGV{$sp[2]});
  next if($sp[6] ne "PASS");
  my @csq = split /;CSQ=/, $sp[7];
  my $geneRef;
  if($#csq >0){
    ($geneRef) = reportVef($csq[1]);
  }
  $sp[2]=~ s/^chr//;
  my $prfx = $sp[2];
  my @outlines = ();
  my $sharedCount = 0;
  ###
  # multinucleotide or multi-allele
  next if(length($sp[4])>1);
  ###
  if ($sp[8] ne $lastFeildOrder){
    # More efficent than recomputing each time
    print STDERR "WARN: $lastFeildOrder => $sp[8]\n";
    $nucCountPos = decodeFeildOrder($sp[8]);
    $lastFeildOrder = $sp[8];
  }
  foreach my $k (keys %{$geneRef}){
    for my $tn (9 .. $#sp){
      my $tid = $tn - 9;
      next if($sp[$tn] =~ /^\./);
      print "$prfx\t$idOrder[$tid]\t$k\t$geneRef->{$k}\n";
    }
  }
}

sub reportVef {
  my ($vec) = @_;
  my @eachAnno = split /,/, $vec;
  my (%genes,$topImpact);
  $topImpact = 0;
  my %impact = ('MODIFIER' => 1,
    'LOW' => 2,
    'MODERATE' => 3,
    'HIGH' => 4);
  foreach my $ea (@eachAnno){
    my @easp = split /\|/, $ea;

    my $geneName = ".";
    if (defined $easp[3]){
      if ($easp[3] =~ /\w+/){
        $geneName = $easp[3];
      }
    }
    elsif (defined $easp[4]){
      if ($easp[4] =~ /\w+/){
        $geneName = $easp[4];
      }
    }
    $genes{$geneName} = 1;
    if (defined $impact{$easp[2]}){
      $topImpact = $impact{$easp[2]} if ($impact{$easp[2]}>$topImpact);
      if($genes{$geneName}<$impact{$easp[2]}){
        $genes{$geneName}=$impact{$easp[2]}
      }
    }
  }
  return(\%genes);
}

sub decodeFeildOrder {
  my ($strings) = @_;
  my @strs = split /:/, $strings;
  my %lookup = ('A' =>0, 'C' => 0, 'G' => 0, 'T' => 0);
  for my $n (0 .. $#strs){
    $lookup{'A'} = $n if ($strs[$n] eq "AU");
    $lookup{'C'} = $n if ($strs[$n] eq "CU");
    $lookup{'G'} = $n if ($strs[$n] eq "GU");
    $lookup{'T'} = $n if ($strs[$n] eq "TU");
  }
  return(\%lookup);
}

sub readIdFile {
  my ($fi) = @_;
  open(FI, "<$fi") or die "Failed to read $fi\n";
  my @ord;
  while (<FI>){
    chomp;
    my $n = $_;
    #$n =~ s/_/\t/g;
    push @ord, $n;
  }
  return(@ord);
}
