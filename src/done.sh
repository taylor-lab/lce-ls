#!/usr/bin/bash

# Given a list of files on the command line, check they all exist.
# if they do then return 0. Otherwise return 1.
args=$#
for ((i=1; i<=$args; i++))
do
  if [ ! -f ${!i} ]
  then
    exit 1
  fi
done

exit 0
