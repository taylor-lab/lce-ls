cliargs<-commandArgs(trailingOnly=TRUE)
source("../src/lceRbasics.R")
source(paste0(src,"nodulePlotterFunctions.R"))
cliargs<-c("hg19","samplesSummary.txt","denovo_subclone_doublesub_final.txt","figOtherMutagens.draft.pdf")

strain<-cliargs[1]
strn<-strain
samplesFile<-cliargs[2]
kucabDoubleFile<-cliargs[3]
outFile<-cliargs[4]

source(paste0(src,"doChromOffsets.R"))
chromOffsets[,chromId:=gsub("chr","",chromId)]
xmax<-chromOffsets[chromId=="X",off+length]
genomeSpan<-xmax
plimit<-1e-20
ss<-fread(file=samplesFile,header=T)
kMap<-fread(file="kucabMapping.txt")
kWithSig<-fread(file="kucabWithSig.txt")
#pcawg<-fread(file="../icgc/PCAWG_sigProfiler_SBS_signatures_in_samples.csv",header=T)
source(paste0(src,"kucabAnnotate.R"))
sp<-ss[toptypeCount>800,]

yshape<-c(0,1,1,0)
nchrom<-which(chromOffsets$chromId=="X")

colWatsonVec2<-c(.5,0,1)
colCrickVec2<-c(1,0.4,0)
colRbgW<-rgb(colWatsonVec[1],colWatsonVec[2],colWatsonVec[3])
colRbgC<-rgb(colCrickVec[1],colCrickVec[2],colCrickVec[3])
colRbgW2<-rgb(colWatsonVec2[1],colWatsonVec2[2],colWatsonVec2[3])
colRbgC2<-rgb(colCrickVec2[1],colCrickVec2[2],colCrickVec2[3])

annotateMargins<-function(x,right=NA,color="black",left=NA,line=6){
  par(new=T)
  plot(0,type="n",axes=F,xlab="",ylab="",xlim=c(0,10),ylim=c(0,10))
  mtext(x,adj=1,side=2,outer=0,las=2,at=10,font=2,line=line,cex=1.4)
  if(!is.na(right)){
    mtext(right,adj=1,side=4,outer=0,las=2,at=5,font=1,line=3,cex=1.2,col=color)
  }
  if(!is.na(left)){
    mtext(left,adj=1,side=2,outer=0,las=2,at=5,font=1,line=-1,cex=0.8,col=color)
  }
}


library("extrafont")
cairo_pdf(file=outFile,width=mmScale(180),height=mmScale(130),family="Arial",fallback_resolution=600)


########
# Order for output of ribon plots
setkey(sp,Alkylating_agents,Others,Nitro_PAHs,PAHs,Radiation,treatDesc)
########
# Layout
xpalloon<-c(1:28)
xpa<-c(1:20)
xpb<-c(21:40)
xstack<-c(45:80)
xgenome<-c(21:90)
figmat<-matrix(0,60,90)
# cumulative
figmat[1:20,xpa]=1
#  c3h
figmat[24:40,xpa]=2
#  kucab
figmat[24:40,xpb]=3
# zou
figmat[42:58,xpa]=4
# icgc
figmat[42:58,xpb]=5

# rainfall watson
figmat[1:9,xgenome]=6
# segmentaton
figmat[10:14,xgenome]=7
# rainfall crick
figmat[15:23,xgenome]=8
# stacked segmetation
figmat[24:60,xstack]=9
layout(figmat)
alevel<-"90"


########################################
# (a) rl20 statistic demo
par(mar=c(4,5,2,1))
plot(1,type="n",axes=F,xlab="",ylab="",xlim=c(0,100),ylim=c(0,1))
abline(h=.8,col="pink")
abline(v=22,col="pink",lty=2)
axis(1,at=c(22),labels=c(22),col.axis="pink",col.tick="pink",cex.axis=1.2)
axis(2,at=c(0.8),labels=c(80),col.axis="pink",col.tick="pink",cex.axis=1.2,las=2)
# load simMat object
load(file="rl20SimMatMu800.1000sims.Rdat")
simMat[100,]=1
minRl<-apply(simMat,1,min)
maxRl<-apply(simMat,1,max)
medianRl<-apply(simMat,1,median)
polygon(c(minRl,rev(maxRl)),c(100:1,1:100)/100,border=NA,col="grey")
points(medianRl,c(100:1)/100,col="grey",type="l",lwd=2)
points(medianRl,c(100:1)/100,col="black",type="l",lwd=1)
nodId<-"MSM0.56_s5" # SSR
fmat<-fread(file=paste0(nodId,".nodMat"),sep=",",header=T)
srss<-lsScanner(lmat=fmat,genomeSize=xmax)
# from the way we are calculating the cumulative distribution we are actually plotting in reverse.
lastLen<-srss$rlengths[1]
points(c(lastLen,srss$rlengths),c(1,1-srss$cumsumR),type="l",col="purple")
# example nodule from figure 2
if(1==2){
  gmat<-fread(file="../nodules/94315_N8.nodMat",header=T)
  n8ss<-lsScanner(lmat=gmat,genomeSize=xmax)
  lastLen<-n8ss$rlengths[1]
  points(c(lastLen,n8ss$rlengths),c(1,1-n8ss$cumsumR),type="l",col="red")
}
axis(1,cex.axis=1.2,at=c(0,50,100))
axis(2,cex.axis=1.2,at=c(0,.5,1),labels=c(0,50,100),las=2)
mtext("Run length",side=1,cex=.8,line=3)
mtext("Cumulative % of informative mutations",side=2,cex=.8,line=3)
##
# standard margin for palloons
par(mar=c(2,5,0.5,0.5))


########################################
# (b) plot c3h as a point of reference
odom<-fread(file="c3h.rlStatOut")
toP<-odom
toP[,classCol:="#FF0000"]
palloons(toPlot=toP[order(topTypeCount)],ymax=log10(120))



########################################
# (c) Kucab mutagenesis
# Potassium bromate is scored as ROS and metal, score as just ROS
ss[(treatType=="MSM0.102" | treatType=="MSM0.35"),Metals:=0]
ss[,withSig:=as.numeric(NA)]
kucab<-fread(file="mutagen.rlStatOut",header=T)
setkey(kucab,nodId)
setkey(ss,Sample)
# join tables on nodId
kss<-kucab[ss]
kss[,classCol:="#000000"]
for(s in 1:dim(ss)[1]){
  kss$withSig[s]=kWithSig[treatType==kss$treatType[s],withSig]
  ntype<-which(ss[s,7:19]==1)
  if(length(ntype)>0){
    typeClass<-names(ss)[6+ntype]
    kss$classCol[s]=paste0("#",kMap[class %in% typeClass,classCol])
  }
}

palloons(toPlot=kss[withSig>0][order(topTypeCount)],alpha=alevel,ymax=log10(120))


###########################################
#  plot DNA repair KO data from Zou
zou<-fread(file="denovo.rlStatOut")
toP<-zou[topTypeCount>=1]
toP[,classCol:="#FF0000"]
palloons(toPlot=toP[order(topTypeCount)],ymax=log10(120))

sz<-c(.05,.10,.15)
yv<-c(30,50,80)
for(i in 1:length(sz)){
  draw.circle(7,log10(yv[i]),nv=500,radius=sz[i]*4,border=NA,col="black")
}

#####
# (e) Load full ICGC data and apply donor filtering
icgcFull<-fread(file="icgc_rel28.rlAll")
doFilterFile<-"../icgc/LICA-CN.problemList"
doFilter<-fread(file=doFilterFile,header=F)
names(doFilter)<-c("icgcDonorId","subDonorId")
icgc<-icgcFull[!nodId %in% doFilter$subDonorId]

# list of projects to plot
pTick<-c("BTCA-SG","LICA-CN","RECA-EU")
pCol<-c("#000000","#AA0000","#0000AA")
pShow<-data.table(project=pTick,colour=pCol)
toP<-icgc[topTypeCount>=100 & project %in% pTick]
# set colours
toP[,classCol:="#333333"]
for(ps in 1:length(pTick)){
  toP[project==pTick[ps],classCol:=pCol[ps]]
}
palloons(toPlot=toP[order(topTypeCount)],alpha=alevel,ymax=log10(120))
xv<-5
yv<-.0
r<-.1*4
shapeX<-c(0,1,1,0)
shapeY<-c(0,0,1,1)
polygon((shapeX*1)+xv,(shapeY*.08)+yv,border=NA,col=paste0(pCol[1],alevel))
text(xv+1.2,yv+.04,"Biliary tract (Singapore) n=70",pos=4)
yv<-.1
polygon((shapeX*1)+xv,(shapeY*.08)+yv,border=NA,col=paste0(pCol[2],alevel))
text(xv+1.2,yv+.04,"Liver (China) n=135",pos=4)
yv<-.2
polygon((shapeX*1)+xv,(shapeY*.08)+yv,border=NA,col=paste0(pCol[3],alevel))
text(xv+1.2,yv+.04,"Renal (European Union) n=95",pos=4)

mtext("Runs test significance -log10(p-value)",side=1,line=2.5,cex=.8)
mtext(paste0("rl20"),side=2,line=2.5,cex=1.2)




########
# (f) UV light example
#nodId<-"MSM0.56_s4"
nodId<-"MSM0.56_s5" # SSR
fnod<-fread(file=paste0(nodId,".auto.drcr"),sep=",")
fmat<-fread(file=paste0(nodId,".nodMat"),sep=",",header=T)
doubleSub<-fread(file="denovo_subclone_doublesub_final.txt")
focalDs<-doubleSub[Sample==nodId]
names(fnod)<-c("Drcr","nodId","chr","indexLeft","indexRight","cStart","cEnd","cNext","gStart","gEnd","gNext","frac","changeOne","changeTwo")
drcr<-fnod
drcr[,genStart:=gStart]
drcr[,genEnd:=gEnd]
tone<-unlist(strsplit(fnod$changeOne[1],""))
ttwo<-unlist(strsplit(fnod$changeTwo[1],""))
nmatChange1<-buildNmat(fmat[refN==tone[1] & altN==tone[2]],offsets=chromOffsets)
nmatChange2<-buildNmat(fmat[refN==ttwo[1] & altN==ttwo[2]],offsets=chromOffsets)
changeVec<-fmat[(refN==tone[1] & altN==tone[2]) | (refN==ttwo[1] & altN==ttwo[2]),]
changeVec[refN==tone[1],changeClass:=0]
changeVec[refN==ttwo[1],changeClass:=1]
setkeyv(changeVec,c("chr","pos"))
#runs.test(changeVec[,changeClass],threshold=0.5)
## plotting
ylabParse<-expression(paste("Inter-",mu," distance (nt)"))
par(mar=c(0.5,10,2.5,6),lend=1,xpd=T)
# rainfall watson
plotRainfall(nmatChange1,forward=T,colvec=colWatsonVec,gSpan=genomeSpan,rainfallYlabs=c(0,3,6,9),rainfallYlim=c(0,9),pointGamma=1,pointScale=0.6)
###
# Add in dinuc changes
watsonFDS<-focalDs[dinuc_mutation=="CC>TT",.(Chrom,Pos)]
names(watsonFDS)<-c("chr","pos")
watsonFDS[,rt:=posToGenomePos(watsonFDS,offsets=chromOffsets)]
points(watsonFDS$gPos,rep(0.1,length(watsonFDS$gPos)),pch=20,col="white",cex=.6)
points(watsonFDS$gPos,rep(0.1,length(watsonFDS$gPos)),pch=2,col=colWatson)
#annotateMargins("",right=expression(C%->%T),color=colRbgW)
#annoMarg("a",yat=10)

# segments
par(mar=c(0.5,10,0.5,6),lend=1,xpd=T)
plotSegmentStepper(drcr,colTop=colWatsonVec,colBottom=colCrickVec,gSpan=genomeSpan)
annotateMargins("",left="Segmentation")
# rainfall crick
par(mar=c(2.5,10,0.5,6),lend=1,xpd=T)
plotRainfall(nmatChange2,forward=F,colvec=colCrickVec,gSpan=genomeSpan,rainfallYlabs=c(0,3,6,9),rainfallYlim=c(0,9),pointGamma=1,pointScale=0.6)
crickFDS<-focalDs[dinuc_mutation=="GG>AA",.(Chrom,Pos)]
names(crickFDS)<-c("chr","pos")
crickFDS[,rt:=posToGenomePos(crickFDS,offsets=chromOffsets)]
points(crickFDS$gPos,rep(0.1,length(crickFDS$gPos)),pch=20,col="white",cex=.6)
points(crickFDS$gPos,rep(0.1,length(crickFDS$gPos)),pch=6,col=colCrick)
xbs<-c(chromOffsets$off[c(1:nchrom)],xmax)
axis(1,at=xbs,labels=rep("",length(xbs)))
cMids<-chromOffsets$off+(chromOffsets$length/2)
mtext(chromOffsets$chromId,at=cMids,font=2,line=1,side=1,cex=0.8,col=c("black","darkgrey"))

#annotateMargins("",right=expression(G%->%A),color=colRbgC)


########
# (g) stacked other examples

par(mar=c(4,10,1,1))

kss[,shortDesc:=as.character(NA)]
kss[,control:=0]
kss[classCol=="#000000",control:=1]
for(s in 1:dim(kss)[1]){
  trim<-strsplit(kss[,treatDesc][s]," \\(")[[1]][1]
  kss$shortDesc[s]=trim
}
setkey(kss,control,Aldehydes,Alkylating_agents,Drug_therapy,DNA_damage_response_inhibitors,Aromatic_amines,Heterocyclic_amines,Radiation,Nitrosamines,PAHs,Nitro_PAHs,ROS,Metals,Others)

plotTable<-unique(kss[withSig>1 | control==1,.(shortDesc,control,Aldehydes,Alkylating_agents,Drug_therapy,DNA_damage_response_inhibitors,Aromatic_amines,Heterocyclic_amines,Radiation,Nitrosamines,PAHs,Nitro_PAHs,ROS,Metals,Others)])
dkss<-kss[withSig==1 & topTypeCount>200]
ntypes<-plotTable[control==0,.N]+1
plot(1,type="n",ylim=c(ntypes+1.5,1),xlim=log10(c(3,100)),axes=F,xlab="",ylab="")
abline(v=log10(5.5),col="lightblue")
bonfThr<-0.05/dim(kss)[1]
for(i in 1:ntypes){
  mtext(plotTable$shortDesc[i],las=2,line=2,side=2,at=i,cex=0.8)
  xv<-log10(kss[shortDesc==plotTable$shortDesc[i]]$rlStat)
  xvp<-kss[shortDesc==plotTable$shortDesc[i]]$runsP
  xvc<-kss[shortDesc==plotTable$shortDesc[i]]$topTypeCount
  r<-xvc/50000
  yv<-rep(i,length(xv))
  xvcol<-kss[shortDesc==plotTable$shortDesc[i]]$classCol[1]
  #points(xv,yv,col=xvcol)
  bord<-NA
  for(j in 1:length(xv)){
    draw.circle(xv[j],yv[j],nv=500,radius=r[j],border=bord,col=paste0(xvcol,"AA"))
    if(xvp[j]<bonfThr){
      points(xv[j],yv[j],pch=3,col="black")
    }
  }
}
controlTable<-plotTable[control==1]
for(i in 1:dim(controlTable)[1]){
  xv<-log10(kss[shortDesc==controlTable$shortDesc[i]]$rlStat)
  xvp<-kss[shortDesc==controlTable$shortDesc[i]]$runsP
  xvc<-kss[shortDesc==controlTable$shortDesc[i]]$topTypeCount
  r<-xvc/50000
  yv<-rep(ntypes+(i/3),length(xv))
  xvcol<-kss[shortDesc==controlTable$shortDesc[i]]$classCol[1]
  #points(xv,yv,col=xvcol)
  bord<-NA
  for(j in 1:length(xv)){
    draw.circle(xv[j],yv[j],nv=500,radius=r[j],border=bord,col=paste0(xvcol,"AA"))
    if(xvp[j]<bonfThr){
      points(xv[j],yv[j],pch=3,col="black")
    }
  }
}

# scalling key
rk<-c(200,400,800,1600,3200)/50000
xv<-log10(c(30,32,35,40,50))
yv<-rep(ntypes,5)
for(i in 1:5){
  draw.circle(xv[i],yv[i],nv=500,radius=rk[i],border=bord,col="#000000AA")
}

ticks<-c(seq(from=3,to=9,by=1),seq(from=10,to=80,by=10))
axis(1,at=log10(ticks),labels=rep("",length(ticks)),col="grey")
axis(1,at=log10(c(3,5,10,50)),labels=c(3,5,10,50),cex.axis=1.2)
mtext("rl20",side=1,line=2.5,cex=.8)

dev.off()

if(1==2){
  # stacked ribbons
  crickRampPalette<-colorRamp(c("grey",colCrick))
  watsonRampPalette<-colorRamp(c("grey",colWatson))
  par(mar=c(0.5,10,2,6),lend=1,xpd=T)
  plot(1,type="n",xlim=c(1,xmax),ylim=c(dim(sp)[1],1),axes=F,xlab="",ylab="")
  for(s in 1:dim(sp)[1]){
    nodId<-sp$Sample[s]
    fnod<-fread(file=paste0(nodId,".auto.drcr"),sep=",")
    fmat<-fread(file=paste0(nodId,".nodMat"),sep=",",header=T)
    names(fnod)<-c("drcr","nodId","chr","indexLeft","indexRight","cStart","cEnd","cNext","gStart","gEnd","gNext","frac","changeOne","changeTwo")
    tone<-unlist(strsplit(fnod$changeOne[1],""))
    ttwo<-unlist(strsplit(fnod$changeTwo[1],""))
    nmatChange1<-buildNmat(fmat[refN==tone[1] & altN==tone[2]],offsets=chromOffsets)
    nmatChange2<-buildNmat(fmat[refN==ttwo[1] & altN==ttwo[2]],offsets=chromOffsets)
    changeVec<-fmat[(refN==tone[1] & altN==tone[2]) | (refN==ttwo[1] & altN==ttwo[2]),]

    for(b in 1:dim(fnod)[1]){
      xshape<-c(fnod$gStart[b],fnod$gStart[b],fnod$gEnd[b],fnod$gEnd[b])

      col<-"lightgrey"
      if(fnod$drcr[b]>=.3){
        col<-fastRgb(watsonRampPalette(fnod$drcr[b])/255)
      }
      if(fnod$drcr[b]<=-.3){
        col<-fastRgb(crickRampPalette(abs(fnod$drcr[b]))/255)
      }
      polygon(xshape,yshape+s,border=NA,col=col)
    }
    nmatChange1[,gPos:=as.numeric(NA)]
    nmatChange2[,gPos:=as.numeric(NA)]
    changeTypeGt<-paste0(tone[1],">",tone[2])
    mtext(changeTypeGt,side=4,at=s+.5,cex=0.8,las=2,adj=1,line=-0.5)
    shortDesc<-gsub(" \\(.*\\)","",sp$treatDesc[s])
    mtext(shortDesc,side=2,at=s+.5,cex=0.8,las=2,adj=1,line=-0.5)
  }

  cboundCol<-"white"
  cboundType<-1
  abline(v=chromOffsets$off[c(2:nchrom)],col="white",lty=1)
  abline(v=chromOffsets$off[c(2:nchrom)],col=cboundCol,lty=cboundType)
  xbs<-c(chromOffsets$off[c(1:nchrom)],xmax)
  axis(3,at=xbs,labels=rep("",length(xbs)))
}
dev.off()
