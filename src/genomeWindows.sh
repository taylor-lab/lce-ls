# Get genome related files and their pre-processing for
# analysis scripts

###
# Get genomes
mkdir GENOMES
cd GENOMES
wget -nd ftp://ftp.ensembl.org/pub/release-93/fasta/mus_musculus_c3hhej/dna/Mus_musculus_c3hhej.C3H_HeJ_v1.dna.toplevel.fa.gz
gunzip Mus_musculus_c3hhej.C3H_HeJ_v1.dna.toplevel.fa.gz
ln -s Mus_musculus_c3hhej.C3H_HeJ_v1.dna.toplevel.fa c3h.fa
samtools faidx c3h.fa

wget -nd ftp://ftp.ensembl.org/pub/release-93/fasta/mus_musculus_casteij/dna/Mus_musculus_casteij.CAST_EiJ_v1.dna.toplevel.fa.gz
gunzip Mus_musculus_casteij.CAST_EiJ_v1.dna.toplevel.fa.gz
ln -s Mus_musculus_casteij.CAST_EiJ_v1.dna.toplevel.fa cast.fa

wget -nd ftp://ftp.ensembl.org/pub/release-93/fasta/mus_caroli/dna/Mus_caroli.CAROLI_EIJ_v1.1.dna.toplevel.fa.gz
gunzip Mus_caroli.CAROLI_EIJ_v1.1.dna.toplevel.fa.gz
ln -s Mus_caroli.CAROLI_EIJ_v1.1.dna.toplevel.fa caroli.fa

wget -nd ftp://ftp.ensembl.org/pub/release-93/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.toplevel.fa.gz
gunzip Mus_musculus.GRCm38.dna.toplevel.fa.gz
ln -s Mus_musculus.GRCm38.dna.toplevel.fa bl6.fa
samtools faidx bl6.fa

wget -nd # where's rat?

wget -nd ftp://ftp.ensembl.org/pub/release-93/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.toplevel.fa.gz
gunzip Homo_sapiens.GRCh38.dna.toplevel.fa.gz
ls -s Homo_sapiens.GRCh38.dna.toplevel.fa GRCh38.fa


export wsz=10mb
bedtools makewindows -g ${genShortName}.chromSize.main -w 10000000 > ${genShortName}_${wsz}_windows.bed
${btools} getfasta -tab -fi ${genFa} -bed ${genShortName}_${wsz}_windows.bed | perl -wlane '($ca,$cc,$cg,$ct)=(0,0,0,0);while($F[1]=~/A/ig){$ca++}while($F[1]=~/T/ig){$ct++}while($F[1]=~/C/ig){$cc++}while($F[1]=~/G/ig){$cg++};if($F[0]=~/^(\S+):(\d+)-(\d+)/){print"$1\t$2\t$3\t$ca\t$cc\t$cg\t$ct";}' > ${genShortName}_${wsz}_windows.nucCounts

export wsz=50kb
bedtools makewindows -g ${genShortName}.chromSize.main -w 50000 > ${genShortName}_${wsz}_windows.bed
${btools} getfasta -tab -fi ${genFa} -bed ${genShortName}_${wsz}_windows.bed | perl -wlane '($ca,$cc,$cg,$ct)=(0,0,0,0);while($F[1]=~/A/ig){$ca++}while($F[1]=~/T/ig){$ct++}while($F[1]=~/C/ig){$cc++}while($F[1]=~/G/ig){$cg++};if($F[0]=~/^(\S+):(\d+)-(\d+)/){print"$1\t$2\t$3\t$ca\t$cc\t$cg\t$ct";}' > ${genShortName}_${wsz}_windows.nucCounts



####
# repeatmasker annotation of matrix file.
# It is occiasionally possible for a mutation to overlap two repeats. This leads
# to duplication of the repeat line hence the perl filtering using a simple hash
# so we only retain one copy of each mutaton for each nodule in the matrix file.
export rel=20180406
for strain in c3h cast caroli bl6 f344; do  bedtools intersect -wao -a ${strain}.${rel}.vef.matrix.cln -b ../genomes/${strain}.rmsk.cln.bed | perl -walne '$k="$F[3]$F[12]";if(!defined$o{$k}){$o{$k}=1;print}' > ${strain}.${rel}.vef.matrix.repAnno ;done
