# Process GTF TSV files to get transcript ends
use strict;
my ($fileIn,$tssOut,$tesOut) = @ARGV;
open(FI,"<$fileIn");
open(SS,">$tssOut");
open(SE,">$tesOut");
my ($lft,$rght,$lftE,$rghtE);
while(<FI>){
  chomp;
  my @sp = split /\s+/;
  if($sp[5] eq "-"){
    $lft=$sp[2]-1;
    $rght=$sp[2];
    $lftE=$sp[1];
    $rghtE=$sp[1]+1;
  } elsif ($sp[5] eq "+"){
    $lft=$sp[1];
    $rght=$sp[1]+1;
    $lftE=$sp[2]-1;
    $rghtE=$sp[2];
  }
  print SS join "\t", ($sp[0],$lft,$rght,@sp[3..9]);
  print SS "\n";
  print SE join "\t", ($sp[0],$lftE,$rghtE,@sp[3..9]);
  print SE "\n";
}
