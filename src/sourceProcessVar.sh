#!/bin/bash
####
# Run parameters
# Expects passed arguments of: $1=path_to_parameters_file $2=strain
paramFile=/hps/nobackup2/flicek/user/mst/lce/src/parameters_lce.sh
export strainList="bl6 c3h cast caroli"
export lceCollab=/nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration
source $paramFile
source ${src}/parameters_lce_${SYSNAME}.sh
$PARAMsamtools
$PARAMr
$PARAMbedtools
$PARAMperl


#######
# now move into ANALYSIS directory for next steps

cd ${analysis}
###
# Data and annotation generated in the LCE project
cp ${lceCollab}/library_metadata/${rel}/LCE_WGS_annotation_${rel}.tsv ${analysis}/
# simplify subsequent R processing of strings
cat ${analysis}/LCE_WGS_annotation_${rel}.tsv | perl -wlne 's/ //g;s/\//_/g;s/\//_/g;print' > LCE_WGS_annotation_${rel}.tsv.clean

# Produce lists of WGS library IDs
for strn in `echo ${strainList}`
do
  source ${genomeDir}/${strn}.${rel}.version
  zcat ${lceCollab}/variant_calls/${rel}/${strn}_strelka_snvs_annotated_${gnm}.vcf.gz | head -10000 | grep '^#CHROM' | perl -walne '@s=split/\s+/;foreach $r (@s){if($r=~/^do/){print"$r";}}' > ${analysis}/${strn}_strelka_snvs_${rel}.ids
done

for strn in `echo ${strainList}`
do
  source ${genomeDir}/${strn}.${rel}.version
  zcat ${lceCollab}/variant_calls/${rel}/${strn}_strelka_indels_annotated_${gnm}.vcf.gz | head -10000 | grep '^#CHROM' | perl -walne '@s=split/\s+/;foreach $r (@s){if($r=~/^do/){print"$r";}}' > ${analysis}/${strn}_strelka_indels_${rel}.ids
done


# Produce matrix files (main substrate for subsequent analysis)
# Includes a filter to only write out main-chromosome variants.
for strn in `echo ${strainList}`
do
  source ${genomeDir}/${strn}.${rel}.version
  rname="matrix"
  bsub -W 3:00 -M 2096 -R "rusage[mem=2096]" -J ${strn}.${rname} -o ${runlogs}/${strn}.matrix.o -e /dev/null "zcat ${lceCollab}/variant_calls/${rel}/${strn}_strelka_snvs_annotated_${gnm}.vcf.gz | perl -w ${src}/getContextsPerTumorVef.pl ${strn}_strelka_snvs_${rel}.ids | perl -wlne 's/^chr//;if(/^[\d+XY]/){print}' > ${analysis}/${strn}.${rel}.snvs.vef.matrix ; echo ${dstamp} > ${strn}.${rname}.hold"
done

# produce matrix files from indel calls.
for strn in `echo ${strainList}`
do
  source ${genomeDir}/${strn}.${rel}.version
  rname="indels.matrix"
  bsub -W 1:00 -M 2096 -R "rusage[mem=2096]" -J ${strn}.${rname} -o ${runlogs}/${strn}.${rname}.o -e ${logs}/${i}.${type}.${thing}.e "zcat ${lceCollab}/variant_calls/${rel}/${strn}_strelka_indels_annotated_${gnm}.vcf.gz | perl -w ${src}/getContextsPerTumorVefIndel.pl ${strn}_strelka_indels_${rel}.ids | perl -wlne 's/^chr//;if(/^[\d+XY]/){print}' > ${analysis}/${strn}.${rel}.indels.vef.matrix ; echo ${dstamp} > ${i}.${rname}.hold"
done

### Add in repeatMasker annotation
# Intersect repeat annotation with mutations - includes redundancy filter as a single site can overlap
# multiple repeats and we don't want to mutliple count mutations. Filters on concatentation of mutation
# reference and nodule ID so we keep multiple independent mutations.
type=snvs
thing=rmsk
for i in `echo ${strainList}`
  do
  rname="snvs.repAnno"
  depName="matrix"
  deps="${src}/done.sh ${strn}.${depName}.hold"
  bsub -W 4:00 -M 10000 -R "rusage[mem=10000]" -E "${deps}" -J ${i}.${type}.${thing} -o ${logs}/${i}.${type}.${thing}.o -e ${logs}/${i}.${type}.${thing}.e "bedtools intersect -wao -a ${analysis}/${i}.${rel}.${type}.vef.matrix -b ${genomeDir}/${i}.repeatMasker.bed | perl -walne '\$k=\$F[3].\$F[12];if(defined\$m{\$k}){next;}else{print;\$m{\$k}=1}' > ${analysis}/${i}.${rel}.${type}.vef.matrix.repAnno ;  echo ${dstamp} > ${i}.${rname}.hold"
done


# doesn't currently wait on previoius matrix file to be
# generated. Could do with sge like wait implementing with
# bsub -E <- now done using $src/done.sh
type=indels
thing=rmsk
for i in `echo ${strainList}`
  do
  rname=${type}.${thing}
  depName="indels.matrix"
  deps="${src}/done.sh ${strn}.${depName}.hold"
  bsub -W 4:00 -M 10000 -R "rusage[mem=10000]" -E "${deps}" -J ${i}.${type}.${thing} -o ${logs}/${i}.${type}.${thing}.o -e ${logs}/${i}.${type}.${thing}.e "bedtools intersect -wao -a ${analysis}/${i}.${rel}.${type}.vef.matrix -b ${genomeDir}/${i}.repeatMasker.bed | perl -walne '\$k=\$F[3].\$F[12];if(defined\$m{\$k}){next;}else{print;\$m{\$k}=1}' > ${analysis}/${i}.${rel}.${type}.vef.matrix.repAnno ; echo ${dstamp} > ${i}.${rname}.hold"
done

###
# Wait for all strains to catch-up


####
# Apply Familial filtering
for i in `echo ${strainList}`
  do
  rname=fFilter
  deps="${src}/done.sh ${i}.indels.rmsk.hold ${i}.snvs.repAnno.hold"
  bsub -W 3:00 -M 24000 -R "rusage[mem=24000:tmp=10]" -E "${deps}" -J ${i}.fFilter -o ${logs}/${i}.fFilter.o -e ${logs}/${i}.fFilter.e "cat ${analysis}/${i}.${rel}.indels.vef.matrix.repAnno ${analysis}/${i}.${rel}.snvs.vef.matrix.repAnno | sort -k1,1V -k2,2n > ${analysis}/${i}.${rel}.vef.matrix.repAnno ; Rscript --vanilla ${src}/familialFilter.R ${i} ; cat ${i}.${rel}.vef.matrix.repAnno | cut -f 1,2,3,4,5,6 > ${i}.allMat.bed ; echo ${dstamp} > ${i}.${rname}.hold"
done

###
# All versus all projections using Hal (based on prototyping by Craig Anderson)
export nextDeps="${src}/done.sh"
for strn in `echo ${strainList}`
do
  for strnTo in `echo ${strainList} hg19`
  do
    if [ $strn != $strnTo ]; then
      # Apply liftover using HAL system
      #strnTo="hg19"
      rname="${strn}2${strnTo}"
      deps="-E \"${src}/done.sh ${strn}.fFilter.hold\""
      deps=""
      #bsub -W 12:00 -M 8096 -R "rusage[mem=8096:tmp=10]" ${deps} -J ${rname}.hal -o ${runlogs}/${strn}2${strnTo}.hal.o -e ${runlogs}/${strn}2${strnTo}.hal.e "/nfs/leia/research/flicek/user/rayner/src/hal/bin/halLiftover --noDupes  /hps/nobackup/flicek/user/cander21/ref/liftover/1509_ca.hal ${halGenNames[$strn]} ${strn}.allMat.bed ${halGenNames[$strnTo]} ${strn}.2${strnTo}.bed ; echo ${dstamp} > ${rname}.hal.hold"
      # Calculate sequence context interval for projected coordinates
      #deps="-E \"${src}/done.sh ${rname}.hal.hold\""
      #bsub -W 2:00 -M 4096 -R "rusage[mem=4096:tmp=10]" ${deps} -J ${rname}.slop -o ${runlogs}/${rname}.slop.o -e ${runlogs}/${rname}.slop.e "bedtools slop -i ${strn}.2${strnTo}.bed -g ${genomeDir}/${strnTo}.chromSize.main -b 3 > ${strn}.slop7.2${strnTo}.bed ; echo ${dstamp} > ${rname}.slop.hold"
      # Extract sequence for sequence context
      #deps="-E \"${src}/done.sh ${rname}.slop.hold\""
      bsub -W 12:00 -M 8096 -R "rusage[mem=8096:tmp=10]" ${deps} -J ${rname}.gfa -o ${runlogs}/${rname}.gfa.o -e ${runlogs}/${rname}.gfa.e "bedtools getfasta -s -tab -name+ -fi ${genomeDir}/${strnTo}.fa -bed ${strn}.slop7.2${strnTo}.bed -fo ${strn}.slop7.2${strnTo}.seqSlice ; echo ${dstamp} > ${rname}.gfa.hold"
      # coerce into a format we can process easily in R
      deps="-E \"${src}/done.sh ${rname}.gfa.hold\""
      bsub -W 2:00 -M 14096 -R "rusage[mem=14096:tmp=10]" ${deps} -J ${rname}.post -o ${runlogs}/${rname}.post.o -e ${runlogs}/${rname}.post.e "perl -w ${src}/halProjectionStripper.pl ${strn}.slop7.2${strnTo}.seqSlice > ${strn}.slop7.2${strnTo}.toLoad ; echo ${dstamp} > ${strn}2${strnTo}.hold"
      export nextDeps="${nextDeps} ${strn}2${strnTo}.hold"
    fi
  done
done

####
# Run the ISD code
rname="isdProcessor"
deps="-E ${nextDeps}"
for strn in `echo ${strainList}`
do
  bsub -W 8:00 -M 104096 -R "rusage[mem=104096]" ${deps} -J ${rname} -o ${runlogs}/${strn}.${rname}.${dstamp}.o -e ${runlogs}/${strn}.${rname}.${dstamp}.e "Rscript --vanilla ${src}/isdPopulate1.R ${strn} 20180406 20180813 0 ; echo ${dstamp} > ${strn}.${rname}.hold"
done
