# Given VCF, extract table of:
# chr left right name 1 + from to context triName refCount altCount tumor_id sharedCount
use strict;
my ($idFile) = @ARGV;
my @idOrder = readIdFile($idFile);
# Genotype record structure in 20171103 release
# GT:VAF:DP:FDP:SDP:SUBDP:AU:CU:GU:TU
my $lastFeildOrder = "GT:VAF:DP:FDP:SDP:SUBDP:AU:CU:GU:TU";
my $nucCountPos = decodeFeildOrder($lastFeildOrder);
my (%filterGV);
while(<STDIN>){
  next if (/^#/);
  chomp;
  my @sp = split /\t/;
  if($sp[6] =~ /Germline/){
    my ($tv) = split /\|/, $sp[2];
    $filterGV{$tv}=1;
    next;
  }
  next if(defined $filterGV{$sp[2]});
  next if($sp[6] ne "PASS");
  my $ctnx = "NNNnNNN";
  my $context = "NNNnNNN";
  my $triName = "NnN_n";
  if($sp[7] =~ /CONTEXT=(\w{7,7})/){
    $ctnx = $1;
    my @csp = split //, $ctnx;
    $context = "$csp[0]$csp[1]$csp[2]$sp[3]$csp[4]$csp[5]$csp[6]";
    $triName = "$csp[2]$sp[3]$csp[4]_$sp[4]";
  }
  else {
    print STDERR "WARN: $sp[2] context not defined\n";
  }
  my @rcrd = ($sp[0],$sp[1]-1,$sp[1],$sp[2],1,"+",$sp[3],$sp[4],$context,$triName);
  my $prfx = join "\t", @rcrd;
  my @outlines = ();
  my $sharedCount = 0;
  ###
  # multinucleotide or multi-allele
  next if(length($sp[4])>1);
  ###
  if ($sp[8] ne $lastFeildOrder){
    # More efficent than recomputing each time
    print STDERR "WARN: $lastFeildOrder => $sp[8]\n";
    $nucCountPos = decodeFeildOrder($sp[8]);
    $lastFeildOrder = $sp[8];
  }
  for my $tn (9 .. $#sp){
    my $tid = $tn - 9;
    next if($sp[$tn] =~ /^\./);
    my @gtsp = split /:/, $sp[$tn];
    my ($t1,$refCount) = split /,/, $gtsp[$nucCountPos->{$sp[3]}];
    my ($t2,$altCount) = split /,/, $gtsp[$nucCountPos->{$sp[4]}];
    push @outlines, "$prfx\t$refCount\t$altCount\t$idOrder[$tid]";
    $sharedCount++;
  }
  foreach my $o (@outlines){
    print "$o\t$sharedCount\n";
  }
}

sub decodeFeildOrder {
  my ($strings) = @_;
  my @strs = split /:/, $strings;
  my %lookup = ('A' =>0, 'C' => 0, 'G' => 0, 'T' => 0);
  for my $n (0 .. $#strs){
    $lookup{'A'} = $n if ($strs[$n] eq "AU");
    $lookup{'C'} = $n if ($strs[$n] eq "CU");
    $lookup{'G'} = $n if ($strs[$n] eq "GU");
    $lookup{'T'} = $n if ($strs[$n] eq "TU");
  }
  return(\%lookup);
}

sub readIdFile {
  my ($fi) = @_;
  open(FI, "<$fi") or die "Failed to read $fi\n";
  my @ord;
  while (<FI>){
    chomp;
    my $n = $_;
    #$n =~ s/_/\t/g;
    push @ord, $n;
  }
  return(@ord);
}
