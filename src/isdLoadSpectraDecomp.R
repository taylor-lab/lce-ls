library("data.table")

cmdArgs = commandArgs(trailingOnly=TRUE)
if(!exists("cliargs")){
  if (length(cmdArgs)==0){
    cliargs<-c("bl6","20180406","20180813","0")
  }
  if (length(cmdArgs)==1){
    cliargs<-c(cmdArgs[1],"20180406","20180813","0")
  }
  if (length(cmdArgs)>1){
      cliargs<-cmdArgs
  }
}

if(exists("cliargs")){
  strain<-cliargs[1]
}

src="../src/"
genomesDir="../genomes/"
segmentsDir="./segments/"
expressionDir="./expression/"
imagesDir="./images/"
inputDir="../inputdata"

isdIn<-fread(file="LCE_integratedSampleData.csv",header=T,sep=",")
iicc<-as.vector(sapply(isdIn,typeof))
iicc[c(29:37)]="numeric"
iicc[38]="character"
# Re-read with correct class assignments
isdIn<-fread(file="LCE_integratedSampleData.csv",header=T,sep=",",colClasses=iicc)

strainList=c("bl6","cast","caroli","c3h")
for(s in 1:length(strainList)){
  strain<-strainList[s]
  sigfitFile<-paste0(inputDir,"/sigfit_all_",strain,".txt")
  sigfit<-as.matrix(fread(file=sigfitFile,sep="\t",skip=1))
  sigfitHead<-unlist(strsplit(gsub("\r","",system(paste0("head -1 ",sigfitFile),intern=T,ignore.stderr=T)),split="\t"))

  nodnames<-gsub("1\\d_","",gsub("do\\d+_","",sigfitHead,perl=T))
  donames<-gsub("_.*","",sigfitHead,perl=T)
  for(i in 1:length(donames)){
    isdIn[wgsId==donames[i]]$den1sig=as.numeric(sigfit[1,i+1])
    isdIn[wgsId==donames[i]]$den2sig=as.numeric(sigfit[2,i+1])
    isdIn[wgsId==donames[i]]$spont1sig=as.numeric(sigfit[3,i+1])
    isdIn[wgsId==donames[i]]$spont2sig=as.numeric(sigfit[4,i+1])
  }
}
fwrite(isdIn,file="LCE_isd_withSig.csv",na="NA")
