# Take the LCE histology table from Sarah and process into R readable table
# containing key data on the histology and also subsetting just to sequenced nodules.
# Also want to dump out machine readalbe data on all nodules - especially odd nodules
# from the same mice as sequenced samples as there could be interaction
###
# If you are reading this - the code is horrific! having to deal with many edge cases of
# mannual annotation by multiple invividuals over many years. Coded to at least ensure the
# processing of data is reproducible rather than manually editing Excel tables.
###
use strict;
my ($histology,$nodules,$bl,$crudeCounts,$nodExpress) = @ARGV;
my %libSanity;
open(NOD,"<$nodules") or die "Failed to read $nodules\n";
open(HIS,"<$histology") or die "Failed to read $histology\n";
open(BL,"<$bl") or die "Failed to read $bl\n";
open(CC,"<$crudeCounts") or die "Failed to read $crudeCounts\n";
open(NODEXPRESS,"<$nodExpress") or die "Failed to read $nodExpress\n";


my %subStrain = ( 'CAST_EiJ' => 'cast',
  'C3H_HeOuJ' => 'c3h',
  'CAROLI_EiJ' => 'caroli',
  'BL6' => 'bl6',
  'BL6(CTCF;AlbCreHE;HE)' => 'bl6',
  'BL6(CTCF;AlbCreHE;WT)' => 'bl6',
  'BL6CTCF(ko)HE' => 'bl6',
  'CTCF(ko)' => 'bl6',
  'C57BL/6' => 'bl6',
  'Ctcf(ko)' => 'bl6',
  'F344' => 'f344',
  'CAST/EiJ' => 'cast',
  'C3H/HeOuJ' => 'c3h',
  'CAROLI/EiJ' => 'caroli',
  'CAROLI' => 'caroli',
  'CTCF;AlbCre' => 'bl6',
  'Ctcf(lilZ); AlbCre(tg)' => 'bl6',
  );

my %genomeId = ( 'GRCm38' => 'bl6',
  'C3H' => 'c3h',
  'CAST' => 'cast',
  'CAROLI' => 'caroli'
);

my %modMale = ( 'M' => 'Male',
  'Male' => 'Male',
  'male' => 'Male',
  'female' => 'Female',
  'Female' => 'Female');


my %modTissue = ('Liver' => 'Liver',
  'liver' => 'Liver');

my %modGenotype = ( 'HE;WT' => 'HE;WT',
  'HE;HE' => 'HE;HE',
  'HE' => 'HE;HE',
  'WT' => 'HE;WT',
  'BL6CTCF(ko)HE' => 'HE:HE');

my %nodIndex;
my @outOrder = qw(nodId animalId tissue nodNumber slice sex strain age weeksPostDen genotype treatment motherId fatherId diagnosis cleanDiagnosis cause histology
tumorGrade steatosis bileDuct solid glandular cystic necrosis mitoses diameter wgsId rnaSeqId numSnvs
den1sig den2sig spont1sig spont2sig residSig sisterExchanges aneuplidy fracAsym knownDriver inflamComments);

my %expFileList;
while(<NODEXPRESS>){
  chomp;
  $expFileList{$_}=1;
}

my %doLib;
while(<CC>){
  chomp;
  my @ss = split /_/;
  my $lid = $ss[0];
  $doLib{$lid} = 1;
}

while(<HIS>){
  chomp;
  my @sp = split /\t/;
  if(/^Spreadsheet/){
    for my $j (0 ..$#sp){
      print STDERR "HIS\t$j\t$sp[$j]\n";
    }
    next;
  }
  my $nn = $sp[20];
  $nn =~ s/\s+$//; # deals with single edge case
  $nn =~ s/\s+-\s+/_/;
  $nn =~ s/\-/_slice/; # deals with single edge case
  $nn =~ s/\s+/_/;
  if($nn =~ /\d+\/(\d+\S+)/){
    $nn = $1;
  }
  ###
  # More specific edge cases with their own formatting.
  $nn = "13341_B" if($sp[20] eq "15/13341_liver tumour B");
  $nn = "13341_A" if($sp[20] eq "15/13341_liver tumour A");
  ###
  my $slice="NA";
  if($nn =~ /(^\d+_\S+)_(\d+)$/){
    $nn = "$1_slice$2";
    $slice = $2;
  }
  $nodIndex{$nn}=nodStruc($nn,\@outOrder);
  $nodIndex{$nn}->{'slice'} = $slice;
  if ($nn =~ /^(\d+)/){
    $nodIndex{$nn}->{'animalId'} = $1;
  }
  if ($nn =~ /^\d+_([ABNCT]\d+)/){
    $nodIndex{$nn}->{'nodNumber'} = $1;
  }
  #print "$sp[19]\n";
  if(!$subStrain{$sp[19]}){
    print STDERR "Can not decode strain $sp[19]\n";
  }
  $nodIndex{$nn}->{'strain'} = $subStrain{$sp[19]};
  my $gt = ".";
  $gt = "CTCFHet" if($sp[19]=~/AlbCreHE;HE/);
  $gt = "CTCFWT" if($sp[19]=~/AlbCreHE;WT/);
  $nodIndex{$nn}->{'genotype'} = $gt;
  if(defined $sp[33]){
    if ($sp[33] eq "x"){
      $nodIndex{$nn}->{'cause'} = "spontaneous";
    }
  }
  if(defined $sp[35]){
    $nodIndex{$nn}->{'diagnosis'} = $sp[35];
    if($sp[35] =~ /^(\w+)\s/){
      $nodIndex{$nn}->{'cleanDiagnosis'} = $1;
    }
    else {
      $nodIndex{$nn}->{'cleanDiagnosis'} = $sp[35];
    }
  }
  $nodIndex{$nn}->{'histology'} = 1;
  $nodIndex{$nn}->{'tumorGrade'} = $sp[36];
  if(defined $nodIndex{$nn}->{'tumorGrade'}){
    if($nodIndex{$nn}->{'tumorGrade'} eq "Moderately"){
      $nodIndex{$nn}->{'tumorGrade'} = "Moderate";
    }
  }
  $nodIndex{$nn}->{'steatosis'} = $sp[37];
  $nodIndex{$nn}->{'bileDuct'} = $sp[38];
  $nodIndex{$nn}->{'solid'} = $sp[39];
  $nodIndex{$nn}->{'glandular'} = $sp[40];
  $nodIndex{$nn}->{'cystic'} = $sp[41];
  $nodIndex{$nn}->{'necrosis'} = $sp[42];
  $nodIndex{$nn}->{'mitoses'} = $sp[44];
  $nodIndex{$nn}->{'diameter'} = $sp[45];
  $nodIndex{$nn}->{'sex'} = $sp[21];
  $nodIndex{$nn}->{'age'} = $sp[22];
  if(defined $sp[6]){
    $nodIndex{$nn}->{'tissue'} = $sp[6];
  }
  if($sp[31] =~ /(\d+)\s*weeks\s+post/){
    $nodIndex{$nn}->{'weeksPostDen'} = $1;
  }
  $nodIndex{$nn}->{'inflamComments'} = $sp[81];
}

while(<BL>){
  chomp;
  my @sp = split /\t/;
  if(/^Use/){
    for my $j (0 ..$#sp){
      print STDERR "BL6\t$j\t$sp[$j]\n";
    }
    next;
  }
  next if (!$sp[3]);

  my $nn = $sp[3];
  $nn =~ s/\s+/_/g;
  #$nn =~ s/^14\///;
  #$nn =~ s/^15\///;
  if($nn =~ /\d+\/(\d+)/){
    $nn = $1;
  } else {
    next;
  }
  $nn =~ s/_$//;
  if(!$subStrain{$sp[4]}){
    print STDERR "Can not decode strain $sp[4]\n";
    next;
  }
  my $nodNum = $sp[21];
  if($sp[21]){
    if($sp[21]=~/^([CN]\d+)/){
      $nodNum = $1;
    }
  }
  my $anId = $nn;
  $anId =~ s/_\S+//;
  $nn .= "_$nodNum";
  $nodIndex{$nn}=nodStruc($nn,\@outOrder);
  $nodIndex{$nn}->{'animalId'} = $anId;
  $nodIndex{$nn}->{'nodNumber'} = $nodNum;
  $nodIndex{$nn}->{'strain'} = $subStrain{$sp[4]};
  my $gt = $sp[5];
  #$gt = "CTCFHet" if($sp[4]=~/AlbCreHE;HE/);
  #$gt = "CTCFWT" if($sp[4]=~/AlbCreHE;WT/);
  $nodIndex{$nn}->{'genotype'} = $gt;
  #$nodIndex{$nn}->{'treatment'} = "none";
  if(defined $sp[49]){
    $nodIndex{$nn}->{'diagnosis'} = $sp[49];
    if($sp[35] =~ /^(\w+)\s/){
      $nodIndex{$nn}->{'cleanDiagnosis'} = $1;
    }
    else {
      $nodIndex{$nn}->{'cleanDiagnosis'} = $sp[49];
    }
  }
  if(defined $sp[21]){
    if($sp[21]=~/^[NC]\d+$/){
      $nodIndex{$nn}->{'tissue'} = "Liver";
    }
    else {
      $sp[21] =~ s/\s+/_/g;
      $nodIndex{$nn}->{'tissue'} = $sp[21];
    }
  }
  $nodIndex{$nn}->{'tumorGrade'} = $sp[50];
  $nodIndex{$nn}->{'steatosis'} = $sp[51];
  $nodIndex{$nn}->{'bileDuct'} = $sp[52];
  $nodIndex{$nn}->{'solid'} = $sp[53];
  $nodIndex{$nn}->{'glandular'} = $sp[54];
  $nodIndex{$nn}->{'cystic'} = $sp[55];
  $nodIndex{$nn}->{'necrosis'} = $sp[56];
  $nodIndex{$nn}->{'mitoses'} = $sp[58];
  $nodIndex{$nn}->{'diameter'} = $sp[59];
  $nodIndex{$nn}->{'sex'} = $modMale{$sp[6]};
  $nodIndex{$nn}->{'weeksPostDen'} = $sp[11];
  $nodIndex{$nn}->{'inflamComments'} = $sp[62];
  if(defined $sp[8]){
    if($sp[8]=~ /\d\d\/\d\d\/\d\d\d\d/){
      $nodIndex{$nn}->{'treatment'} = "DEN";
    }
  }
}


my @outTemplate = ();
for my $hd (0 .. $#outOrder){
  $outTemplate[$hd] = "NA";
}

while(<NOD>){
  chomp;
  my @sp = split /\t/;
  if($sp[0]=~/^Library/){
    for my $j (0 ..$#sp){
      print STDERR "NOD\t$j\t$sp[$j]\n";
    }
    next;
  }
  if(!$doLib{$sp[0]}){
    # Library not used / failed
    next;
  }
  $libSanity{$sp[0]}=0;
  my $nn = $sp[5];
  if ($nn =~ /1\d_(\d+_[NCT]\d+.*)/){
    $nn = $1;
  }
  if ($nn =~ /1\d_(\d+)_([AB])/){
    # single mouse edge case (spontaneous CAST)
    $nn = $1."_".$2;

  }
  if($nn =~ /^\d+_N\d+_\d$/){
    my @sq = split /_/, $nn;
    $nn = "$sq[0]_$sq[1]_slice$sq[2]";
  }
  if($sp[5] =~ /^1\d_(\d+)$/){
    $nn = $1;
  }
  if($nn =~ /slice/i){
    $nn =~ s/slice_/slice/;
    if ($nn =~ /\dslice/){
      $nn =~ s/slice/_slice/;
    }
  }
  if ($nn =~ /(\d+_[NCT]\d+)_(\S+)/){
    my $sliceId=$2;
    my $nodIdent = $1;
    my $newNodId = $nodIdent."_".$sliceId;
    if(!defined($nodIndex{$newNodId})){
      # Sometimes we do have per-slice histology so check for that before duplicating per slice.
      #print STDERR "QQ> $newNodId $sliceId $nodIdent\n";
      # In this case we may have multiple slices from the same nodule
      # to resolve we duplicate the existing data structure with histology info but only add in
      # the sequencing data for the slice.
      $nodIndex{$newNodId}=nodStruc($newNodId,\@outOrder);
      if(defined($nodIndex{$nodIdent})){
        # So we have histology data for nodule, need to copy in for this slice.
        for my $ky (keys %{$nodIndex{$nodIdent}}){
          $nodIndex{$newNodId}->{$ky}=$nodIndex{$nodIdent}->{$ky};
        }
        # Don't want to overwrite the new nodId
        $nodIndex{$newNodId}->{'nodId'}=$newNodId;
      }
    }
    $nodIndex{$newNodId}->{'slice'}=$sliceId;
    $nn = $newNodId;
  }
  if(!$nodIndex{$nn}){
    my $newFlag = 0;
    if($nn =~ /(\d+)_T/){
      my $anId = $1;
      # tail or ear sample don't expect any histology.
      $nodIndex{$nn}=nodStruc($nn,\@outOrder);
      $newFlag = 1;
      $nodIndex{$nn}->{'animalId'}=$anId;
    }
    if($nn =~ /^1\d+_(\d+)$/){
      # normal sample so don't expect histology.
      my $anId = $1;
      $nn = $anId;
      #print STDERR "WW:$nn -> $anId $sp[0]\n";
      $nodIndex{$anId}=nodStruc($anId,\@outOrder);
      $newFlag = 1;
      $nodIndex{$anId}->{'animalId'}=$anId;
    }
    if($nn =~ /HCCRat/){
      # rat normal so no histology
      my $anId = $nn;
      $nodIndex{$nn}=nodStruc($nn,\@outOrder);
      $newFlag = 1;
      $nodIndex{$nn}->{'animalId'}=$anId;
    }
    if($nn =~ /^(\d+)\.0$/){
      # normal so no histology
      my $anId = $1;
      $nodIndex{$nn}=nodStruc($nn,\@outOrder);
      $newFlag = 1;
      $nodIndex{$nn}->{'animalId'}=$anId;
    }
    if($nn =~ /^(\d+)$/){
      # normal so no histology
      my $anId = $1;
      $nodIndex{$nn}=nodStruc($nn,\@outOrder);
      $newFlag = 1;
      $nodIndex{$nn}->{'animalId'}=$anId;
    }
    if($nn =~ /^(\d+)_ear$/){
      # normal ear so no histology
      my $anId = $1;
      $nodIndex{$nn}=nodStruc($nn,\@outOrder);
      $newFlag = 1;
      $nodIndex{$nn}->{'animalId'}=$anId;
    }
    if ($newFlag == 0) {
      print STDERR "WARN: $nn is not a known sample\n";
      if ($nn =~/^(\d+)_/){
        my $anId = $1;
        $newFlag =1;
        $nodIndex{$nn}=nodStruc($nn,\@outOrder);
        $nodIndex{$nn}->{'animalId'}=$anId;
      }
      $nodIndex{$nn}->{'histology'}=0;
      $newFlag=1;
    }
    if ($newFlag == 1){
      print STDERR "NEW: $nn\n";
      my $anId = $nn;
      $anId =~ s/\..+//;
      $anId =~ s/_\.+//;
      # Capture info from WGS_annotation as we don't have histology entry.
      $nodIndex{$nn}->{'animalId'} = $anId if (!defined $nodIndex{$nn}->{'animalId'});
      if(defined $modTissue{$sp[3]}){
        $nodIndex{$nn}->{'tissue'} = $modTissue{$sp[3]};
      } else{
        $nodIndex{$nn}->{'tissue'} = $sp[3];
      }
      if ($sp[2] eq "male"){
        $nodIndex{$nn}->{'sex'} = "Male";
      }
      if ($sp[2] eq "female"){
        $nodIndex{$nn}->{'sex'} = "Female";
      }
      if($subStrain{$sp[4]}){
        $nodIndex{$nn}->{'strain'} = $subStrain{$sp[4]};
      }
      $nodIndex{$nn}->{'diagnosis'} = "normal";
      $nodIndex{$nn}->{'cleanDiagnosis'} = "normal";
    }
  }
  $sp[9] =~ s/1\d_//g;
  $sp[10] =~ s/1\d_//g;
  $nodIndex{$nn}->{'motherId'}=$sp[9];
  $nodIndex{$nn}->{'fatherId'}=$sp[10];
  if($sp[16] eq "Genome"){
    $nodIndex{$nn}->{'wgsId'}=$sp[0];
  }
  if(defined $sp[13]){
    if($sp[13]=~/DEN/){
      $nodIndex{$nn}->{'cause'}="DEN";
    }
  }
}

my @listOfNod = keys %nodIndex;
my @listOfExpFiles = keys %expFileList;
print join ",", @outOrder;
print "\n";
for my $ln (0 .. $#listOfNod){
  my @outLine = @outTemplate;
  $libSanity{$nodIndex{$listOfNod[$ln]}->{'wgsId'}}=1;
  # check for nodule matched rna-seq
  for my $exp (0 .. $#listOfExpFiles){
    if ($listOfExpFiles[$exp] =~ /_$nodIndex{$listOfNod[$ln]}->{'animalId'}_/){
      if ($listOfExpFiles[$exp] =~ /_$nodIndex{$listOfNod[$ln]}->{'nodNumber'}_/){
        my @sss = split /_/, $listOfExpFiles[$exp];
        if($nodIndex{$listOfNod[$ln]} =~ /slice(\d+)/){
          my $sliceNum = $1;
          if ($listOfExpFiles[$exp] =~ /_$nodIndex{$listOfNod[$ln]}->{'nodNumber'}_$sliceNum\_/){
            $nodIndex{$listOfNod[$ln]}->{'rnaSeqId'}= $sss[0];
          }
        } else {
          $nodIndex{$listOfNod[$ln]}->{'rnaSeqId'}= $sss[0];
        }
      }
      elsif ($nodIndex{$listOfNod[$ln]}->{'cleanDiagnosis'} eq "normal"){
        my @sss = split /_/, $listOfExpFiles[$exp];
        if(defined $genomeId{$sss[4]}){
          # should only match genome ID in field 4 when not a nodule.
          $nodIndex{$listOfNod[$ln]}->{'rnaSeqId'}= $sss[0];
        }
      }
    }
  }
  if(defined $nodIndex{$listOfNod[$ln]}->{'genotype'}){
    if(defined $modGenotype{$nodIndex{$listOfNod[$ln]}->{'genotype'}}){
      $nodIndex{$listOfNod[$ln]}->{'genotype'} = $modGenotype{$nodIndex{$listOfNod[$ln]}->{'genotype'}};
    }
  }
  if(defined $nodIndex{$listOfNod[$ln]}->{'tissue'}){
    if(defined $modGenotype{$nodIndex{$listOfNod[$ln]}->{'tissue'}}){
      $nodIndex{$listOfNod[$ln]}->{'tissue'} = $modGenotype{$nodIndex{$listOfNod[$ln]}->{'tissue'}};
    }
  }
  for my $hd (0 .. $#outOrder){
    if($nodIndex{$listOfNod[$ln]}->{$outOrder[$hd]}){
      # clean up quoting and punctuation in free text feilds.
      $nodIndex{$listOfNod[$ln]}->{$outOrder[$hd]} =~ s/"//g;
      $nodIndex{$listOfNod[$ln]}->{$outOrder[$hd]} =~ s/,//g;
      $outLine[$hd] = $nodIndex{$listOfNod[$ln]}->{$outOrder[$hd]}
    }
  }
  print join ",", (@outLine);
  print "\n";
}

my @ko = keys %doLib;
for my $ln (0 .. $#ko){
  if ($libSanity{$ko[$ln]}==0){
    print STDERR "$ko[$ln] was not reported\n";
  }
}

sub nodStruc {
  my($name,$headersRef) = @_;
  my %nd;
  for my $i (0 .. $#{$headersRef}){
    $nd{$headersRef->[$i]} = 'NA';
  }
  $nd{'nodId'} = $name;
  return(\%nd);
}
