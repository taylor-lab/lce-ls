# from http://www.ensembl.org/info/docs/tools/vep/vep_formats.html#input
#1   881907    881906    -/C   +
#5   140532    140532    T/C   +
#12  1017956   1017956   T/A   +
#2   946507    946507    G/C   +
#14  19584687  19584687  C/T   -
#19  66520     66520     G/A   +    var1
#8   150029    150029    A/T   +    var2
#An insertion (of any size) is indicated by start coordinate = end coordinate + 1. For example, an insertion of 'C' between nucleotides 12600 and 12601 on the forward strand of chromosome 8 is indicated as follows:
#A deletion is indicated by the exact nucleotide coordinates. For example, a three base pair deletion of nucleotides 12600, 12601, and 12602 of the reverse strand of chromosome 8 will be:

# Take a stream of muIds
while(<STDIN>){
  chomp;
  my $id = $_;
  if($id =~ /^(\S+):(\d+)_(\S+)\/(\S+)/){
    my $chr = $1;
    my $pos = $2;
    my $ref = $3;
    my $alt = $4;
    my $left = $pos;
    my $right = $pos;
    if(length($ref)>1){
      $right = $left+(length($ref)-1);
      $left+=1;
      $alt="-";
      $ref=~s/^.//;
    }
    if(length($alt)>1){
      $left=$pos+1;
      $ref="-";
      $alt=~s/^.//;
    }
    print join "\t", ($chr,$left,$right,"$ref/$alt","+",$id);
    print "\n";
  }
}
