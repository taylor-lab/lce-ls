#cliargs<-c("bl6","20180406","20180813","0")

src="../src/"
source(paste0(src,"productionInit.R"))


if(length(cliargs)<1){
  cliargs<-c("c3h","c3h.fnodExchanges.bed","c3h.shuffleMerge",1000,"c3h.sisterExchangesObvsVsShuff.tab")
}
# cliargs<-c("c3h","c3h.fnodExchanges.bed","c3h.shuffleMerge",1000,"c3h.sisterExchangesObvsVsShuff20191129.tab")
strn<-cliargs[1]
obsFile=cliargs[2]
shuffFile=cliargs[3]
numShuffles=as.numeric(cliargs[4])
outFile=cliargs[5]
strain<-strn

source(paste0(src,"doChromOffsets.R"))

##
# compartment
cmprtmnt<-fread(file="c3h.projected.liverHiC.eigen.tsv")
names(cmprtmnt)<-c("chr","left","right","score")
####
# Get repeats
repeats<-fread(paste0(genomesDir,strain,".repeatMasker.bed"))
names(repeats)<-c("chr","left","right","name","score","strand")
repeats$left=repeats$left+1
####


####
# Get ARC blacklist regions
arc<-fread(paste0(genomesDir,strain,".arc.blacklist.bed"))
names(arc)<-c("chr","left","right")
####

####
# Get expression averages per transcript - grabbed from transcriptionCoupledRepair.R
# genic spans
gtf<-fread(paste0(genomesDir,strain,".gtf.tsv"))
names(gtf)<-c("chr","left","right","geneId","score","strand","geneName","tranId","biotype","unit")
gtf[,chr:=as.character(chr)]
genes<-gtf[unit=="gene" & biotype=="protein_coding",]
# expression
kallistoFiles<-Sys.glob(paste0(genomesDir,"expression_15day/","*_",strain,"_*","kallisto_tpm.tsv"))
kf<-fread(kallistoFiles[1])
kexp<-data.table(geneId=kf$gene_id,transId=kf$target_id)
kmean<-kexp
for(i in 1:length(kallistoFiles)){
  kf<-fread(kallistoFiles[i])
  kexp<-cbind(kexp,kf$tpm,deparse.level=0)
}
kmean$mean<-apply(kexp[,3:(length(kallistoFiles)+2)],1,mean)
kmean$sd<-apply(kexp[,3:(length(kallistoFiles)+2)],1,sd)
tpmPerGene<-kmean[,.(sum(mean)),geneId]
genes$tpm15d<-tpmPerGene$V1[match(genes$geneId,tpmPerGene$geneId)]
qthresh<-quantile(genes$tpm15d,probs=c(.25,.5,.75),na.rm=T)
####



ex<-fread(file=obsFile)
exs<-fread(file=shuffFile)
ln<-dim(ex)[1]
names(ex)<-c("chr","left","right","nodId","score","strand")
names(exs)<-names(ex)

m<-matrix(0,numShuffles,ln)
for(i in 1:numShuffles){
  m[i,]=i
}
setkey(ex,chr,left,right)
setkey(arc,chr,left,right)
exs[,rep:=1]
exs$rep=as.vector(t(m))


####
# Deal with ARC masking
totalSizeArcMasked<-arc[,.(right-left)][,sum(V1)]
genomeLength<-sum(chromOffsets$length)
fractionArc<-totalSizeArcMasked/genomeLength
exArc<-foverlaps(ex,arc,type="within")
fractionExArc<-exArc[!is.na(left),.N]/exArc[,.N]
# Filter for those projecting into ARC masked regions
exNoMask<-exArc[is.na(left),.(chr,i.left,i.right,nodId,score,strand)]
names(exNoMask)<-names(ex)
setkey(exNoMask,chr,left,right)
singleRepToMask<-exArc[!is.na(left),which=T]
v<-rep(F,ln)
v[exArc[!is.na(left),which=T]]=T
vv<-rep(v,numShuffles)
exs[,arcMask:=0]
exs$arcMask[vv]=1
setkey(exs,chr,left,right)



# genes overlaps
setkey(genes,chr,left,right)
exNmGenes<-foverlaps(exNoMask,genes,type="within")[!is.na(left),.N]
exGenes<-foverlaps(ex,genes,type="within")[!is.na(left),.N]
exs.genes<-foverlaps(exs,genes,type="within")[!is.na(left),.N,by=rep]
exsNm.genes<-foverlaps(exs,genes,type="within")[!is.na(left) & arcMask==0,.N,by=rep]
dtNmGenes<-data.table(t(c("Genes",1,exNmGenes,exsNm.genes$N)))
dtGenes<-data.table(t(c("Genes",0,exGenes,exs.genes$N)))
dtNmGenes<-data.table(t(c("Genes",1,exNmGenes,exsNm.genes$N)))
dtGenes<-data.table(t(c("Genes",0,exGenes,exs.genes$N)))


# active genes
expThresh<-as.numeric(qthresh[2])
exNmGenesActive<-foverlaps(exNoMask,genes,type="within")[!is.na(left) & tpm15d>=expThresh,.N]
exGenesActive<-foverlaps(ex,genes,type="within")[!is.na(left) & tpm15d>=expThresh,.N]
exs.genesActive<-foverlaps(exs,genes,type="within")[!is.na(left) & tpm15d>=expThresh,.N,by=rep]
exsNm.genesActive<-foverlaps(exs,genes,type="within")[!is.na(left) & arcMask==0 & tpm15d>=expThresh,.N,by=rep]
dtNmGenesActive<-data.table(t(c("Genes (active)",1,exNmGenesActive,exsNm.genesActive$N)))
dtGenesActive<-data.table(t(c("Genes (active)",0,exGenesActive,exs.genesActive$N)))

# inactive genes
expThresh<-as.numeric(qthresh[2])
exNmGenesInactive<-foverlaps(exNoMask,genes,type="within")[!is.na(left) & tpm15d<=expThresh,.N]
exGenesInactive<-foverlaps(ex,genes,type="within")[!is.na(left) & tpm15d<=expThresh,.N]
exs.genesInactive<-foverlaps(exs,genes,type="within")[!is.na(left) & tpm15d<=expThresh,.N,by=rep]
exsNm.genesInactive<-foverlaps(exs,genes,type="within")[!is.na(left) & arcMask==0 & tpm15d<=expThresh,.N,by=rep]
dtNmGenesInactive<-data.table(t(c("Genes (inactive)",1,exNmGenesInactive,exsNm.genesInactive$N)))
dtGenesInactive<-data.table(t(c("Genes (inactive)",0,exGenesInactive,exs.genesInactive$N)))


# repeats
setkey(repeats,chr,left,right)
exNmRepeats<-foverlaps(exNoMask,repeats,type="within")[!is.na(left),.N]
exRepeats<-foverlaps(ex,repeats,type="within")[!is.na(left),.N]

exs.repeats<-foverlaps(exs,repeats,type="within")[!is.na(left),.N,by=rep]
exsNm.repeats<-foverlaps(exs,repeats,type="within")[!is.na(left) & arcMask==0,.N,by=rep]
dtNmRepeats<-data.table(t(c("Repeats",1,exNmRepeats,exsNm.repeats$N)))
dtRepeats<-data.table(t(c("Repeats",0,exRepeats,exs.repeats$N)))


compA<-cmprtmnt[!is.na(score) & score>0,]
setkey(compA,chr,left,right)
exNmCompA<-foverlaps(exNoMask,compA,type="within")[!is.na(left),.N]
exCompA<-foverlaps(ex,compA,type="within")[!is.na(left),.N]

exs.compA<-foverlaps(exs,compA,type="within")[!is.na(left),.N,by=rep]
exsNm.compA<-foverlaps(exs,compA,type="within")[!is.na(left) & arcMask==0,.N,by=rep]
dtNmCompA<-data.table(t(c("Comp.A",1,exNmCompA,exsNm.compA$N)))
dtCompA<-data.table(t(c("Comp.A",0,exCompA,exs.compA$N)))
### compB
compB<-cmprtmnt[!is.na(score) & score<0,]
setkey(compB,chr,left,right)
exNmCompB<-foverlaps(exNoMask,compB,type="within")[!is.na(left),.N]
exCompB<-foverlaps(ex,compB,type="within")[!is.na(left),.N]

exs.compB<-foverlaps(exs,compB,type="within")[!is.na(left),.N,by=rep]
exsNm.compB<-foverlaps(exs,compB,type="within")[!is.na(left) & arcMask==0,.N,by=rep]
dtNmCompB<-data.table(t(c("Comp.B",1,exNmCompB,exsNm.compB$N)))
dtCompB<-data.table(t(c("Comp.B",0,exCompB,exs.compB$N)))



####
# combine tables
obsVsShuffExch<-rbind(dtRepeats,dtNmRepeats,dtGenesInactive,dtNmGenesInactive,dtGenesActive,dtNmGenesActive,dtGenes,dtNmGenes,dtCompA,dtNmCompA,dtCompB,dtNmCompB,fill=T)
for (j in names(obsVsShuffExch)){
  # set nulls from rbind.fill above to zero
  set(obsVsShuffExch,which(is.na(obsVsShuffExch[[j]])),j,0)
}
slist<-paste0(rep("shuff",numShuffles),1:numShuffles)
names(obsVsShuffExch)<-c("name","mask","obs",slist)
fwrite(obsVsShuffExch,file="c3h.sisterExchangesObvsVsShuffCompartment20191206.Rdat",col.names=T)


#####################

compA<-cmprtmnt[!is.na(score) & score>0,]
compB<-cmprtmnt[!is.na(score) & score<0,]
setkey(compA,chr,left,right)
setkey(compB,chr,left,right)
exCompA<-foverlaps(ex,compA,type="within")[!is.na(left),.N]
exCompB<-foverlaps(ex,compB,type="within")[!is.na(left),.N]
numShuffles<-1000
exsCompA<-rep(as.numeric(NA),numShuffles)
exsCompB<-rep(as.numeric(NA),numShuffles)
auxFile<-data.table(name=c("compA","compB"),mask=c(0,0),obs=c(exCompA,exCompB))

for(i in 1:numShuffles){
  vec<-c(1:ln)+(ln*(i-1))
  exsSamp<-exs[vec]
  setkey(exsSamp,chr,left,right)

  exsCompA[i]=foverlaps(exsSamp,compA,type="within")[!is.na(left),.N]
  exsCompB[i]=foverlaps(exsSamp,compB,type="within")[!is.na(left),.N]
  cname<-paste0("shuff",i)
  auxFile<-cbind(auxFile,c(exsCompA[i],exsCompB[i]))
  print(i)
}
fwrite(auxFile,file="c3h.compartmentProjected.tab")
#####
#
