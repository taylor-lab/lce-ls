# Given VCF, extract table of:
# chr left right name 1 + from to context triName refCount altCount tumor_id sharedCount
use strict;
my ($idFile) = @ARGV;
my @idOrder = readIdFile($idFile);
# Genotype record structure in 20171103 release
# GT:VAF:DP:FDP:SDP:SUBDP:AU:CU:GU:TU for indels -> GT:DP:DP2:TAR:TIR:TOR:DP50:FDP50:SUBDP50:BCN50
my $lastFeildOrder = "GT:DP:DP2:TAR:TIR:TOR:DP50:FDP50:SUBDP50:BCN50";
my ($dpos,$apos) = decodeFeildOrderIndel($lastFeildOrder);
my (%filterGV);
while(<STDIN>){
  next if (/^#/);
  chomp;
  my @sp = split /\t/;
  if($sp[6] =~ /Germline/){
    my ($tv) = split /\|/, $sp[2];
    $filterGV{$tv}=1;
    next;
  }
  next if(defined $filterGV{$sp[2]});
  next if($sp[6] ne "PASS");
  my $ctnx = "NNNnNNN";
  my $context = "NNNnNNN";
  # triName now used to code size and direction of change
  my $triName = "NnN_n";
  if($sp[7] =~ /CONTEXT=(\w+)/){
    $ctnx = $1;
    my $ctnxStart = substr($ctnx,0,3);
    my $ctnxEnd = substr($ctnx,-3,3);
    my @csp = split //, $ctnx;
    $context = "$ctnxStart:$ctnxEnd";
    $triName = length($sp[4])-length($sp[3]);
  }
  else {
    print STDERR "WARN: $sp[2] context not defined\n";
  }
  my @csq = split /;CSQ=/, $sp[7];
  my($genes,$vefs) = (".",".");
  if($#csq >0){
    ($genes,$vefs) = readVef($csq[1]);
  }
  my @rcrd = ($sp[0],$sp[1]-1,$sp[1],$sp[2],1,"+",$sp[3],$sp[4],$context,$triName);
  my $prfx = join "\t", @rcrd;
  my @outlines = ();
  my $sharedCount = 0;
  ###
  # multinucleotide or multi-allele
  #next if(length($sp[4])>1);
  ###
  if ($sp[8] ne $lastFeildOrder){
    # More efficent than recomputing each time
    print STDERR "WARN: $lastFeildOrder => $sp[8]\n";
    ($dpos,$apos) = decodeFeildOrderIndel($sp[8]);
    $lastFeildOrder = $sp[8];
  }
  for my $tn (9 .. $#sp){
    my $tid = $tn - 9;
    next if($sp[$tn] =~ /^\./);
    my @gtsp = split /:/, $sp[$tn];
    my $refCount = $gtsp[$dpos];
    my ($altCount,$t2) = split /,/, $gtsp[$apos];
    $refCount = $refCount - $altCount;
    my ($au,$cu,$gu,$tu) = (0,0,0,0);
    push @outlines, join "\t", ($prfx,$refCount,$altCount,$idOrder[$tid],$genes,$vefs,$au,$cu,$gu,$tu);
    $sharedCount++;
  }
  foreach my $o (@outlines){
    print "$o\t$sharedCount\n";
  }
}

sub readVef {
  my ($vec) = @_;
  my @eachAnno = split /,/, $vec;
  my (%genes,$topImpact);
  $topImpact = 0;
  my %impact = ('MODIFIER' => 1,
    'LOW' => 2,
    'MODERATE' => 3,
    'HIGH' => 4);
  foreach my $ea (@eachAnno){
    my @easp = split /\|/, $ea;

    my $geneName = ".";
    if (defined $easp[3]){
      $geneName = $easp[3];
    }
    elsif (defined $easp[4]){
      $geneName = $easp[4];
    }
    $genes{$geneName} = 1;
    if (defined $impact{$easp[2]}){
      $topImpact = $impact{$easp[2]} if ($impact{$easp[2]}>$topImpact);
    }
  }
  my $geneList = join ",", keys %genes;
  return($geneList,$topImpact);
}

sub decodeFeildOrderIndel {
  my ($strings) = @_;
  my @strs = split /:/, $strings;
  my $readDepth = 0;
  my $altDepth = 0;
  for my $n (0 .. $#strs){
    if ($strs[$n] eq "DP"){
      $readDepth=$n;
    }
    if ($strs[$n] eq "TAR"){
      $altDepth=$n;
    }
  }
  return($readDepth,$altDepth);
}

sub readIdFile {
  my ($fi) = @_;
  open(FI, "<$fi") or die "Failed to read $fi\n";
  my @ord;
  while (<FI>){
    chomp;
    my $n = $_;
    #$n =~ s/_/\t/g;
    push @ord, $n;
  }
  return(@ord);
}
