####
# Load functions
source(file="../src/lce_taylor_functions.R")
args<-commandArgs(trailingOnly=TRUE)
strain<-args[1]
freeze<-args[2]
#strain<-"cast"
####
#strain<-"bl6"
#freeze<-20171208
####
# Big matrix (4GB RAM for c3h)
mat<-read.table(file=paste(strain,freeze,"matrix",sep="."),header=F,sep="\t",colClasses=c('character','integer','integer','character','integer','character','factor','factor','factor','character','integer','integer','factor','integer'))
####

mu<-generateMuClasses()
mat$class<-factor(mat$V10,levels=mu$forward)
#id<-read.table(file="c3h_strelka_snvs_20171103.ids",header=F)
# There are "#" characters in some IDs, need to disable comment processing
animalMagic<-read.table(file=paste("LCE_WGS_annotation_",freeze,".tsv",sep=""),sep="\t",header=T,comment.char="")
# in the 20171208 release _ were substiuted for spaces in re-run libaries in this tsv file.
animalMagic$Sample.ID=gsub(" ","_",animalMagic$Sample.ID)
animalMagic$Sample.ID=gsub("/","_",animalMagic$Sample.ID)
animalMagic$cName<-apply(animalMagic,1,function(x){paste(x[1],x[8],sep="_")})


######
# repeat with collapsed animalMagic
t<-table(mat$V13)
tmrt<-data.frame(cName=as.factor(names(t)),muCount=as.vector(t),lib=as.character(NA),exp=as.character(NA),facil=as.character(NA),strain=as.character(NA),sample=as.character(NA),treat=as.character(NA),individual=as.character(NA),diagnosis=as.character(NA),totalReads=0,pcMapped=0,pcUqmapped=0)
ln<-dim(tmrt)[1]
lord<-rep(0,ln)
for(i in c(1:ln)){
  mtch<-which(tmrt$cName[i]==animalMagic$cName)
  lord[i]=mtch[1]
  tmrt$totalReads[i]=sum(as.numeric(animalMagic$Total.Reads.Passed.PF[mtch]))
  tmrt$pcMapped[i]=mean(as.numeric(animalMagic$Max.Percent.Mapped[mtch]))
  tmrt$pcUqmapped[i]=mean(as.numeric(animalMagic$Max.Percent.Uniquely.Mapped[mtch]))
}
tmr<-data.frame(cName=tmrt$cName,muCount=tmrt$muCount,
  lib=animalMagic$Library[lord],
  exp=animalMagic$Experiment[lord],
  facil=animalMagic$Facility[lord],
  strain=animalMagic$Strain[lord],
  sample=animalMagic$Sample.ID[lord],
  treat=animalMagic$Treatments[lord],
  individual=animalMagic$Individual[lord],
  diagnosis=animalMagic$Diagnosis[lord],
  totalReads=tmrt$totalReads,
  pcMapped=tmrt$pcMapped,
  pcUqmapped=tmrt$pcUqmapped)
tmr$assumedDen<-0
tmr$assumedDen[grep("DEN",tmr$treat)]=1
tmr$assumedDen[grep("_N",tmr$cName)]=1
tmr$assumedDen[which(tmr$diagnosis=="normal")]=0

dm<-tmr[which(tmr$assumedDen==1),]
fit<-aov(muCount ~ individual, data=dm)
summary(fit)

listOfIndi<-data.frame(indi=unique(dm$individual),count=0,order=0,xpos=0)
lnLoi<-dim(listOfIndi)[1]
for(i in c(1:length(listOfIndi$indi))){
  listOfIndi$count[i]=length(which(dm$individual==listOfIndi$indi[i]))
}
listOfIndi$order=(lnLoi+1)-rank(as.numeric(listOfIndi$count),ties="random")
cxpos=0
for(i in c(1:length(listOfIndi$indi))){
  j<-which(listOfIndi$order==i)
  listOfIndi$xpos[j]=cxpos+1
  cxpos=cxpos+listOfIndi$count[j]
}
svg(file=paste(strain,freeze,"muCountByIndividual.svg",sep="."),width=8,height=3)
par(mar=c(4,5,5,2))
plot(c(1,cxpos),c(0,max(dm$muCount)),type="n",xlab="Samples grouped by individual",ylab="Mutation count",main=strain,axes=F)
axis(1)
axis(2,cex.axis=0.8,las=1)
for(i in c(1:length(listOfIndi$indi))){
  j<-which(listOfIndi$order==i)
  trgts<-which(dm$individual==listOfIndi$indi[j])
  mcounts<-sort(dm$muCount[trgts])
  abline(v=listOfIndi$xpos[j]+length(mcounts)-0.5,col="salmon")
  points(c(0:(length(mcounts)-1)+listOfIndi$xpos[j]),mcounts,pch=20)
  points(c(0:(length(mcounts)-1)+listOfIndi$xpos[j]),mcounts,type="l",col="grey")
}
dev.off()
# Want a distance/similarity matrix between tumors of same animal
# Not sure yet how to plot (star tree?)

####
# Find pairs/groups of tumors from the same animal
