source("analysisInit.R")
strn<-strain
#Contains all collected samples
ccfss<-c(rep("character",7),rep("numeric",2),rep("character",19),"integer",rep("numeric",5),"integer",rep("numeric",2),"character","character")
fullSampleSet<-fread("LCE_isd_withSig.csv",header=T,sep=",",fill=T, na.strings=c("NA","","n/a","NULL"),colClasses=ccfss)
#Just samples with WGS data
wgsSamples<-fullSampleSet[!is.na(wgsId) & strain==strn,]
iicc<-as.vector(sapply(wgsSamples,typeof))
wgsSamples[is.na(cause),cause:=""]


wgsSamples[is.na(cause),cause:=""]
normals<-wgsSamples[strain==strn & cleanDiagnosis=="normal" & cause!="DEN",.(nodId,wgsId,tissue,motherId)]

readFirst<-fread(paste0(genomesDir,normals$wgsId[1],".1kbCov"))
normMat<-matrix(0,dim(readFirst)[1],dim(normals)[1])
for(i in 1:dim(normals)[1]){
  readTemp<-fread(paste0(genomesDir,normals$wgsId[i],".1kbCov"))
  normMat[,i]=readTemp$V4
}


###
# Normalise coverage over genome
colTotals<-colSums(normMat)
normCov<-t((t(normMat)/colTotals)*1e9)
autosomal<-which(readFirst$V1!="X")
chrX<-which(readFirst$V1=="X")
normCov[chrX,]=normCov[chrX,]*2
maskMat<-matrix(0,dim(normCov)[1],dim(normCov)[2])
for(i in 1:dim(normCov)[2]){
  mask<-which(normCov[,i]==0 | normCov[,i]>median(normCov[,i])*10)
  normCov[mask,i]=NA
  sds<-sd(log10(normCov[,1]),na.rm=T)
  sdm<-mean(log10(normCov[,1]),na.rm=T)
  lowerBound<-sdm-(sds*2)
  upperBound<-sdm+(sds*2)
  maskMat[which(log10(normCov[,1])<lowerBound),i]=1
  maskMat[which(log10(normCov[,1])>upperBound),i]=1
  maskMat[mask,i]=1
}
arcSegs<-readFirst[which(rowSums(maskMat)>=dim(maskMat)[2]),c(1,2,3)]
#readFirst[V1=="X",.N]
#arcSegs[V1=="X",.N]
fwrite(arcSegs,file=paste0(strain,".arcToMerge.bed"),sep="\t",col.names=F)
bedmergeConstruct<-paste0("bedtools merge -i ",strain,".arcToMerge.bed > ",strain,".arc.blacklist.bed")
system(bedmergeConstruct,ignore.stderr=T,intern=F)
