cliargs<-commandArgs(trailingOnly=TRUE)
# expects to be run from integrationDir
source("../src/lceRbasics.R")
source(paste0(src,"nodulePlotterFunctions.R"))

#cliargs<-c("c3h","figMaDraft4.pdf")
strn<-cliargs[1]
strain<-strn
outputFile<-cliargs[2]

######
# chromosomes and windows
sas<-fread(file=paste0(analysisDir,"allStrains.LCE_isdWGSs.tab"),header=T,sep=",")
source(paste0(src,"doChromOffsets.R"))
chromOffsets[,chromId:=gsub("chr","",chromId)]
xmax<-chromOffsets[chromId=="X",off+length]
genomeSpan<-xmax
kd<-fread(file=paste0(inputDir,strn,".knownDrivers"),header=F)
names(kd)<-c("muId","gene","strain")

autosomes<-chromOffsets$chromId[1:(dim(chromOffsets)[1]-1)]
w10m<-fread(file=paste(genomesDir,strain,"_10mb_windows.nucCounts",sep=""))
names(w10m)<-c("chrom","left","right","UA","UC","UG","UT")
w10m$genStart<-0
w10m$genEnd<-0
for(i in 1:dim(w10m)[1]){
  cIndex<-which(chromOffsets$chromId==w10m$chrom[i])
  w10m$genStart[i]=chromOffsets$off[cIndex]+w10m$left[i]
  w10m$genEnd[i]=chromOffsets$off[cIndex]+w10m$right[i]
}

######

if(1==1){
figmat<-matrix(0,11,27)
figmat[1:4,1:5]=1 # ma rates versus control
figmat[1:4,6:10]=2 # ma validation in WES
figmat[1:4,11:15]=3 # combinatoric reads
figmat[1:4,16:20]=4 # combinatoric reads II
figmat[1:4,21:27]=5 # combi and ma cor.
figmat[5:8,21:27]=6 # gen time hist
figmat[9:10,21:27]=7 # forrest plot

figmat[5:10,1:4]=8 # Segregation tree
figmat[5:6,5:20]=9 # segs uniform
figmat[7:8,5:20]=10 # segs het
figmat[9:10,5:20]=11 # segs zero
figmat[11,5:20]=12
}
if(1==2){
figmat<-matrix(0,10,20)
figmat[1:2,1:4]=1 # ma rates versus control
figmat[3:4,1:4]=2 # ma validation in WES
figmat[1:4,9:13]=3 # combinatoric reads
figmat[1:4,14:16]=4 # sitewise and combinatorial combination
figmat[1:3,17:20]=5 # estimate missing sepctra
figmat[4,17:20]=6
figmat[5:10,1:4]=7 # Segregation tree
figmat[5:6,5:18]=8 # segs uniform
figmat[7:8,5:18]=9 # segs het
figmat[9:10,5:18]=10 # segs zero
}

library("extrafont")
cairo_pdf(file=outputFile,width=mmScale(180),height=mmScale(130),family="Arial",fallback_resolution=600)
layout(figmat)
par(mar=c(6,4,1,2))

set.seed(1)
########
# MA rates

nodList<-sas[strain==strn & cause=="DEN" & cleanDiagnosis!="normal",nodId]
maRates<-fread(file=paste0(maDir,nodList[1],".maRatesWithProxy"))
maRateTrack<-data.table(nodId=nodList,maRate=as.numeric(NA),combiRate=as.numeric(NA))
# populate first row
maRateTrack[nodId==nodList[1],maRate:=maRates$wgsRate[1]]
for(n in 2:length(nodList)){
  # loop through nodles and load {nodId}.maRatesWithProxy files
  ratesFile<-paste0(maDir,nodList[n],".maRatesWithProxy")
  if(!file.exists(ratesFile)){
    print(ratesFile)
  } else {
    tmpRates<-fread(file=ratesFile)
    maRates<-rbind(maRates,tmpRates)
    maRateTrack[nodId==nodList[n],maRate:=tmpRates$wgsRate[1]]
  }
}

plot(1,axes=F,type="n",xlab="",ylab="",xlim=c(0,2.2),ylim=c(0,30))
mrq<-as.numeric(quantile(maRates$wgsRate*100,c(0.25,.5,0.75)))
points(c(1.2,2.2),c(mrq[2],mrq[2]),type="l",col="darkgrey",lwd=2)
polygon(c(1.5,1.9,1.9,1.5),c(mrq[1],mrq[1],mrq[3],mrq[3]),col="grey",border=NA)

mrq<-as.numeric(quantile(maRates$proxyRate*100,c(0.25,.5,0.75)))
points(c(0,1),c(mrq[2],mrq[2]),type="l",col="darkgrey",lwd=2)
polygon(c(1.5,1.9,1.9,1.5)-1,c(mrq[1],mrq[1],mrq[3],mrq[3]),col="grey",border=NA)


wgsPoints<-maRates$wgsRate[order(maRates$wgsRate)]*100
proxyPoints<-maRates$proxyRate[order(maRates$proxyRate)]*100
xrange<-((c(1:dim(maRates)[1])/dim(maRates)[1])*.7)+.15
xrange<-sample(xrange,replace=F)
points(xrange,proxyPoints,pch=20,col="#BF3EFF88",cex=0.8)
xrange<-xrange+1.2
points(xrange,wgsPoints,pch=20,col="#FF000044",cex=0.8)
axis(2,at=c(0,10,20,30),cex.axis=1.2,las=2)
axis(1,at=c(0,1),labels=c("",""))
axis(1,at=c(1.2,2.2),labels=c("",""))

########
# WES validaton
valCurves<-fread(file="wesValidationCurves.rdat")
par(mar=c(6,4,1,2))
plot(1,xlim=log10(c(2,10)),ylim=c(0,100),type="n",axes=F,xlab="",ylab="")
xp<-log10(c(1:8,9.5))
polygon(log10(c(1:8,9.5,9.5,8:1)),c(valCurves$bvpAltUpper,rev(valCurves$bvpAltLower)),col="#FF000044",border=NA)
points(xp,valCurves$vNotpAlt,type="l",col="#BF3EFF")
points(xp,valCurves$vNotnAlt,type="l",col="#68228B")
points(xp,valCurves$vnAlt,type="l",col="#000000")
points(xp,valCurves$vpAlt,type="l",col="#FF0000")
axis(1,at=c(log10(c(2:8))),labels=rep("",7),col="grey")
axis(1,at=log10(c(2,4,8)),labels=c(2,4,8))
axis(1,at=log10(c(9,10)),labels=c("",""))
axis(2,at=c(0,50,100),cex.axis=1.2,las=2)
mtext("Validation in WES (%)",side=2,line=2.5)
mtext("Support in WGS (read count)",side=1,line=2.5)
#########

#########
# Combinatorial pair
##
par(mar=c(8,2,2,2))
combiFile<-"93260_N6.14.105100158-105100191.combinations.txt"
#combiFile<-"91100_N4.12.108166725-108166798.combinations.txt"
#combiFile<-"89986_N1.11.37152164-37152190.combinations.txt"
#plot
source(paste0(src,"plotCombiReads.R"))
#garnish
points(posX,-1,col=colAN,pch="A")
points(posY,-1,col=colAN,pch="A")
axis(1,at=c(xBounds[1],posMid,xBounds[2]),labels=c("-75","chr14:105100174","+75"),cex.axis=1.2)
cp[,pairType:=paste0(variant_x,variant_y)]
pairs<-cp[doubleInfo==1,.N,by=pairType]

##
# Second reads based combintatorial example
combiFile<-"93129_N7.4.69932590-69932631.combinations.imp.txt"
#combiFile<-"91100_N4.12.108166725-108166798.combinations.txt"
#combiFile<-"89986_N1.11.37152164-37152190.combinations.txt"
#plot
source(paste0(src,"plotCombiReads.R"))
#garnish
points(posX,-1,col=colAN,pch="A")
points(posY,-1,col=colAN,pch="A")
axis(1,at=c(xBounds[1],posMid,xBounds[2]),labels=c("-75","chr14:105100174","+75"),cex.axis=1.2)
cp[,pairType:=paste0(variant_x,variant_y)]
pairs<-cp[doubleInfo==1,.N,by=pairType]


#########
# Combinatorial pair correlation - loading
if(!exists("macAg")){
  for(n in 1:length(nodList)){
    fname<-paste0(maDir,nodList[n],".MA_combos.tab")
    if(file.exists(fname)){
      mac<-fread(file=fname,header=T)
      mac[,nodId:=nodList[n]]
      if(n>1){
        macAg<-rbind(macAg,mac)
      } else {
        macAg<-mac
      }
    }
  }

  #######
  # Which generation was the progenitor cell in?
  fdm<-fread(file=paste0(maDir,nodList[1],".fnodDrcrMa"),header=T)
  hapSegsMin<-data.table(nodId=nodList,numChrXSegs=as.numeric(NA),minMA=as.numeric(NA),maxMA=as.numeric(NA),fracMA=as.numeric(NA))
  # Threshold for zero MA is set at 0.04 based on partitioning of the trimodal distribution
  # from agregated MA rate singals over all segments of C3H 371 tumours.
  # hist(fdm$maFraction,breaks=60)
  maZero<-0.04
  for(n in 1:length(nodList)){
    fname<-paste0(maDir,nodList[n],".fnodDrcrMa")
    if(file.exists(fname)){
      fdmLocal<-fread(file=fname,header=T,colClasses=as.vector(sapply(fdm,class)))
      # count chrX segments
      hapSegsMin[nodId==nodList[n],numChrXSegs:=fdmLocal[chr=="X",.N]]
      # report min MA rate
      hapSegsMin[nodId==nodList[n],minMA:=fdmLocal[chr=="X",min(maFraction,na.rm=T)]]
      hapSegsMin[nodId==nodList[n],maxMA:=fdmLocal[chr=="X",max(maFraction,na.rm=T)]]
      if(n>1){
        # concatenate
        fdm<-rbind(fdmLocal,fdm)
      }
    } else {
      print(paste0("File missing: ",fname))
    }
    #estimateSegmentCount<-sas[nodId==nodList[n],sisterExchanges]+sum(chromOffsets$expectedPloidy)
    numChrom<-20
    #Count chrX in but then subtract off all chrX segments
    estimateSegmentCount<-(sas[nodId==nodList[n],sisterExchanges]+numChrom)-fdmLocal[chr=="X",.N]
    p2<-fdmLocal[maFraction<=maZero & chr!="X",.N]/estimateSegmentCount
    # Hardy-Weinberg here.
    hapSegsMin[nodId==nodList[n],fracMA:=1-sqrt(p2)]
  }
}
########
# Combinatorial pair correlation plotting
###
# pre-calculations for mutation chromsome phasing
if(!exists("isMa")){
  macAg[,rr:=paste0(refLeft,refRight)]
  macAg[,ra:=paste0(refLeft,altRight)]
  macAg[,ar:=paste0(altLeft,refRight)]
  macAg[,aa:=paste0(altLeft,altRight)]

  isBa<-fdm[maFraction<0.04,.(nodId,chr,cStart,cEnd)]
  isBa[,numPhased:=0]
  isBa[,numImpossible:=0]
  macAg[,class:=as.character(NA)]
  for(i in 1:dim(isBa)[1]){
    #fma<-macAg[nodId==isBa$nodId[i] & chrLeft==isBa$chr[i] & posLeft>=isBa$cStart[i] & posRight<=isBa$cEnd[i] & dist >3 & dist < 150]
    macAg[nodId==isBa$nodId[i] & chrLeft==isBa$chr[i] & posLeft>=isBa$cStart[i] & posRight<=isBa$cEnd[i], class:="BA"]
    if(1==2){
      if(dim(fma)[1]>0){
        numPhasedPairs<-fma[get(aa)>1,.N]
        numImpossibleQuads<-fma[get(aa)>1 & get(rr)>1 & get(ra)>=1 & get(ar)>=1,.N]
        isBa[i,numPhased:=numPhasedPairs]
        isBa[i,numImpossible:=numImpossibleQuads]
      }
    }
  }

  isMa<-fdm[maFraction>=0.04,.(nodId,chr,cStart,cEnd)]
  isMa[,numPhased:=0]
  isMa[,numImpossible:=0]
  for(i in 1:dim(isMa)[1]){
    macAg[nodId==isMa$nodId[i] & chrLeft==isMa$chr[i] & posLeft>=isMa$cStart[i] & posRight<=isMa$cEnd[i], class:="MA"]
    if(1==2){
      if(dim(fma)[1]>0){
        numPhasedPairs<-fma[get(aa)>1,.N]
        numImpossibleQuads<-fma[get(aa)>1 & get(rr)>1 & get(ra)>=1 & get(ar)>=1,.N]
        isMa[i,numPhased:=numPhasedPairs]
        isMa[i,numImpossible:=numImpossibleQuads]
      }
    }
  }
  isBa[,sum(numPhased)]
  isBa[,sum(numImpossible)]
  isMa[,sum(numPhased)]
  isMa[,sum(numImpossible)]
}
####
# plotting correlation of MA and combinatorial pairs
# bam_name, short_name, combo1count, combo2count, combo3count, combo4count, combo5count, proportion_poss/imposs, prop_MA.
craigCombo<-fread(file=paste0(maDir,"c3h_all_combos.txt"),header=F)
names(craigCombo)<-c("bam_name", "short_name", "combo1count", "combo2count", "combo3count", "combo4count", "combo5count", "proportion", "prop_MA")
par(mar=c(4,4,1,1))
plot(1,axes=F,xlab="",ylab="",type="n",xlim=c(0,35),ylim=c(0,35))
points(craigCombo$proportion*100,craigCombo$prop_MA*100,pch=19,col="#aaaaaa99")
axis(1,at=c(0,10,20,30))
axis(2,at=c(0,10,20,30),las=2)
ct<-cor.test(craigCombo$proportion,craigCombo$prop_MA)
ctv<-round(as.numeric(ct$estimate),4)
ctp<-as.numeric(ct$p.value)
text(20,5,paste0("Pearson's cor=",ctv))
text(20,4,paste0("p-value=",ctp))

########
# Plotting generation of transformation

par(mar=c(4,5,4,1))
plot(1,xlim=c(1,0),ylim=c(0,140),axes=F,type="n",xlab="",ylab="")
brange<-seq(from=1,to=0,by=-0.05)
bfrom<-brange[1:(length(brange)-1)]
bto<-brange[2:length(brange)]
for(i in 1:length(bfrom)){
  tcount<-hapSegsMin[fracMA<=bfrom[i] & fracMA>bto[i],.N]
  #polygon(c(hleft[i],hleft[i],hright[i],hright[i]),c(0,hh$count[i],hh$count[i],0),border=NA,col="grey")
  polygon(c(bfrom[i]-.01,bfrom[i]-.01,bto[i]+.01,bto[i]+.01),c(0,tcount,tcount,0),border=NA,col="grey")
}
axis(3,at=c(0,12.5,25,50,100)/100,labels=c("0","","25","50","100"))
axis(2,at=c(0,50,100,140),las=2,cex.axis=1.2)
axis(1,at=c(0,12.5,25,50,100)/100,labels=c(">4","4","3","2","1"))
testEnrichment<-function(gfeat="Egfr",fracThresh=0.75){
  loPos<-sum(grepl(gfeat,sas[nodId %in% hapSegsMin[fracMA<fracThresh,nodId],knownDriver]))
  loNeg<-sum(!grepl(gfeat,sas[nodId %in% hapSegsMin[fracMA<fracThresh,nodId],knownDriver]))
  hiPos<-sum(grepl(gfeat,sas[nodId %in% hapSegsMin[fracMA>=fracThresh,nodId],knownDriver]))
  hiNeg<-sum(!grepl(gfeat,sas[nodId %in% hapSegsMin[fracMA>=fracThresh,nodId],knownDriver]))
  fisher.test(matrix(c(loPos,loNeg,hiPos,hiNeg),2,2))
}
testEnrichmentClean<-function(gfeat="Egfr",fracThresh=0.75){
  loPos<-sas[nodId %in% hapSegsMin[fracMA<fracThresh,nodId] & knownDriver==gfeat,.N]
  loNeg<-sas[nodId %in% hapSegsMin[fracMA<fracThresh,nodId] & knownDriver!=gfeat & !grepl(";",knownDriver),.N]
  hiPos<-sas[nodId %in% hapSegsMin[fracMA>=fracThresh,nodId] & knownDriver==gfeat,.N]
  hiNeg<-sas[nodId %in% hapSegsMin[fracMA>=fracThresh,nodId] & knownDriver!=gfeat & !grepl(";",knownDriver),.N]
  fisher.test(matrix(c(loPos,loNeg,hiPos,hiNeg),2,2))
}
listOfTargets<-c("Hras","Braf","Egfr",";")
par(mar=c(4,7,1,1))
plot(1,axes=F,type="n",xlim=c(-3.5,3.5),ylim=c(length(listOfTargets)+1,0),xlab="",ylab="")
abline(v=0,col="lightblue")
for(i in 1:length(listOfTargets)){
  te<-testEnrichment(gfeat=listOfTargets[i],fracThresh=.75)
  tec<-testEnrichmentClean(gfeat=listOfTargets[i],fracThresh=.75)
  j<-i-.1
  k<-i+.1
  points(log2(c(te$conf.int[1],te$conf.int[2])),c(j,j),type="l",col="black",lwd=2)
  points(log2(te$estimate),j,pch=20,cex=2,col="black")
  points(log2(c(tec$conf.int[1],tec$conf.int[2])),c(k,k),type="l",col="grey",lwd=2)
  points(log2(tec$estimate),k,pch=20,cex=2,col="grey")
  mtext(listOfTargets[i],side=2,at=i,line=0,las=2)
}
axis(1)

#######
# simulated segregation tree
par(mar=c(4,2,1,2))
source(paste0(src,"figMaSimSeg.R"))

#####
# Selected examples to plot
exampLow<-"91930_N2"
# "91930_N2"
exampHet<-"90797_N2"
# "90797_N2", "90887_N1", "93131_N2"
exampHigh<-"91097_N1"
#"91097_N1", "94316_N3"
source("../src/figMaSegsFunction.R")
par(mar=c(0.5,2,1,2))
plotSegs(exampHigh)
par(mar=c(0.5,2,1,2))
plotSegs(exampHet)
plotSegs(exampLow)

# axis of chromosome boundaries
par(mar=c(2,2,0,2))
plot(0,axes=F,type="n",xlab="",ylab="",xlim=c(0,genomeSpan))
nc<-length(chromOffsets$off)
cBounds<-c(chromOffsets$off,chromOffsets$off[nc]+chromOffsets$length[nc])
cMids<-chromOffsets$off+(chromOffsets$length/2)
axis(1,at=cBounds,labels=rep("",length(cBounds)))
#axis(1,at=cMids,labels=chromOffsets$chromId,tick=F,cex=axScale)
mtext(chromOffsets$chromId,at=cMids,font=2,line=1,side=1,cex=0.8,col=c("black","darkgrey"))


dev.off()
