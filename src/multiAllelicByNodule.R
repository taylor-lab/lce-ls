cmdArgs = commandArgs(trailingOnly=TRUE)
cliargs<-cmdArgs

#cliargs<-c("c3h","20180406","20180813","0")
source("analysisInit.R")


mao<-fread(paste0(strain,".multiHitVarMao.tab"))


focalNodule<-"do9853_94314_N6"
fmao<-mao[V1==focalNodule]
setkey(fmao,V2,V3)
fmat<-mat[V13==focalNodule]
setkey(fmat,V1,V3)

perChrom<-data.table(chrom=1:19,fracMulti=0,medianDepth=0,medianFracRef=0)
for(cr in 1:19){
  mu<-fmat[V1==cr,.N]
  multiMu<-fmao[V2==cr,.N]
  perChrom$fracMulti[cr]=multiMu/mu
  chromMu<-fmat[V1==cr]
  chromMu[,depth:=V11+V12]
  chromMultiMu<-fmao[V2==cr]
  chromMuUpdateDepth<-match(chromMultiMu$V3,chromMu$V3)
  chromMu$depth[chromMuUpdateDepth]=chromMultiMu$V5
  perChrom$medianDepth[cr]=median(chromMu$depth)
  perChrom$medianFracRef[cr]=median(as.numeric(unlist(chromMultiMu[,.(V6/V5)])))
}
perChrom
