module add samtools/1.5
strain=bl6
bamdir=/nfs/leia/research/flicek/user/fnc-odompipe/lce-collaboration/repository/merged_alignments/alignments_20171018
genomesdir=/hps/nobackup/flicek/user/mst/lce/genomes
coveragedir=/hps/nobackup/flicek/user/mst/lce/ANALYSIS/coverage
counter=1
for f in `ls ${strain}.*.T.A.20180802.fnod`; do
  nodName=`echo $f | perl -F'\.' -walne 'print $F[1]'`
  cat $f | grep -v '^nodule' | perl -F',' -walne 'print join "\t",($F[1],$F[4]-1,$F[5])' > ${f}.bed
  bsub -M 20096 -R "rusage[mem=20096]" -W 6:00 -o ${nodName}.w.o -e ${nodName}.w.e "samtools bedcov ${genomes}/${strain}_10mb_windows.bed ${bamdir}/${nodName}.bam > ${coveragedir}/${strain}.${nodName}.10mb.coverage.bed"
  bsub -M 20096 -R "rusage[mem=20096]" -W 4:00 -o ${nodName}.w.o -e ${nodName}.s.e "samtools bedcov ${f}.bed ${bamdir}/${nodName}.bam > ${coveragedir}/${strain}.${nodName}.seg.coverage.bed"
done
