# transcirpt tricomp files are large and require
# excessive memory in R. Subset them
# so they contain only primary and spit into
# expression quantiles.
use strict;
my($expSubsetFile,$triComp,$tcSlim) = @ARGV;

open(ES,"<$expSubsetFile") or die "Failed to read expSubsetFile $expSubsetFile\n";

my (%idx);
while(<ES>){
  chomp;
  my @sp = split/,/,$_;
  $idx{$sp[7]}=1;
}

open(TC,"<$triComp") or die "Failed to read triComp file $triComp\n";
my $ln=0;
open(OUT,">$tcSlim");
while(<TC>){
  chomp;
  if($ln==0){
    print OUT "$_\n";
    $ln=1;
    next;
  }
  my @sp = split /\s+/,$_;
  my @ssp = split /;/, $sp[3];
  my $tr=$ssp[1];
  my $slice="";
  if($ssp[1]=~/(\S+)_(\d+)$/){
    $tr=$1;
    $slice=$2;
  }
  if(defined($idx{$tr})){
    $sp[3]=join";",($ssp[0],$tr,$slice);
    print OUT join "\t", @sp;
    print OUT "\n";
  }
}
