# Take output of slopt7 slce and produced a toLoad file
use strict;

while(<>){
  chomp;
  @sp = split/\s+/;
  if($F[0]=~/::(\S+):(\d+)-\d+){
    $chr=$1;
    $pos=$2+4;
    $refN=substr($sp[1],3,1);
    $triContext=substr($sp[1],2,3);
    print join "\t",($chr,$pos,$refN,$triContext);
    print "\n";
  }
}
