use strict;
# cd /exports/igmm/eddie/TCGA_exome/EGAD00001004583/xmls/samples
my $basedir = "/exports/igmm/eddie/TCGA_exome/EGAD00001004583/";
opendir(EGAD,$basedir);
my %gotEGAR;
while(readdir(EGAD)){
  if(/^(EGAR\d+)/){
    $gotEGAR{$1}=$_;
  }
}
my %idx;
open (SD,"grep TITLE $basedir/xmls/samples/* |");
while(<SD>){
  print;
  chomp;
  if(/(EGAN\d+)\..*TITLE>([^<]*)</){
    print "$1\t$2\n";
    $idx{$1}->{'libid'} = $2;
  }
}
open(MISSING,">$basedir/kucabMissing.txt");
open(SERS,"<$basedir/delimited_maps/Study_Experiment_Run_sample.map");
while(<SERS>){
  chomp;
  my @sp = split /\t/;
  #print "$sp[14]\t$sp[11]\n";
  if(defined $idx{$sp[14]}){
    push @{$idx{$sp[14]}->{'egar'}}, $sp[11];
    if(!$gotEGAR{$sp[11]}){
      print MISSING "$sp[11]\n";
    }
  } else {
    #print STDERR "WARN: $sp[14] is not defined in samples xml\n"
  }
}
close MISSING;
open(LA,"<$basedir/libAnnotation");
my %msm;
while(<LA>){
  chomp;
  my @sp = split /\t/;
  $msm{$sp[0]} = $sp[1];
}

foreach my $egan (keys %idx){
  my $desc = "NA";
  my $k = $idx{$egan}->{'libid'};
  $k =~ s/_s\d+//;
  if(defined $msm{$k}){
    $desc = $msm{$k};
  }
  my $egarOut = join ";",@{$idx{$egan}->{'egar'}};
  print qq{$idx{$egan}->{libid}\t$egarOut\t$egan\t"$desc"\n};
}
