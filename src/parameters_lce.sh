export dstamp=`date +%Y%m%d`
export rel=20180406
export reload=0
export ensVersion=91
####
# Locations
export basedir=/hps/nobackup2/flicek/user/mst/lce
export logs=${basedir}/runlogs
export runlogs=${basedir}/runlogs
export src=${basedir}/src
export analysis=${basedir}/ANALYSIS
export coverage=${analysis}/coverage
export genomeDir=${basedir}/genomes
export inputDir=${basedir}/inputdata
# genome names
declare -A halGenNames=()
halGenNames[c3h]=C3H_HeJ
halGenNames[bl6]=C57B6J
halGenNames[cast]=CAST_EiJ
halGenNames[caroli]=CAROLI_EiJ
halGenNames[hg19]=hg19

# alias for portability between SGE and LSF
#alias bsub=bsub
#module add bedtools/2.27.0
#module add samtools/1.5
#module add R/3.5.1
#module add meme/5.0.2
alias R="R --vanilla"
