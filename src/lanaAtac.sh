#!/bin/bash
# Based extensively on Lana Talmane's ATAC-seq alignment code

module add bedtools/2.27.0 R/3.5.1 python/2.7.15 samtools/1.5

#module add igmm/apps/BEDTools/2.27.1 igmm/apps/bowtie/2.3.1 igmm/apps/meme/4.11.1 R/3.2.2 igmm/apps/samtools/1.6 python/2.7.10 igmm/apps/MACS2/2.1.1

scriptdir="/exports/igmm/eddie/taylor-lab/Lana/scripts"
fasta_one=$1
fasta_two=$2
outname=$3
genome=$4



# cut the adaptor sequences
echo "Trimming adaptors...";
#cutadapt -n3 --format=fastq --overlap=3 -g GAGATGTGTATAAGAGACAG -g CAGATGTGTATAAGAGACAG -a CTGTCTCTTATACACATCTG -a CTGTCTCTTATACACATCTC ${fasta_one} > ${fasta_one}.trim35.fastq;
#cutadapt -n3 --format=fastq --overlap=3 -g GAGATGTGTATAAGAGACAG -g CAGATGTGTATAAGAGACAG -a CTGTCTCTTATACACATCTG -a CTGTCTCTTATACACATCTC ${fasta_two} > ${fasta_two}.trim35.fastq;
echo "Done!";

# align to the genome
#bowtie2 --threads 6 -q --maxins 4000 --met-file ${outname}.bowtieMetrics -x /exports/igmm/eddie/taylor-lab/genomes/${genome}/${genome} -1 ${fasta_one}.trim35.fastq -2 ${fasta_two}.trim35.fastq -S ${outname}.doubleTrim.sam;

# quality filter and convert to .bam
samtools view -Shu -q30 -o ${outname}.doubleTrim.bam ${outname}.init.bam;
samtools flagstat ${outname}.init.bam > ${outname}.bamStats;
#rm ${outname}.doubleTrim.sam;

# sort the .bam file
samtools sort -n -m 10G -@ 4 -o ${outname}.doubleTrim.sorted.bam ${outname}.doubleTrim.bam;
samtools flagstat ${outname}.doubleTrim.bam > ${outname}.samStats;
rm ${outname}.doubleTrim.bam;

# convert to .bedpe, remove random, Unknown and mate pairs mapped to different chromosomes
bamToBed -bedpe -i ${outname}.doubleTrim.sorted.bam | grep -v "random" | grep -v "Un" | awk '{FS=OFS="\t" ; if ($1 ~ $4) print $0 }'  > ${outname}.bedpe;
allreads=`wc -l ${outname}.bedpe | cut -f1`;

# remove the reads aligned to mitochondrial genome
cat ${outname}.bedpe | grep -v "chrM" > ${outname}.noM.bedpe;
noMreads=`wc -l ${outname}.noM.bedpe | cut -f1`;
mv ${outname}.noM.bedpe ${outname}.bedpe;

# sort and deduplicate
sort -k1,1 -k2,2n -k6,6n ${outname}.bedpe | cut -f1,2,6 | uniq | awk '{FS=OFS="\t"; print $1 "\t" $2 "\t" $3 "\t" $3-$2 }' > ${outname}.fragments.bed;
dedup=`wc -l ${outname}.fragments.bed`;

# filter the fragments on the ATAc blacklist and ENCODE excludables
bedtools intersect -v -a ${outname}.fragments.bed -b  /exports/igmm/eddie/taylor-lab/Lana/scripts/JDB_blacklist.${genome}.bed  > ${outname}.fragments.temp.bed;
mv ${outname}.fragments.temp.bed ${outname}.fragments.bed;

# Separate long and short fragment
cat ${outname}.fragments.bed | awk '{FS=OFS="\t"; if ($4>100) print $0}' > ${outname}.long.fragments.bed;
cat ${outname}.fragments.bed | awk '{FS=OFS="\t"; if ($4<=100) print $0}' > ${outname}.short.fragments.bed;


# convert fragment files to .bampe
cat ${outname}.short.fragments.bed | awk -F "\t" {' print $1 "\t" $2 "\t" $2+1 "\t" $1 "\t" $3-1 "\t" $3 "\t.\t255\t+\t-" '} > ${outname}.short.fragments.bedpe;
cat ${outname}.fragments.bed | awk -F "\t" {' print $1 "\t" $2 "\t" $2+1 "\t" $1 "\t" $3-1 "\t" $3 "\t.\t255\t+\t-" '} > ${outname}.fragments.bedpe;
bedpeToBam -i ${outname}.short.fragments.bedpe -g /exports/igmm/eddie/taylor-lab/genomes/${genome}/${genome}.genome > ${outname}.short.fragments.bampe;
bedpeToBam -i ${outname}.fragments.bedpe -g /exports/igmm/eddie/taylor-lab/genomes/${genome}/${genome}.genome > ${outname}.fragments.bampe;

# call peaks for all and short fragments
macs2 callpeak -t ${outname}.short.fragments.bampe -f BAMPE -g ${macs_genome} --keep-dup all -B --SPMR --nomodel -n ${outname}_short_fragment;
macs2 callpeak -t ${outname}.fragments.bampe -f BAMPE -g ${macs_genome} --keep-dup all -B --SPMR --nomodel -n ${outname}_fragment;

# shift fragment cut sites to midte
cat ${outname}.fragments.bed | awk '{FS=OFS="\t"; $2=$2+4 ; $3=$3-5; print $1,$2,$2+1 + "\n" + $1,$3,$3+1 }' > ${outname}.5ends.midte.bed;

echo -e "${allreads}\n${noMreads}\n${dedup}" > ${outname}.stats;


# make fragment distribution plot
cat ${outname}.fragments.bed | awk '{FS=OFS="\t"; print $3-$2 "\t1"}' | sort -k1,1n | bedtools groupby -g 1 -c 2 -o sum > ${outname}.fraglen
R --vanilla < /exports/igmm/eddie/taylor-lab/Lana/scripts/fragment_length_distr_plot.R ${outname}.fraglen ${outname_sep}
rm ${outname}.fraglen


# convert pileup to .bw
sort -k1,1 -k2,2n ${outname}_short_fragment_treat_pileup.bdg > ${outname}.temp
bedGraphToBigWig ${outname}.temp /exports/igmm/eddie/taylor-lab/genomes/${genome}/${genome}.genome ${outname}_short_fragment_treat_pileup.bw
sort -k1,1 -k2,2n ${outname}_fragment_treat_pileup.bdg > ${outname}.temp
bedGraphToBigWig ${outname}.temp /exports/igmm/eddie/taylor-lab/genomes/${genome}/${genome}.genome ${outname}_fragment_treat_pileup.bw
rm ${outname}.temp
rm ${outname}_fragment_treat_pileup.bdg
rm ${outname}_short_fragment_treat_pileup.bdg

# Stats

python /exports/igmm/eddie/taylor-lab/Lana/scripts/atac_stats.py ${outname}.samStats ${outname}.bamStats ${outname}.stats ${outname}.allStats


# clean-up


rm ${outname}.bedpe;
rm ${outname}.short.fragments.bedpe;
rm ${outname}.fragments.bedpe;
rm ${outname}_fragment_control_lambda.bdg;
rm ${outname}_short_fragment_control_lambda.bdg;
rm ${fasta1}.trim35.fastq;
rm ${fasta2}.trim35.fastq;
rm ${outname}_short_fragment_treat_pileup.bw;
rm ${outname}_fragment_treat_pileup.bw
