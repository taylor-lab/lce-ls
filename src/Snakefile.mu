from os.path import join
configfile:
    "config.yaml"

localrules: all
workdir: config['muDir']

rule all:
    input:
        isd="allStrains.LCE_isdWGSs.tab",
        vepBl6=expand("{strn}.projectedFilteredMu.bl6Vep",strn=config['strains']),
        pfm=expand("{strn}.projectedFilteredMu.tab",strn=config['strains'])

rule produce_list_indel_wgsIds:
    input:
        vcf=join(config['inputDir'],"{strn}.indels.vcf.gz")
    output:
        "{strn}.indels.ids"
    shell:
        "set +o pipefail; zcat {input.vcf} | head -10000 | grep '^#CHROM' | perl -walne '@s=split/\s+/;foreach $r (@s){{if($r=~/^do/){{print $r;}}}}' >  {output}"

rule produce_list_snvs_wgsIds:
    input:
        vcf=join(config['inputDir'],"{strn}.snvs.vcf.gz")
    output:
        "{strn}.snvs.ids"
    shell:
        """
        set +o pipefail; zcat {input.vcf} | head -10000 | grep '^#CHROM' | perl -walne '@s=split/\s+/;foreach $r (@s){{if($r=~/^do/){{print $r;}}}}' >  {output}
        """


rule produce_indels_matrix:
    input:
        vcf=join(config['inputDir'],"{strn}.indels.vcf.gz"),
        ids="{strn}.indels.ids",
        scrpt=config['src']+"getContextsPerTumorVefIndel.pl"
    output:
        "{strn}.indels.vef.matrix"
    shell:
        """
        set +o pipefail; zcat {input.vcf} | perl -w {input.scrpt} {input.ids} | perl -wlne 's/^chr//;if(/^[\d+XY]/){{print}}' > {output}
        """

rule produce_snvs_matrix:
    input:
        vcf=join(config['inputDir'],"{strn}.snvs.vcf.gz"),
        ids="{strn}.snvs.ids",
        scrpt=config['src']+"getContextsPerTumorVef.pl"
    output:
        "{strn}.snvs.vef.matrix"
    shell:
        """
        set +o pipefail; zcat {input.vcf} | perl -w {input.scrpt} {input.ids} | perl -wlne 's/^chr//;if(/^[\d+X]/){{print}}' > {output}
        """

rule merge_mu_types:
    input:
        matSnvs="{strn}.snvs.vef.matrix",
        matIndels="{strn}.indels.vef.matrix"
    output:
        "{strn}.vef.MATRIX"
    shell:
        """
        cat {input.matSnvs} {input.matIndels} | sort -k1,1V -k2,2n > {output}
        """

rule annotate_repeats:
    input:
        mat="{strn}.vef.MATRIX",
        repMaskBed=join(config['genomeDir'],"{strn}.repeatMasker.bed"),
        dedup=join(config['src'],"dedupRepAnno.pl")
    output:
        "{strn}.vef.matrix.repAnno"
    shell:
        """
        bedtools intersect -wao -a {input.mat} -b {input.repMaskBed} | perl -w {input.dedup} > {output}
        """

rule produce_bed_for_projection:
    input:
        mat="{strn}.vef.matrix.repAnno"
    output:
        "{strn}.allMat.bed"
    shell:
        """
        cat {input.mat} | cut -f 1,2,3,4,5,6 > {output}
        """

rule  do_hal_projections:
    input:
        bed="{strn}.allMat.bed"
    output:
        "{strn}.2{strnTo}.bed"
    params:
        alnFrom=lambda wildcards: config['halNames'][wildcards.strn],
        alnTo=lambda wildcards: config['halNames'][wildcards.strnTo]
    shell:
        """
        /nfs/leia/research/flicek/user/rayner/src/hal/bin/halLiftover --noDupes  /hps/nobackup/flicek/user/cander21/ref/liftover/1509_ca.hal {params.alnFrom} {input.bed} {params.alnTo} {output}
        """

rule calculate_pos_context_for_projected:
    input:
        proj="{strn}.2{strnTo}.bed",
        chromSize=join(config['genomeDir'],"{strnTo}.chromSize.main")
    output:
        "{strn}.slop7.2{strnTo}.bed"
    params:
        chromSize=join(config['genomeDir'],"{strnTo}.chromSize.main")
    shell:
        """
        set +o pipefail; bedtools slop -i {input.proj} -g {input.chromSize} -b 3 | grep '^\(chr\)*[1-9X]' | grep -vP '\t\-1\t' > {output}
        """

rule calculate_seq_context_for_projected:
    input:
        slop="{strn}.slop7.2{strnTo}.bed",
        genomeFa=join(config['genomeDir'],"{strnTo}.fa")
    output:
        "{strn}.slop7.2{strnTo}.seqSlice"
    shell:
        """
        bedtools getfasta -s -tab -name+ -fi {input.genomeFa} -bed {input.slop} -fo {output}
        """

rule projection_reformat:
    input:
        ss="{strn}.slop7.2{strnTo}.seqSlice",
        script=join(config['src'],"halProjectionStripper.pl")
    output:
        "{strn}.slop7.2{strnTo}.toLoad"
    shell:
        """
        perl -w {input.script} {input.ss} > {output}
        """

rule integratedSampleData_processor:
    input:
        depends=expand("{strn}.slop7.2{strnTo}.toLoad",strn=config['strains'],strnTo=config['halNames']),
        sampleSet=config['inputDir']+"LCE_isd_withSig.csv",
        coverage=join(config['genomeDir'],"{strn}.arc.blacklist.bed"),
        arcMask=join(config['genomeDir'],"{strn}.arcMask.fa"),
        rscript=join(config['src'],"isdPopulate1.R")
    output:
        sas="{strn}.LCE_isdWGSsSep.tab",
        pfm="{strn}.projectedFilteredMu.tab"
    params:
        strnId="{strn}",
        rel=config['rel'],
        ver=config['ver']
    shell:
        """
        Rscript --vanilla {input.rscript} {params.strnId} {params.rel} {params.ver} 0
        """

rule mergeIsdWgs:
    input:
        isdWgs=expand("{strn}.LCE_isdWGSsSep.tab",strn=config['strains']),
        rscript=join(config['src'],"mergeIsdWGSs.R")
    output:
        "allStrains.LCE_isdWGSs.tab"
    params:
        strList=expand("{str} ",str=config['strains'])
    shell:
        """
        Rscript --vanilla {input.rscript} {params.strList}
        """


rule generateMuToBl6:
    input:
        isd="allStrains.LCE_isdWGSs.tab",
        rscript=join(config['src'],"generateBl6ProjectionList.R")
    output:
        spont = "{strn}.filteredMuInBl6.spont.tab",
        den = "{strn}.filteredMuInBl6.den.tab"
    params:
        strnId="{strn}",
        rel=config['rel'],
        ver=config['ver']
    shell:
        """
        set +o pipefail;
        Rscript --vanilla {input.rscript} {params.strnId} {params.rel} {params.ver};
        cat {params.strnId}.filteredMuInBl6.spont.tmp | perl -walne '$F[0]=~s/[:_\/]/\t/g;print"$F[0]\t$F[1]"' > {output.spont};
        cat {params.strnId}.filteredMuInBl6.den.tmp | perl -walne '$F[0]=~s/[:_\/]/\t/g;print qq:$F[0]\t$F[1]:' > {output.den};
        """

rule vepEm:
    input:
        pfm="{strn}.projectedFilteredMu.tab",
        script=join(config['src'],"muIdToVepInput.pl")
    output:
        toVep = "{strn}.projectedFilteredMu.forBl6Vep",
        vep = "{strn}.projectedFilteredMu.bl6Vep"
    params:
        strnId="{strn}",
        rel=config['rel'],
        genomes=config['genomeDir']
    shell:
        """
        set +o pipefail;
        cat {input.pfm} | cut -d',' -f 27 | perl -w {input.script} | sort -u > {output.toVep};
        vep -i {output.toVep} -o {output.vep} --species mus_musculus --cache --dir {params.genomes} --assembly GRCm38 --canonical --symbol --force_overwrite
        """
