source("../src/lce_taylor_functions.R")
library("data.table")
args = commandArgs(trailingOnly=TRUE)
strain<-args[1]
strain<-"c3h"
rel<-20180406
mat<-fread(file=paste(strain,rel,"vef.matrix.repAnno",sep="."),header=F,sep="\t")
knownDrivers<-fread(file=paste(strain,".knownDrivers",sep=""),header=F)
libAnno<-fread(file="LCE_WGS_annotation_20180406.tsv.clean",header=T,sep="\t")
#####
# GID chrom offsets
chrom<-fread(file="../GENOMES/C3H_HeJ_v1.chromSize.main",header=F,sep="\t")
chromId<-c(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,"X")
chromOffsets<-data.table(chromId=chromId,off=0,length=0)
for(i in c(2:length(chromId))){
  cindex<-which(chrom$V1==chromOffsets$chromId[i-1])
  chromOffsets$length[i-1]=chrom$V2[cindex]
  chromOffsets$off[i]=chromOffsets$off[i-1]+chrom$V2[cindex]
}
# catch the last chrom
chromOffsets$length[length(chromId)]=chrom$V2[which(chrom$V1==chromOffsets$chromId[length(chromId)])]
#####


set<-as.data.frame(mat[V16>5 & V20==".", order(.N), by = .(V1,V3,V4,V20,strtrim(V14,15),V15,V16,V9,V5)])
# V5 is always 1 in mat
set$V5=0
for(i in c(1:length(chromOffsets$chromId))){
  sel<-which(set$V1==chromOffsets$chromId[i])
  set$V5[sel]=set$V3[sel]+chromOffsets$off[i]
}
set[order(set$V16),]

###
# Jaccard distance of variant pairs
pr<-c("17:51787423_A/C","17:51788332_G/A")

#####
# all pairwise comparisons
ln<-length(set$V4)
pairMat<-matrix(rep(NA,ln*ln),ln,ln)
pairMatDist<-pairMat
maxCount<-max(set$V16)
for(pa in c(1:ln)){
  for(pb in c(1:ln)){
     pr<-c(set$V4[pa],set$V4[pb])
     jv<-mat[V4 %in% pr,.N,by=V13]
     #pairMat[pa,pb]=(length(jv$N)-(sum(jv$N)-length(jv$N)))/length(jv$N)
     pairMat[pa,pb]=(sum(jv$N)-length(jv$N))/length(jv$N)
     if(pa==pb){
       pairMat[pa,pb]=1
     }
     pairMatDist[pb,pa]=(log10(abs(set$V5[pa]-set$V5[pb]))/9)
     if(set$V1[pa]!=set$V1[pb]){pairMatDist[pb,pa]=1}
  }
}
pairMatDist[which(pairMatDist<0)]=0
cramp<-colorRampPalette(c("grey","blue","green","yellow","red"))
#image(pairMat,col=cramp(5),axes=F)
setBounds<-findBounds(set[,1])
maxB<-max(setBounds$end)
tmv<-triangleMaskVec(dim(pairMat)[1])
pm<-pairMat
pm[tmv]=0
png(file="jaccardByVarHeat.png",width=900,height=900)
image(pm,col=cramp(5),axes=F)
crampBW<-colorRampPalette(c("black","white"))
pmd<-pairMatDist
tmv<-triangleMaskVec(dim(pairMat)[1],upper=F,diag=T)
pmd[tmv]=NA
image(pmd,col=crampBW(5),axes=F,add=T)
axis(1,at=c(0,setBounds$end/maxB),lab=F,tick=T)
axis(1,at=setBounds$mid/maxB,lab=setBounds$label,tick=F)


ypos<-0.8
xstart<-0.2
sdim<-0.03
nblocks<-5
blocks<-c(0:nblocks)*sdim
cols<-cramp(nblocks)
xend<-xstart+(nblocks*sdim)
xmid<-((xend-xstart)/2)+xstart
points(c(xstart,xstart),c(ypos-(sdim/2),ypos+sdim),type="l",col="black")
points(c(xmid,xmid),c(ypos-(sdim/2),ypos+sdim),type="l",col="black")
points(c(xend,xend),c(ypos-(sdim/2),ypos+sdim),type="l",col="black")
text(xstart,ypos-(sdim),0)
text(xmid,ypos-(sdim),0.5)
text(xend,ypos-(sdim),1)
for(u in c(1:nblocks)){
  xl<-xstart+blocks[u]
  xr<-xstart+blocks[u+1]
  polygon(c(xl,xl,xr,xr),c(ypos,ypos+sdim,ypos+sdim,ypos),col=cols[u],border=NA)
}
polygon(c(xstart,xstart,xend,xend),c(ypos,ypos+sdim,ypos+sdim,ypos),col=NA,border="black")

ypos<-0.9
xstart<-0.2
sdim<-0.03
nblocks<-5
blocks<-c(0:nblocks)*sdim
cols<-crampBW(nblocks)
xend<-xstart+(nblocks*sdim)
points(c(xstart,xstart),c(ypos-(sdim/2),ypos+sdim),type="l",col="black")
points(c(xmid,xmid),c(ypos-(sdim/2),ypos+sdim),type="l",col="black")
points(c(xend,xend),c(ypos-(sdim/2),ypos+sdim),type="l",col="black")
text(xstart,ypos-(sdim),1)
text(xmid,ypos-(sdim),5)
text(xend,ypos-(sdim),9)
for(u in c(1:nblocks)){
  xl<-xstart+blocks[u]
  xr<-xstart+blocks[u+1]
  polygon(c(xl,xl,xr,xr),c(ypos,ypos+sdim,ypos+sdim,ypos),col=cols[u],border=NA)
}
polygon(c(xstart,xstart,xend,xend),c(ypos,ypos+sdim,ypos+sdim,ypos),col=NA,border="black")
dev.off()
####
####

v<-c(0,0,0,0,1,1,0,1,0,1,0,1,1,1,1,1,0,1,1,1,1,0,0,1)
oneDimSW(v)

#####
# For each nodule - how many recurrent mutations shared with each other
slimMat<-mat[V16>5 & V20==".",]
slimMat$animal<-extractAnimal(slimMat$V13)
slimMat$lib<-extractLib(slimMat$V13)
slimMat$libAnnoIndex<-match(slimMat$lib, libAnno$Library)
byNod<-mat[,uniqueN(V13),by=.(V13)]
nodByVar<-matrix(0,dim(byNod)[1],dim(set)[1])
jaccardByNod<-matrix(NA,dim(byNod)[1],dim(byNod)[1])
byNodCounts<-rep(0,dim(byNod)[1])
startAt<-350
for(i in c(startAt:dim(byNod)[1])){
  va<-slimMat[V13 %in% byNod[i,1],.N,by=V4]
  nodByVar[i,which(set$V4 %in% va$V4)]=1
  byNodCounts[i]<-dim(va)[1]
  for(j in c(startAt:dim(byNod)[1])){
    pr<-as.vector(unlist(c(byNod[i,1],byNod[j,1])))
    jv<-slimMat[V13 %in% pr,.N,by=V4]
    jaccardByNod[i,j]=(sum(jv$N)-length(jv$N))/length(jv$N)
    if(i==j){
      jaccardByNod[i,j]=1
    }
  }
}
nodByVar[,which(set$V4 %in% knownDrivers$V1)]=nodByVar[,which(set$V4 %in% knownDrivers$V1)]*2
png(file="nodSharingAllRec.png",width=900,height=600)
image(t(nodByVar),col=c("white","black","red"))
dev.off()
image(t(nodByVar[c(350:397),]),col=c("white","black","red"))


dstamp<-gsub("-","",Sys.Date())

####
# Write out the tables for reload
write.table(slimMat,file=paste(strain,"slimMat",dstamp,sep="."),sep="\t",quote=F)
write.table(jaccardByNod,file=paste(strain,"jaccardByNod",dstamp,sep="."),sep="\t",quote=F)
write.table(nodByVar,file=paste(strain,"nodByVar",dstamp,sep="."),sep="\t",quote=F)
write.table(pairMat,file=paste(strain,"pairMat",dstamp,sep="."),sep="\t",quote=F)
write.table(pairMatDist,file=paste(strain,"pairMatDist",dstamp,sep="."),sep="\t",quote=F)
write.table(chromOffsets,file=paste(strain,"pairMatDist",dstamp,sep="."),sep="\t",quote=F)
image(t(jaccardByNod),col=cramp(5))
