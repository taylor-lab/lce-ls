# Process the output of bedtools getfasta with name+ and do coordinate
# transform and strand flip to define equivalent mutation
use strict;

my %idx;

while(<>){
  chomp;
  my @sp = split /\t/;
  my $change = "X";
  if($sp[0] =~ /([ATCG-]+\/[ATCG-]+)/){
    $change = $1;
  }
  if($sp[0]=~/^(.*)::(\w+):(\d+)-(\d+)\(([-+])\)/){
    my $from = $1;
    my $pChr = $2;
    my $pLeft = $3;
    my $pRight = $4;
    my $pStrand = $5;
    my $pos = $pLeft + 4;
    if ($pStrand eq "-"){
      $change =~ y/ATCG/TAGC/;
    }
    my $multipleMatchSites=0;
    my $pMu = "$pChr:$pos"."_$change";
    my $construct = join "\t", ($from,$pMu,$sp[1]);
    if(defined $idx{$from}){
      if($idx{$from} ne $construct){
        $multipleMatchSites=1;
      }
    }
    if($multipleMatchSites){
      $idx{$from} = join "\t", ($from,"MULTI",$sp[1]);
    } else {
      $idx{$from} = $construct;
    }
  } else {
    print STDERR "WARN failed to interpret $sp[0]\n";
  }
}
my @kys = keys %idx;
for my $ky (0 .. $#kys){
  print "$idx{$kys[$ky]}\n";
}
