#!/usr/bin/perl
#
# Split a coordinates based stream given constraints on where to split...
my ($outLines,$distLim,$prefix) = @ARGV;
my $lastPos = 0;
my $suffix = 1;
my $count = $outLines;
open(FO,">$prefix.$suffix") or die "Failed to write $prefix.$suffix";
while(<STDIN>){
  if ($count-- <= 0){
    # We're ready for a new output
    if (/(\S+)\t(\d+)/){
      my $d = $2 - $lastPos;
      print STDERR "$lastPos $2\t$d\t$suffix\n";
      if($d > $distLim){
        close FO;
        sleep 1;
        $count = $outLines;
        $suffix++;
        open(FO,">$prefix.$suffix");
      }
      $lastPos = $2;
    }
  }
  print FO $_;
}
