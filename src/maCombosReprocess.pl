use strict;

my %idx = ('AA' => 0,
  'AC' => 1,
  'AG' => 2,
  'AT' => 3,
  'CA' => 4,
  'CC' => 5,
  'CG' => 6,
  'CT' => 7,
  'GA' => 8,
  'GC' => 9,
  'GG' => 10,
  'GT' => 11,
  'TA' => 12,
  'TC' => 13,
  'TG' => 14,
  'TT' => 15
  );
my @keyOrd = sort {$a cmp $b} keys %idx;
my $header=join ",",("chrLeft","posLeft","refLeft","altLeft","chrRight","posRight","refRight","altRight","dist","craigCode","combiCount",@keyOrd);
print"$header\n";
while(<>){
  chomp;
  my @sp = split/\t/;
  next if($#sp <3);
  my($c1,$p1,$fr1,$to1) = ("NA","NA","NA","NA");
  my($c2,$p2,$fr2,$to2) = ("NA","NA","NA","NA");
  if($sp[0]=~/(\w+):(\d+)_([ATCG])\/([ATCG])/){
    ($c1,$p1,$fr1,$to1)=($1,$2,$3,$4);
  }
  if($sp[2]=~/(\w+):(\d+)\.0_([ATCG])\/([ATCG])/){
    ($c2,$p2,$fr2,$to2)=($1,$2,$3,$4);
  }
  $sp[4]=int($sp[4]);
  $sp[7]=~s/\s+//g;
  $sp[7]=~s/,//g;
  $sp[7]=~s/'//g;
  $sp[7]=~s/\(//g;
  $sp[7]=~s/\)//g;
  my @diNucs = split/\d+/, $sp[7];
  my @counts = split/[ACGT]+/, $sp[7];
  shift @counts;
  my @diCount = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
  for my $n (0 .. $#diNucs){
    #print "$n $diNucs[$n] $idx{$diNucs[$n]} $diCount[$idx{$diNucs[$n]}]\n";
    $diCount[$idx{$diNucs[$n]}]=$counts[$n];
  }
  my $o=join",",($c1,$p1,$fr1,$to1,$c2,$p2,$fr2,$to2,$sp[4],$sp[5],$sp[6],@diCount);
  print"$o\n";
}
