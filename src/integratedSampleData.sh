
cd /hps/nobackup2/flicek/user/mst/lce/ANALYSIS
source /hps/nobackup2/flicek/user/mst/lce/src/initEnvironment.sh /hps/nobackup2/flicek/user/mst/lce/src/parameters_lce.sh c3h

ls ../genomes/expression_nodule/*.tsv | xargs basename -a > ${inputDir}/expression_nodule.list

perl -w ../src/processHistologyTable.pl ${inputDir}/LCE_allHistology.csv ${inputDir}/LCE_WGS_annotation_20180406.tsv.clean ${inputDir}/Master_DB_CTCF_20190311.csv  ${inputDir}/all.20180406.crudeCounts ${inputDir}/expression_nodule.list > LCE_integratedSampleData.csv

Rscript --vanilla ../src/isdLoadSpectraDecomp.R
