# Eigen vectors are dumped from juicebox by stuart on a per chromosome basis but
# +/- assignment is arbitrary for each chromosome. Apparently the following chromosomes
# need to be flipped in the dumps provided on 17th October 2018...
# 1,7,9,12,16,18,19


my ($chromSize) = @ARGV;
my %flip = ( '1' => 1,
  '7' => 1,
  '9' => 1,
  '12' => 1,
  '16' => 1,
  '18' => 1,
  '19' => 1);

open (CS, "<$chromSize");
my @names =();
my @lengths =();
my $window = 500000;
while(<CS>){
  chomp;
  my @sp = split /\s+/;
  push @names, $sp[0];
  push @lengths, $sp[1];
}
close CS;

for my $i (0 .. $#names){
  open(EIGEN, "<eigen.KR.$window.chr.$names[$i].txt");
  my $left = 0;
  while(<EIGEN>){
    chomp;
    $score = $_;
    if ($flip{$names[$i]}){
      $score = $score * -1;
    }
    $right = $left + $window;
    $right = $lengths[$i] if ($right > $lengths[$i]);
    print join "\t", ($names[$i],$left,$right,$score);
    print "\n";
    $left = $right;
  }
  close EIGEN;
}
