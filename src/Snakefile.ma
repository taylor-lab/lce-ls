configfile:
    "config.yaml"

localrules: all
workdir: config['maDir']

NODWES,=glob_wildcards("{nodId}.wes.mpileup")
NODWGS,=glob_wildcards("wgsmp/{nodName}.mpileup")

rule all:
  input:
    rr="wesValidationCurves.rdat",
    pnrc="perNodRate.tab",
    segMa=expand("{nodId}.fnodDrcrMa",nodId=NODWGS)

rule pairwiseWesValidate:
  input:
    wesMp="{nodId}.wes.mpileup"
  output:
    valMat="{nodId}.vs.{wesId}.validationMatrix"
  params:
    rscript=config['src']+"maValidateWes.R",
    wgs="{nodId}",
    wes="{wesId}"
  shell:
    """
    Rscript --vanilla {params.rscript} {params.wgs} {params.wes} {output.valMat}
    """

rule calculateValidationCurves:
  input:
    rr=expand("{nodId}.vs.{wesId}.validationMatrix",nodId=NODWES,wesId=NODWES)
  output:
    curves="wesValidationCurves.rdat"
  params:
    rscript=config['src']+"maAnalyseValidateWes.R"
  shell:
    """
    Rscript --vanilla {params.rscript} {input.rr}
    """

rule tumourWideMaWithNull:
  input:
    mp="wgsmp/{nodName}.mpileup"
  output:
    maRates="{nodName}.maRatesWithProxy",
    maMatMa="{nodName}.nodMatMa"
  params:
    rscript=config['src']+"maPerNodRate.R",
    nodId="{nodName}"
  shell:
    """
    Rscript --vanilla {params.rscript} {params.nodId}
    """

rule collatePerNodRate:
  input:
    pnr=expand("{nodName}.maRatesWithProxy",nodName=NODWGS)
  output:
    colPnr="perNodRate.tab"
  params:
    rscript=config['src']+"maCollatePerNodRate.R"
  shell:
    """
    Rscript --vanilla {params.rscript} {output.colPnr} {input.pnr}
    """

rule perNodSegMaRates:
  input:
    mp="wgsmp/{nodId}.mpileup"
  output:
    maSeg="{nodId}.fnodDrcrMa"
  params:
    rscript=config['src']+"maPerNodSegRates.R",
    nodId="{nodId}"
  shell:
    """
    Rscript --vanilla {params.rscript} {params.nodId}
    """
